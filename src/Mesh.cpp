//
// Created by Matty on 2021-03-26.
//
#include "../app/render/Mesh.h"
#include "../app/render/Renderer.h"
#include "../app/render/RenderData.h"
#include "../app/engine/ModelUniformBuffer.h"
#include "../app/engine/EngineGlobals.h"

void VKP::Mesh::Load(const void *vertices, uint32_t verticesLength, const uint32_t *indices, uint32_t indexCount,
                     uint32_t vertexCount) {
    m_pValid = true;
    auto &renderer = VKP::Renderer::m_Renderer;

    m_pVertexBuffer = renderer->CreateVertexBuffer(verticesLength);
    m_pVertexBuffer->LoadData(vertices, verticesLength, vertexCount);

    m_pIndexBuffer = renderer->CreateIndexBuffer(indexCount);
    m_pIndexBuffer->LoadData(indices, indexCount);

    m_pPipeline = RenderData::GetInstance()->m_Pipelines.at(0);
}

void VKP::Mesh::Draw(const VKP::FrameDrawData &frameCmdBuffer, const glm::mat4 & matrix) {
    PushConstMesh pushConst{};
    pushConst.model = matrix;
    pushConst.viewModel = frameCmdBuffer.viewMatrix*matrix;

    vkCmdPushConstants(frameCmdBuffer.frameCommandBuffer, m_pPipeline->GetLayout(), VK_SHADER_STAGE_VERTEX_BIT, 0,
                       sizeof(pushConst), &pushConst);

    vkCmdDrawIndexed(frameCmdBuffer.frameCommandBuffer, m_pIndexBuffer->GetIndexCount(), 1, 0, 0, 0);
}

VKP::Mesh::Mesh() {

}

VKP::Mesh::~Mesh() {
    if (m_pValid) {
        m_pVertexBuffer->MarkForDeletion();
        m_pIndexBuffer->MarkForDeletion();
    }
}

VKP::Mesh::Mesh(VKP::Mesh &&o) noexcept {
    m_pValid = o.m_pValid;
    m_pPipeline = std::move(o.m_pPipeline);
    m_pIndexBuffer = std::move(o.m_pIndexBuffer);
    m_pVertexBuffer = std::move(o.m_pVertexBuffer);
    o.m_pValid = false;
}

VKP::Mesh &VKP::Mesh::operator=(VKP::Mesh &&o) noexcept {
    m_pValid = o.m_pValid;
    m_pPipeline = std::move(o.m_pPipeline);
    m_pIndexBuffer = std::move(o.m_pIndexBuffer);
    m_pVertexBuffer = std::move(o.m_pVertexBuffer);
    o.m_pValid = false;
    return *this;
}

void VKP::Mesh::Bind(const VKP::FrameDrawData &frameCmdBuffer) {
    m_pPipeline->Bind(frameCmdBuffer);
    m_pVertexBuffer->BindVertexBuffer(frameCmdBuffer.frameCommandBuffer);
    m_pIndexBuffer->BindIndexBuffer(frameCmdBuffer.frameCommandBuffer);
}

void VKP::Mesh::BindUniformBuffer(const VKP::FrameDrawData &frameCmdBuffer, VKP::UniformBufferBinding &uniformBuffer) {
    if(!m_pPipeline->BindUniformBuffer(frameCmdBuffer,uniformBuffer)){
        m_pPipeline->RecreateUniformBuffer(uniformBuffer,VKP_Engine::EngineGlobals::s_ModelUniformDesc);
        assert(m_pPipeline->BindUniformBuffer(frameCmdBuffer,uniformBuffer));
    };
}
