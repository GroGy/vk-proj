//
// Created by Matty on 2021-03-28.
//

#include "../app/game/Player.h"
#include "../app/game/data/Game_WorldData.h"
#include "../app/game/data/Game_RenderData.h"
#include "../app/input/InputController.h"
#include <glm/gtx/string_cast.hpp>

const glm::vec3 &VKP_Game::Player::GetPosition() const {
    return m_pPosition;
}

void VKP_Game::Player::SetPosition(const glm::vec3 &position) {
    m_pPosition = position;
}

const glm::vec3 &VKP_Game::Player::GetRotation() const {
    return m_pRotation;
}

void VKP_Game::Player::SetRotation(const glm::vec3 &rotation) {
    m_pRotationTarget = rotation;
}



VKP_Game::Player::Player(reactphysics3d::PhysicsWorld *physicsWorld, const glm::vec3 &pos, const glm::vec3 &rotation,
                         float scale) : m_pValid(true), m_pScale(scale), m_pPosition(pos), m_pRotation(rotation),m_pRotationTarget(rotation),
                         m_pModel(Game_RenderData::GetInstance()->m_CharacterModel) {


    reactphysics3d::Vector3 position(m_pPosition.x, m_pPosition.y, m_pPosition.z);
    reactphysics3d::Quaternion orientation = reactphysics3d::Quaternion::fromEulerAngles(m_pRotation.x, m_pRotation.y,
                                                                                         m_pRotation.z);
    reactphysics3d::Transform transform(position, orientation);

    m_pRigidBody = physicsWorld->createRigidBody(transform);
    m_pRigidBody->setMass(0.3f);
    m_pRigidBody->setLinearDamping(0.8f);

    m_pColliderShape = Game_WorldData::GetInstance()->m_PhysicsCommon.createCapsuleShape(scale, scale * 2.f);

    auto colliderOffset = reactphysics3d::Transform::identity();
    colliderOffset.setPosition({0,scale*2,0});

    m_pCollider = m_pRigidBody->addCollider(m_pColliderShape,colliderOffset );
}


float VKP_Game::Player::GetScale() const {
    return m_pScale;
}

void VKP_Game::Player::SetScale(float scale) {
    m_pScale = scale;
}

void VKP_Game::Player::Draw(VKP::FrameDrawData &frame){
    if(!m_pValid) return;
    m_pModel.SetPosition(m_pPosition);
    m_pModel.SetRotation(m_pRotation);
    m_pModel.SetScale({m_pScale,m_pScale,m_pScale});
    m_pModel.Draw(frame);
}

void VKP_Game::Player::Update(float deltaTime) {
    m_pRotation.x = 0.0f;
    m_pRotation.y = 0.0f;
    m_pRotationTarget.x = 0.0f;
    m_pRotationTarget.y = 0.0f;

    m_pRotation = glm::mix(m_pRotation,m_pRotationTarget,0.5f);
    auto tmpTrans = m_pRigidBody->getTransform();
    tmpTrans.setOrientation(reactphysics3d::Quaternion::identity());
    m_pRigidBody->setTransform(tmpTrans);
    auto & cam = Game_RenderData::GetInstance()->m_Camera;
    auto frontVec = cam.GetForwardVectorXZ();
    auto rightVec = cam.GetRightVectorXZ();

    auto & inputCon = VKP::InputController::GetInstance();

    bool moveWhereLooking = inputCon->IsKeyPressed("MOUSE_RIGHT");

    if(moveWhereLooking) {
        auto rotFin = cam.GetRotation() + glm::vec3(0,0,0);
        m_pRotationTarget = rotFin;
    }

    float speed = m_pSpeed * 0.1f;

    if (inputCon->IsKeyPressed("FORWARD")) {
        m_pRigidBody->applyForceToCenterOfMass(moveWhereLooking ? reactphysics3d::Vector3(frontVec.x*speed , 0, frontVec.y * speed) : reactphysics3d::Vector3(1*speed,0,0));
    }
    if (inputCon->IsKeyPressed("BACKWARD")) {
        m_pRigidBody->applyForceToCenterOfMass(moveWhereLooking ? -reactphysics3d::Vector3(frontVec.x*speed , 0, frontVec.y * speed) : reactphysics3d::Vector3(-1*speed,0,0));
    }
    if (inputCon->IsKeyPressed("LEFT")) {
        m_pRigidBody->applyForceToCenterOfMass(moveWhereLooking ? -reactphysics3d::Vector3(rightVec.x*speed , 0, rightVec.y * speed) : reactphysics3d::Vector3(0,0,-1*speed));
    }
    if (inputCon->IsKeyPressed("RIGHT")) {
        m_pRigidBody->applyForceToCenterOfMass(moveWhereLooking ? reactphysics3d::Vector3(rightVec.x*speed , 0, rightVec.y * speed) : reactphysics3d::Vector3(0,0,1*speed));
    }

    float jumpMultiplier = 10.0f;

    if(inputCon->OnKeyPressed("JUMP") && m_pGrounded) {
        m_pRigidBody->applyForceToCenterOfMass({0,1*jumpMultiplier,0});
        //m_pGrounded = false;
    }

    auto physTransform = m_pRigidBody->getTransform();
    m_pPosition.x = physTransform.getPosition().x;
    m_pPosition.y = physTransform.getPosition().z;
    m_pPosition.z = physTransform.getPosition().y;

    auto & unitRegistry = VKP_Game::Game_UnitRegistry::s_Instance;
    int32_t gridX = int32_t(m_pPosition.x / 16.0f) + 8;
    int32_t gridY = int32_t(m_pPosition.y / 16.0f) + 8;

    if(auto entt = unitRegistry.GetIGridEntity({gridX,gridY})) {
        if(!unitRegistry.Is<VKP_Game::CubeEntity>({gridX,gridY}))
            unitRegistry.AddGridEntity<VKP_Game::CubeEntity>({gridX,gridY});
    }
}

bool VKP_Game::Player::IsValid() const {
    return m_pValid;
}

