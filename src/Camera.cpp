//
// Created by Matty on 2021-03-28.
//

#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/detail/type_quat.hpp>
#include "../app/render/Camera.h"
#include "../app/game/data/Game_WorldData.h"

void VKP::Camera::CalcView() {
    if(m_pViewMatrixInvalid) {
        m_pViewMatrix = glm::lookAt(m_pPosition, m_pFocusPosition, glm::vec3(0.0f, 0.0f, 1.0f));
        glm::vec3 scale;
        glm::quat rotation;
        glm::vec3 translation;
        glm::vec3 skew;
        glm::vec4 perspective;
        glm::decompose(m_pViewMatrix, scale, rotation, translation, skew,perspective);
        rotation=glm::conjugate(rotation);

        auto rotRad = glm::eulerAngles(rotation);
        m_pRotation = glm::degrees(rotRad);



        m_pViewMatrixInvalid = false;
    }
}

const glm::vec3 &VKP::Camera::GetPosition() const {
    return m_pPosition;
}

void VKP::Camera::SetPosition(const glm::vec3 &mPPosition) {
    m_pPositionTarget = mPPosition;
    m_pViewMatrixInvalid = true;
}

const glm::vec3 &VKP::Camera::GetRotation() const {
    return m_pRotation;
}

void VKP::Camera::SetRotation(const glm::vec3 &mPRotation) {
    m_pRotation = mPRotation;
    m_pViewMatrixInvalid = true;
}

glm::mat4 VKP::Camera::GetViewMatrix() const {
    return m_pViewMatrix;
}

const glm::vec3 &VKP::Camera::GetFocusPosition() const {
    return m_pFocusPosition;
}

void VKP::Camera::SetFocusPosition(const glm::vec3 &position) {
    m_pFocusPosition = position;
    m_pViewMatrixInvalid = true;
}

void VKP::Camera::UpdateScreenRatio(float ratio) {
    m_pScreenRatio = ratio;
    m_pViewMatrixInvalid = true;
}

glm::mat4 VKP::Camera::GetProjMatrix() const {
    auto res = glm::perspective(glm::radians(m_pFOV), m_pScreenRatio, m_pNearPlane, m_pFarPlane);
    res[1][1] *= -1;
    return res;
}

glm::vec3 VKP::Camera::GetForwardVector() const {
    return glm::normalize(m_pFocusPosition - m_pPosition);
}

glm::vec2 VKP::Camera::GetForwardVectorXZ() const {
    glm::vec2 xzDiff{m_pFocusPosition.x -m_pPosition.x,m_pFocusPosition.y -m_pPosition.y};
    return glm::normalize(xzDiff);
}

glm::vec2 VKP::Camera::GetRightVectorXZ() const {
    glm::vec2 fwd = GetForwardVectorXZ();
    return glm::normalize(glm::vec2(fwd.y,-fwd.x));
}

void VKP::Camera::Update(float delta) {
    m_pPosition = glm::mix(m_pPosition, m_pPositionTarget, 0.5f*delta);
}

void VKP::Camera::Init() {
    auto & worldData = VKP_Game::Game_WorldData::GetInstance();

    const reactphysics3d::Vector3 halfExtents(1.0, 1.0, 1.0);

    m_pBoxShape = worldData->m_PhysicsCommon.createBoxShape(halfExtents);

    auto center = (m_pPosition + m_pFocusPosition) / 2.0f;
    auto colliderOffset = reactphysics3d::Transform::identity();
    auto collBodyTrans = reactphysics3d::Transform::identity();
    collBodyTrans.setPosition({center.x,center.y,center.z});
    m_pCollisionBody = worldData->m_World.GetPhysicsWorld()->createCollisionBody(collBodyTrans);
    m_pViewFrustum = m_pCollisionBody->addCollider(m_pBoxShape,colliderOffset);
}

void VKP::Camera::Cleanup() {

}
