//
// Created by Matty on 2021-04-05.
//

#include "../app/render/DepthTexture.h"
#include "../app/render/Renderer.h"

VkFormat VKP::DepthTexture::_FindDepthFormat() {
    return _FindSupportedFormat(
            {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
            VK_IMAGE_TILING_OPTIMAL,
            VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    );
}

VKP::DepthTexture::DepthTexture(const VmaAllocator &allocator) : Texture(allocator,
                                                                         VKP::Renderer::m_Renderer->GetSwapChainExtent().width,
                                                                         VKP::Renderer::m_Renderer->GetSwapChainExtent().height,
                                                                         _FindDepthFormat(), VK_IMAGE_TILING_OPTIMAL,
                                                                         VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                                                                         false) {
    _CreateImageView(_FindDepthFormat(),VK_IMAGE_ASPECT_DEPTH_BIT);
}
