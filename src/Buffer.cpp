//
// Created by Matty on 2021-03-27.
//

#include "../app/render/platform/Buffer.h"
#include "../app/render/Renderer.h"

void VKP::Buffer::Cleanup(VmaAllocator const &allocator) {
    if(!m_pValid) {
        VKP_LOG_WARN("Cleanup was called on invalid buffer");
        return;
    }
    vmaDestroyBuffer(allocator, m_pBuffer, m_pAllocation);
}

bool VKP::Buffer::MarkedForDeletion() const {
    if(!m_pValid) {
        VKP_LOG_WARN("MarkedForDeletion was called on invalid buffer");
        return false;
    }
    return m_pMarkedForDeletion;
}

void VKP::Buffer::MarkForDeletion() {
    m_pMarkedForDeletion = true;
}

VKP::Buffer::Buffer(VmaAllocator &allocator,VkBufferCreateInfo bufferInfo, VmaAllocationCreateInfo allocInfo) :
        m_pAllocator(allocator), m_pValid(true) {
    auto res = vmaCreateBuffer(m_pAllocator, &bufferInfo, &allocInfo, &m_pBuffer, &m_pAllocation, &m_pAllocInfo);
    if(res != VK_SUCCESS) {
        assert(false);
    }
    m_pSize = bufferInfo.size;
}

VKP::Buffer::Buffer(VKP::Buffer &&o) noexcept {
    m_pAllocation = o.m_pAllocation;
    m_pValid = o.m_pValid;
    m_pMarkedForDeletion = o.m_pMarkedForDeletion;
    m_pBuffer = o.m_pBuffer;
    m_pAllocator = o.m_pAllocator;
    m_pAllocInfo = o.m_pAllocInfo;
    m_pSize = o.m_pSize;
    o.m_pValid = false;
}

VKP::Buffer &VKP::Buffer::operator=(VKP::Buffer &&o) noexcept {
    m_pAllocation = o.m_pAllocation;
    m_pValid = o.m_pValid;
    m_pMarkedForDeletion = o.m_pMarkedForDeletion;
    m_pBuffer = o.m_pBuffer;
    m_pAllocator = o.m_pAllocator;
    m_pAllocInfo = o.m_pAllocInfo;
    m_pSize = o.m_pSize;
    o.m_pValid = false;
    return *this;
}

void VKP::Buffer::LoadData(const void *data,size_t size) {
    VkMemoryPropertyFlags memFlags;
    vmaGetMemoryTypeProperties(m_pAllocator, m_pAllocInfo.memoryType, &memFlags);
    m_pSize = size;
    if((memFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) != 0)
    {
        // Allocation ended up in mappable memory. You can map it and access it directly.
        void* mappedData;
        vmaMapMemory(m_pAllocator, m_pAllocation, &mappedData);
            memcpy(mappedData, data, size);
        vmaUnmapMemory(m_pAllocator, m_pAllocation);
    }
    else
    {
        return;
        // TODO : Do this from other buffer, its doable but idk about speed, will try
        // Allocation ended up in non-mappable memory.
        // You need to create CPU-side buffer in VMA_MEMORY_USAGE_CPU_ONLY and make a transfer.
    }
}

VkBuffer VKP::Buffer::GetVulkanBuffer() {
    return m_pBuffer;
}

void VKP::Buffer::LoadData(VKP::Buffer &storage) {
    auto & renderer = VKP::Renderer::m_Renderer;
    renderer->CopyBuffer(storage,*this,storage.m_pSize);
}
