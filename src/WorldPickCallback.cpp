//
// Created by Matty on 2021-09-18.
//

#include "../app/game/raycast_callbacks/WorldPickCallback.h"
#include "../app/game/data/Game_WorldData.h"

reactphysics3d::decimal VKP_Game::WorldPickCallback::notifyRaycastHit(const reactphysics3d::RaycastInfo &raycastInfo) {
    if(!raycastInfo.collider) return 0;

    auto reactPos = raycastInfo.worldPoint;

    //Game_WorldData::GetInstance()->m_Player.SetPosition({reactPos.x,reactPos.y,reactPos.z});

    return 0;
}