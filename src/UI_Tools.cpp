//
// Created by Matty on 2021-03-28.
//

#include "../app/ui/windows/UI_Tools.h"
#include "../app/engine/ModelLoader.h"
#include "../app/game/Game.h"

#include "../app/editor/ModelImporter.h"
#include "../app/engine/ImageLoader.h"

void VKP::UI_Tools::CustomDraw() {
    if(m_pPrefabEditorOpenned) {

    }

    if(ImGui::Button("Import prefab")) {
        m_pPrefabEditorOpenned = true;
    }

    if (!m_pFileBrowser.IsOpened() && m_pFileBrowserOpen) {
        if (m_pFileBrowser.HasSelected()) {
            std::string path = m_pFileBrowser.GetSelected().string();
            if(m_pSelectingModel) {
                auto res = VKP_Editor::ModelImporter::ImportModel(path);
                if(!res.first) {
                    VKP_LOG_ERROR("Failed to import model with error: {}",res.second.value())
                }
                m_pSelectingModel = false;
            } else if(m_pLoadingModel) {
                auto res = VKP_Engine::ModelLoader::LoadModel(path);
                if(!res.first) {
                    VKP_LOG_ERROR("Failed to load model with error: {}",res.second.value())
                }
                auto & game = VKP_Game::Game::GetInstance();
                m_pLoadingModel = false;
            } else if(m_pLoadingImage) {
                auto res = VKP_Engine::ImageLoader::LoadTexture(path);
                if(!res.first) {
                    VKP_LOG_ERROR("Failed to load texture with error: {}",res.second.value())
                }
                auto & game = VKP_Game::Game::GetInstance();
                m_pLoadingModel = false;
            }
        }
        ImGui::SetNextWindowFocus();
        m_pFileBrowserOpen = false;
    }

    if (ImGui::Button("Import model", {80.0f, 24.0f})) {
        m_pFileBrowser.SetTypeFilters({".fbx", ".dae", ".obj", ".FBX", ".DAE", ".OBJ"});
        m_pFileBrowserOpen = true;
        m_pFileBrowser.Open();
        m_pSelectingModel = true;
    }
    if (ImGui::Button("Load model", {80.0f, 24.0f})) {
        m_pFileBrowser.SetTypeFilters({".vk_model"});
        m_pFileBrowserOpen = true;
        m_pFileBrowser.Open();
        m_pLoadingModel = true;
    }
    if (ImGui::Button("Load image", {80.0f, 24.0f})) {
        m_pFileBrowser.SetTypeFilters({".png"});
        m_pFileBrowserOpen = true;
        m_pFileBrowser.Open();
        m_pLoadingImage = true;
    }
}

VKP::UI_Tools::UI_Tools() : UI_Window("Tools") {
}

void VKP::UI_Tools::CustomPostDraw() {
    if (m_pFileBrowserOpen) {
        m_pFileBrowser.Display();
    }
}
