//
// Created by Matty on 2021-03-28.
//

#include "../app/render/Model.h"
#include "../app/engine/ModelUniformBuffer.h"

void VKP::Model::Draw(const VKP::FrameDrawData & frame, const glm::mat4 & modelMatrix, std::vector<VKP::UniformBufferBinding> & bindings) {
    if(!m_pLoaded) return;

    for (uint32_t i = 0; i < m_pMeshes.size(); ++i) {
        m_pMeshes.at(i).Bind(frame);
        VKP_Engine::ModelUniformBuffer modelBuf{modelMatrix, frame.viewMatrix,frame.projMatrix};
        bindings.at(i).m_Buffer->LoadData(&modelBuf, sizeof(modelBuf));
        m_pMeshes.at(i).BindUniformBuffer(frame,bindings.at(i));
        m_pMeshes.at(i).Draw(frame,modelMatrix);
    }
}

bool VKP::Model::IsValid() const {
    return m_pValid;
}

VKP::Model::Model(std::vector<VKP::Mesh> meshes) : m_pValid(true), m_pMeshes(std::move(meshes)){
    m_pLoaded = true;
}

VKP::Model::Model(VKP::Model &&o) noexcept {
    m_pValid = o.m_pValid;
    m_pMeshes = std::move(o.m_pMeshes);
    m_pModelMatrixInvalid = o.m_pModelMatrixInvalid;
    o.m_pValid = false;
}

VKP::Model &VKP::Model::operator=(VKP::Model &&o) noexcept {
    m_pValid = o.m_pValid;
    m_pMeshes = std::move(o.m_pMeshes);
    m_pModelMatrixInvalid = o.m_pModelMatrixInvalid;
    o.m_pValid = false;
    return *this;
}
