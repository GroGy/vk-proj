//
// Created by Matty on 2021-06-27.
//

#include "../app/ui/windows/UI_PrefabEditor.h"

VKP::UI_PrefabEditor::UI_PrefabEditor() : UI_Window("Prefab editor") {

}

void VKP::UI_PrefabEditor::CustomDraw() {

}

void VKP::UI_PrefabEditor::CustomPostDraw() {
    UI_Window::CustomPostDraw();
}
