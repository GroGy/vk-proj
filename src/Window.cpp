//
// Created by Matty on 10.4.2020.
//

#include <iostream>
#include "../app/render/Window.h"
#include "../app/Exceptions.h"
#include "../app/render/RenderData.h"

const std::string& VKP::Window::GetWindowName()
{
    return m_pWindowName;
}
std::unique_ptr<GLFWwindow,
                VKP::GLFWWindowDestructor>& VKP::Window::GetGlfwWindow()
{
    return m_pGLFWWindow;
}

VKP::WindowBuilder::WindowBuilder(uint32_t width, uint32_t height)
        :m_pWidth(width), m_pHeight(height)
{

}
VKP::Window* VKP::WindowBuilder::finish()
{

    if (!m_pHasColor) {
        throw VKP::InitException("Background color is not set!");
    }

    if (!m_pHasName) {
        throw VKP::InitException("Window name is not set!");
    }

    auto finished = new VKP::Window();
    finished->m_pBackgroundColor = m_pBackgroundColor;
    RenderData::GetInstance()->m_ClearColor = finished->m_pBackgroundColor;
    VKP::Window::m_pWindowWidth = m_pWidth;
    VKP::Window::m_pWindowHeight = m_pHeight;
    finished->m_pWindowName = m_pWindowName;
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);
    GLFWwindow* window = glfwCreateWindow((int32_t) VKP::Window::m_pWindowWidth,
            (int32_t) VKP::Window::m_pWindowHeight, finished->m_pWindowName.c_str(), nullptr,
            nullptr);
    if (window==nullptr) {
        glfwTerminate();
        throw VKP::InitException("Failed to initialize GLFW Window");
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval( 1 );
    glfwSetFramebufferSizeCallback(window, [](GLFWwindow* window, int32_t width, int32_t height) {
      auto& self = *static_cast<Window*>(glfwGetWindowUserPointer(window));
      self.FbSizeCallback(window, width, height);
    });

    finished->m_pGLFWWindow = std::unique_ptr<GLFWwindow, VKP::GLFWWindowDestructor>(window);

    return finished;
}
std::shared_ptr<VKP::WindowBuilder> VKP::WindowBuilder::setBackgroundColor(
        const glm::vec4& bgColor)
{
    this->m_pBackgroundColor = bgColor;
    this->m_pHasColor = true;
    return shared_from_this();
}
std::shared_ptr<VKP::WindowBuilder> VKP::WindowBuilder::setWindowName(
        const std::string& name)
{
    this->m_pHasName = true;
    this->m_pWindowName = name;
    return shared_from_this();
}

void VKP::Window::FbSizeCallback(GLFWwindow* window, int32_t width, int32_t height)
{

    //glViewport(0, 0, width, height);
}

uint32_t VKP::Window::m_pWindowWidth;
uint32_t VKP::Window::m_pWindowHeight;