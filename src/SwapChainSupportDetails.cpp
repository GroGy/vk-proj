//
// Created by Matty on 2021-03-24.
//

#include "../app/render/platform/SwapChainSupportDetails.h"

VkSurfaceFormatKHR VKP::SwapChainSupportDetails::ChooseSwapSurfaceFormat() {
    for (const auto& availableFormat : m_Formats) {
        if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            return availableFormat;
        }
    }

    return m_Formats.at(0);
}

VkPresentModeKHR VKP::SwapChainSupportDetails::ChooseSwapPresentMode() {
    for (const auto& availablePresentMode : m_PresentModes) {
        if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
            return availablePresentMode;
        }
    }

    return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D VKP::SwapChainSupportDetails::ChooseSwapExtent(const std::shared_ptr<VKP::Window> & window) {
    if (m_Capabilities.currentExtent.width != UINT32_MAX) {
        return m_Capabilities.currentExtent;
    } else {
        int width, height;
        glfwGetFramebufferSize(window->GetGlfwWindow().get(), &width, &height);

        VkExtent2D actualExtent = {
                static_cast<uint32_t>(width),
                static_cast<uint32_t>(height)
        };

        actualExtent.width = std::max(m_Capabilities.minImageExtent.width, std::min(m_Capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = std::max(m_Capabilities.minImageExtent.height, std::min(m_Capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
}
