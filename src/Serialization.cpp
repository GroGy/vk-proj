//
// Created by Matty on 2021-04-22.
//

#include "../app/engine/Serialization.h"

std::ostream &VKP_Engine::operator<<(std::ostream &os, const VKP_Engine::ModelVertex &obj) {
    os << obj.m_Pos.x;
    os << obj.m_Pos.y;
    os << obj.m_Pos.z;
    os << obj.m_Color.r;
    os << obj.m_Color.g;
    os << obj.m_Color.b;
    return os;
};

std::istream &VKP_Engine::operator>>(std::istream &is, VKP_Engine::ModelVertex &obj) {
    is >> obj.m_Pos.x;
    is >> obj.m_Pos.y;
    is >> obj.m_Pos.z;
    is >> obj.m_Color.r;
    is >> obj.m_Color.g;
    is >> obj.m_Color.b;
    return is;
};