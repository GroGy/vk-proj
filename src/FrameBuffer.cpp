//
// Created by Matty on 2021-04-22.
//

#include "../app/render/FrameBuffer.h"
#include "../app/Exceptions.h"
#include "../app/render/Renderer.h"

VKP::FrameBufferBlueprintResult VKP::FrameBufferBlueprint::_BuildFrameBuffer() {
    assert(m_pValid);
    VKP::FrameBufferBlueprintResult res{};

    VkFramebufferCreateInfo framebufferInfo{};
    framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferInfo.renderPass = m_pRenderPass;
    framebufferInfo.attachmentCount = static_cast<uint32_t>(m_pAttachments.size());
    framebufferInfo.pAttachments = m_pAttachments.data();
    framebufferInfo.width = m_pWidth;
    framebufferInfo.height = m_pHeight;
    framebufferInfo.layers = 1;

    if (vkCreateFramebuffer(VKP::Renderer::m_Renderer->GetDevice(), &framebufferInfo, nullptr, &res.m_FrameBuffer) !=
        VK_SUCCESS) {
        throw VKP::RuntimeException("failed to create framebuffer!");
    }

    return res;
}

VKP::FrameBufferBlueprint::FrameBufferBlueprint(const VKP::RenderPass &renderPass, uint32_t width, uint32_t height,
                                                std::vector<VkImageView> attachments, uint32_t layers) : m_pHeight(
        height), m_pLayers(layers), m_pWidth(width), m_pRenderPass(renderPass.GetRenderPass()), m_pAttachments(
        std::move(attachments)), m_pValid(true) {

}

bool VKP::FrameBufferBlueprint::IsValid() const {
    return m_pValid;
}

VKP::FrameBuffer::FrameBuffer(VKP::FrameBufferBlueprint blueprint) : m_pValid(true),
                                                                     m_pBlueprint(std::move(blueprint)) {
    Load();
}

void VKP::FrameBuffer::Cleanup() {
    if (m_pValid) {
        vkDestroyFramebuffer(VKP::Renderer::m_Renderer->GetDevice(), m_pFrameBuffer, nullptr);
        m_pMarkedForDeletion = false;
    }
}

void VKP::FrameBuffer::MarkForDeletion() {
    m_pMarkedForDeletion = true;
}

bool VKP::FrameBuffer::MarkedForDeletion() const {
    return m_pMarkedForDeletion;
}

VkFramebuffer VKP::FrameBuffer::GetFrameBuffer() const {
    return m_pFrameBuffer;
}

void VKP::FrameBuffer::Load() {
    m_pFrameBuffer = m_pBlueprint._BuildFrameBuffer().m_FrameBuffer;
}

void VKP::FrameBuffer::Unload() {
    Cleanup();
    m_pMarkedForDeletion = false;
}

void VKP::FrameBuffer::SetSize(uint32_t width, uint32_t height) {
    m_pBlueprint.m_pWidth = width;
    m_pBlueprint.m_pHeight = height;
}
