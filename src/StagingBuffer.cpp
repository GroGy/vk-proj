//
// Created by Matty on 2021-04-05.
//

#include <cassert>
#include "../app/render/StagingBuffer.h"

VKP::StagingBuffer::StagingBuffer(VmaAllocator &allocator, uint64_t size) : m_pValid(true) {
    assert(size > 0);

    VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
    bufferInfo.size = size;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;
    m_pBuffer = VKP::Buffer(allocator, bufferInfo, allocInfo);
}

VKP::StagingBuffer::StagingBuffer(VKP::StagingBuffer &&o) noexcept {
    m_pValid = o.m_pValid;
    m_pBuffer = std::move(o.m_pBuffer);
    o.m_pValid = false;
}

VKP::StagingBuffer &VKP::StagingBuffer::operator=(VKP::StagingBuffer &&o) noexcept {
    m_pValid = o.m_pValid;
    m_pBuffer = std::move(o.m_pBuffer);
    o.m_pValid = false;
    return *this;
}

void VKP::StagingBuffer::Cleanup(VmaAllocator const &allocator) {
    m_pBuffer.Cleanup(allocator);
}

void VKP::StagingBuffer::MarkForDeletion() {
    m_pBuffer.MarkForDeletion();
}

bool VKP::StagingBuffer::MarkedForDeletion() const {
    return m_pBuffer.MarkedForDeletion();
}

void VKP::StagingBuffer::LoadData(const void *data, uint32_t size) {

    if(!m_pValid) {
        VKP_LOG_WARN("Tried to load data to invalid vertex buffer");
        return;
    }
    if(size == 0) return;
    m_pBuffer.LoadData(data,size);
}

VKP::Buffer &VKP::StagingBuffer::GetBuffer() {
    return m_pBuffer;
}
