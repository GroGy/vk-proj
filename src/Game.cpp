//
// Created by Matty on 2021-03-28.
//

#include "../app/game/Game.h"
#include "../app/engine/ModelLoader.h"
#include "../app/input/InputController.h"
#include "../app/render/RenderData.h"
#include "../app/game/data/Game_WorldData.h"
#include "../app/util/UtilMath.h"
#include "../app/game/raycast_callbacks/WorldPickCallback.h"

void VKP_Game::Game::Init(VKP_Game::GameInitData initData) {
    m_pInitData = std::move(initData);
    auto &renderData = Game_RenderData::GetInstance();
    renderData->m_Camera = VKP::Camera();
    auto extent = VKP::Renderer::m_Renderer->GetSwapChainExtent();
    renderData->m_Camera.UpdateScreenRatio((float) extent.width / (float) extent.height);
    auto &input = VKP::InputController::GetInstance();

    input->RegisterKeyInput("FORWARD", {VKP::InputType::VKP_INPUT_KEYBOARD, GLFW_KEY_W});
    input->RegisterKeyInput("BACKWARD", {VKP::InputType::VKP_INPUT_KEYBOARD, GLFW_KEY_S});
    input->RegisterKeyInput("LEFT", {VKP::InputType::VKP_INPUT_KEYBOARD, GLFW_KEY_A});
    input->RegisterKeyInput("RIGHT", {VKP::InputType::VKP_INPUT_KEYBOARD, GLFW_KEY_D});
    input->RegisterKeyInput("RELOAD_SHADERS", {VKP::InputType::VKP_INPUT_KEYBOARD, GLFW_KEY_R});
    input->RegisterKeyInput("MOUSE_LEFT", {VKP::InputType::VKP_INPUT_MOUSE, GLFW_MOUSE_BUTTON_LEFT});
    input->RegisterKeyInput("MOUSE_RIGHT", {VKP::InputType::VKP_INPUT_MOUSE, GLFW_MOUSE_BUTTON_RIGHT});
    input->RegisterKeyInput("JUMP", {VKP::InputType::VKP_INPUT_KEYBOARD, GLFW_KEY_SPACE});
    input->RegisterKeyInput("ACTION", {VKP::InputType::VKP_INPUT_KEYBOARD, GLFW_KEY_F});

    auto &dataInstance = Game_RenderData::GetInstance();

    _LoadBaseModel(dataInstance->m_Cube1x1Model, "assets/models/cube1x1.vk_model");
    _LoadBaseModel(dataInstance->m_CharacterModel, "assets/models/character.vk_model");
    _LoadBaseModel(dataInstance->m_OreModel, "assets/models/Volcanoe_05.vk_model");
    _LoadBaseModel(dataInstance->m_FactoryModel, "assets/models/factory.vk_model");

    LoadWorld();
}

void VKP_Game::Game::Render(VKP::FrameDrawData &frame) {
    auto &renderData = Game_RenderData::GetInstance();
    auto &worldData = Game_WorldData::GetInstance();
    renderData->m_Camera.CalcView();

    frame.viewMatrix = renderData->m_Camera.GetViewMatrix();
    frame.projMatrix = renderData->m_Camera.GetProjMatrix();

    if (worldData->m_World.IsValid())
        Game_WorldData::GetInstance()->m_World.Draw(frame);

    if (worldData->m_Player.IsValid())
        Game_WorldData::GetInstance()->m_Player.Draw(frame);
}

void VKP_Game::Game::Update(float deltaTime) {
    auto &renderData = Game_RenderData::GetInstance();


    if (!m_pRotatingCam && (VKP::InputController::GetInstance()->IsKeyPressed("MOUSE_RIGHT") ||
                            VKP::InputController::GetInstance()->IsKeyPressed("MOUSE_LEFT"))) {
        m_pCamRotStart = m_pCamRot;
        m_pMousePosStart = VKP::InputController::GetInstance()->GetMousePosNormalized();
        m_pRotatingCam = true;
        glfwSetInputMode(m_pInitData.m_MainWindow->GetGlfwWindow().get(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }

    if (m_pRotatingCam) {
        if (VKP::InputController::GetInstance()->IsKeyReleased("MOUSE_RIGHT") &&
            VKP::InputController::GetInstance()->IsKeyReleased("MOUSE_LEFT")) {
            m_pRotatingCam = false;
            glfwSetInputMode(m_pInitData.m_MainWindow->GetGlfwWindow().get(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        } else {
            auto mousePos = VKP::InputController::GetInstance()->GetMousePosNormalized();

            auto mouseDelta = mousePos - m_pMousePosStart;

            m_pCamRot.x = m_pCamRotStart.x - mouseDelta.x * 180;
            m_pCamRot.y = m_pCamRotStart.y + mouseDelta.y * 180;
            if (m_pCamRot.y > 89.5f) {
                m_pCamRotStart.y -= (m_pCamRot.y - 89.5f);
                m_pCamRot.y = 89.5f;
            }
            if (m_pCamRot.y < -89.5f) {
                m_pCamRotStart.y += (m_pCamRot.y - -89.5f);
                m_pCamRot.y = -89.5f;
            }
        }
    }

    glm::vec3 newPos;
    newPos.x = cos(glm::radians(m_pCamRot.x)) * cos(glm::radians(m_pCamRot.y));
    newPos.y = sin(glm::radians(m_pCamRot.x)) * cos(glm::radians(m_pCamRot.y));
    newPos.z = sin(glm::radians(m_pCamRot.y));

    newPos *= m_pCamDist;

    newPos += renderData->m_Camera.GetFocusPosition();

    renderData->m_Camera.SetPosition(newPos);

    if (VKP::InputController::GetInstance()->OnKeyPressed("RELOAD_SHADERS")) {
        vkDeviceWaitIdle(VKP::Renderer::m_Renderer->GetDevice());
        for (auto &&pipeline : VKP::RenderData::GetInstance()->m_Pipelines) {
            pipeline->Refresh();
        }
        for (auto &&shader : VKP::RenderData::GetInstance()->m_ShaderModules) {
            shader.Unload();
        }
    }

    auto & worldData = Game_WorldData::GetInstance();

    renderData->m_Camera.Update(deltaTime);
    if (worldData->m_World.IsValid()) worldData->m_World.Update(deltaTime);
    if (worldData->m_Player.IsValid()) worldData->m_Player.Update(deltaTime);

    if (worldData->m_Player.IsValid()) {
        renderData->m_Camera.SetFocusPosition(worldData->m_Player.GetPosition());
    }

    auto camPos = renderData->m_Camera.GetPosition();
    reactphysics3d::Vector3 startPoint(camPos.x, camPos.y, camPos.z);

    auto worldSpacePos = VKP::InputController::GetInstance()->ScreenToWorldSpace(renderData->m_Camera.GetProjMatrix(),
                                                                                 renderData->m_Camera.GetViewMatrix());

    auto dir = glm::normalize(worldSpacePos - camPos);

    dir *= 1000;

    auto endPos = camPos + dir;

    reactphysics3d::Vector3 endPoint(endPos.x,endPos.y,endPos.z);

    reactphysics3d::Ray ray(startPoint, endPoint);
    WorldPickCallback callback{};
    worldData->m_World.GetPhysicsWorld()->raycast(ray, &callback);

}

void VKP_Game::Game::Cleanup() {
}

std::unique_ptr<VKP_Game::Game> &VKP_Game::Game::GetInstance() {
    if (!s_pInstance) s_pInstance = std::make_unique<VKP_Game::Game>();
    return s_pInstance;
}

void VKP_Game::Game::LoadWorld() {
    auto &worldData = Game_WorldData::GetInstance();
    worldData->m_World = World(-.15f, true);
    worldData->m_Player = std::move(
            Player(worldData->m_World.GetPhysicsWorld(),
                   {0.0, 10.0, 0.0},
                   {0.0, 0.0, 0.0},
                   1)
    );
}

void VKP_Game::Game::_LoadBaseModel(std::shared_ptr<VKP::Model> &model, const std::string &path) {
    auto loadRes = VKP_Engine::ModelLoader::LoadModel(path);

    if (loadRes.first) {
        model = std::move(loadRes.first);
    } else {
        throw VKP::InitException(loadRes.second.value());
    }
}

std::unique_ptr<VKP_Game::Game> VKP_Game::Game::s_pInstance;
