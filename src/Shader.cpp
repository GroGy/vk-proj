//
// Created by Matty on 7.5.2020.
//
#include <iostream>
#include "../app/render/platform/Shader.h"
#include "../app/render/Renderer.h"
#include "../app/util/UtilFile.h"

VKP::Shader::~Shader() {
    _Cleanup();
}

VkShaderModule VKP::Shader::GetModule() const {
    assert(m_pValid);
    return m_pModule;
}

void VKP::Shader::Refresh() {
    if(!m_pValid) {
        VKP_LOG_WARN("Attempted to refresh invalid shader");
        return;
    }
    Unload();
    Load();
}

void VKP::Shader::Load() {
    if(!m_pValid) {
        VKP_LOG_WARN("Attempted to load invalid shader");
        return;
    }

    if (!VKP::UtilFile::FileExists(m_pPath)) {
        throw VKP::InitException(std::string("Shader file not found : " + m_pPath));
    }

    std::ifstream file(m_pPath, std::ios::ate | std::ios::binary);

    if (!file.is_open()) {
        throw VKP::InitException(std::string("Failed to open shader file : "  + m_pPath));
    }

    size_t fileSize = (size_t) file.tellg();
    std::vector<char> buffer(fileSize);
    file.seekg(0);
    file.read(buffer.data(), fileSize);
    file.close();

    auto & renderer = VKP::Renderer::m_Renderer;

    VkShaderModuleCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = buffer.size();
    createInfo.pCode = reinterpret_cast<const uint32_t*>(buffer.data());

    if (vkCreateShaderModule(renderer->GetDevice(), &createInfo, nullptr, &m_pModule) != VK_SUCCESS) {
        throw VKP::InitException(std::string("Failed to create shader : " + m_pPath));
    }
    m_pLoaded = true;
    VKP_LOG_INFO("Loaded shader : {}",m_pPath);
}

void VKP::Shader::_Cleanup() {
    if(!m_pValid) return;
    if(!m_pLoaded) return;
    auto & renderer = VKP::Renderer::m_Renderer;
    vkDestroyShaderModule(renderer->GetDevice(), m_pModule, nullptr);
    m_pLoaded = false;
    VKP_LOG_INFO("Unloaded shader : {}",m_pPath);
}

void VKP::Shader::Unload() {
    if(!m_pValid) {
        VKP_LOG_WARN("Attempted to unload invalid shader");
        return;
    }
    _Cleanup();
}

VKP::Shader::Shader(VKP::Shader &&o) {
    m_pPath = o.m_pPath;
    m_pValid = true;
    o.m_pValid = false;
    m_pModule = o.m_pModule;
    m_pLoaded = o.m_pLoaded;
    m_pID = o.m_pID;
}

uint64_t VKP::Shader::_GetIDInternal() {
    static uint64_t id = 0;
    return id++;
}

uint64_t VKP::Shader::GetID() const {
    return m_pID;
}
