//
// Created by Matty on 29.8.2020.
//
#include "../app/render/Renderer.h"
#include "../app/Exceptions.h"
#include "../app/Log.h"
#include "../app/render/RenderData.h"
#include "../app/engine/ModelVertex.h"

#define GLFW_EXPOSE_NATIVE_WIN32

#include <GLFW/glfw3native.h>
#include <set>
#include <iostream>

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
        void *pUserData);


void VKP::Renderer::BeginFrame() {
    vkWaitForFences(m_pDevice, 1, &m_pInFlightFences[m_pCurrentFrame], VK_TRUE, UINT64_MAX);

    VkResult result = vkAcquireNextImageKHR(m_pDevice, m_pSwapChain, UINT64_MAX,
                                            m_pImageAvailableSemaphores[m_pCurrentFrame], VK_NULL_HANDLE,
                                            &m_pCurrentImageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR) {
        _RebuildSwapChain();
        return;
    } else if (result != VK_SUCCESS) {
        throw VKP::RuntimeException("failed to acquire swap chain image!");
    }

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = 0; // Optional
    beginInfo.pInheritanceInfo = nullptr; // Optional

    if (vkBeginCommandBuffer(m_pCommandBuffers.at(m_pCurrentImageIndex), &beginInfo) != VK_SUCCESS) {
        throw VKP::RuntimeException("failed to begin recording command buffer!");
    }

    VkRenderPassBeginInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = m_pCameraRenderPass.GetRenderPass();
    renderPassInfo.framebuffer = m_pSwapChainFrameBuffers.at(m_pCurrentImageIndex).GetFrameBuffer();
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = m_pSwapChainExtent;

    auto bgColor = RenderData::GetInstance()->m_ClearColor;

    std::array<VkClearValue, 2> clearValues{};

    clearValues.at(0).color = {bgColor.r, bgColor.g, bgColor.b, bgColor.a};
    clearValues.at(1).depthStencil = {1.0f, 0};

    renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
    renderPassInfo.pClearValues = clearValues.data();
}

void VKP::Renderer::FinishFrame() {
    /*
      vkCmdEndRenderPass(m_pCommandBuffers.at(m_pCurrentImageIndex));
     */
    vkWaitForFences(m_pDevice, 1, &m_pInFlightFences[m_pCurrentFrame], VK_TRUE, UINT64_MAX);
    if (vkEndCommandBuffer(m_pCommandBuffers.at(m_pCurrentImageIndex)) != VK_SUCCESS) {
        throw VKP::RuntimeException("failed to record command buffer!");
    }
}

void VKP::Renderer::Update() {
    if(m_pFirstCleanup) {
        m_pLastCleanup = glfwGetTime();
        m_pFirstCleanup = false;
    }

    auto now = glfwGetTime();

    if(now - m_pLastCleanup > 10.0) {
        m_pLastCleanup = now;
        vkDeviceWaitIdle(m_pDevice);
        auto & data = RenderData::GetInstance();
        for(auto && it = data->m_Textures.cbegin() ; it != data->m_Textures.end();) {
            if(it.base()->get()->MarkedForDeletion()) {
                it.base()->get()->Cleanup();
                it = data->m_Textures.erase(it);
            } else {
                ++it;
            }
        }
        for(auto && it = data->m_StagingBuffers.cbegin() ; it != data->m_StagingBuffers.end();) {
            if(it.base()->get()->MarkedForDeletion()) {
                it.base()->get()->Cleanup(m_pAllocator);
                it = data->m_StagingBuffers.erase(it);
            } else {
                ++it;
            }
        }
        for(auto && it = data->m_VertexBuffers.cbegin() ; it != data->m_VertexBuffers.end();) {
            if(it.base()->get()->MarkedForDeletion()) {
                it.base()->get()->Cleanup(m_pAllocator);
                it = data->m_VertexBuffers.erase(it);
            } else {
                ++it;
            }
        }
        for(auto && it = data->m_IndexBuffers.cbegin() ; it != data->m_IndexBuffers.end();) {
            if(it.base()->get()->MarkedForDeletion()) {
                it.base()->get()->Cleanup(m_pAllocator);
                it = data->m_IndexBuffers.erase(it);
            } else {
                ++it;
            }
        }
    }
}

VKP::Renderer::Renderer() {
    glfwInit();
}

void VKP::Renderer::PresentRender() {
    // Check if a previous frame is using this image (i.e. there is its fence to wait on)
    if (m_pImagesInFlight[m_pCurrentImageIndex] != VK_NULL_HANDLE) {
        vkWaitForFences(m_pDevice, 1, &m_pImagesInFlight[m_pCurrentImageIndex], VK_TRUE, 100000000);
    }
    // Mark the image as now being in use by this frame
    m_pImagesInFlight[m_pCurrentImageIndex] = m_pInFlightFences[m_pCurrentFrame];

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkSemaphore waitSemaphores[] = {m_pImageAvailableSemaphores[m_pCurrentFrame]};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;

    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &m_pCommandBuffers[m_pCurrentImageIndex];

    VkSemaphore signalSemaphores[] = {m_pRenderFinishedSemaphores[m_pCurrentFrame]};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    vkResetFences(m_pDevice, 1, &m_pInFlightFences[m_pCurrentFrame]);

    if (vkQueueSubmit(m_pGraphicsQueue, 1, &submitInfo, m_pInFlightFences[m_pCurrentFrame]) != VK_SUCCESS) {
        throw VKP::RuntimeException("failed to submit draw command buffer!");
    }

    VkPresentInfoKHR presentInfo{};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;

    VkSwapchainKHR swapChains[] = {m_pSwapChain};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &m_pCurrentImageIndex;
    presentInfo.pResults = nullptr; // Optional
    VkResult result2 = vkQueuePresentKHR(m_pPresentQueue, &presentInfo);

    if (result2 == VK_ERROR_OUT_OF_DATE_KHR || result2 == VK_SUBOPTIMAL_KHR) {
        _RebuildSwapChain();
    } else if (result2 != VK_SUCCESS) {
        throw VKP::RuntimeException("failed to present swap chain image!");
    }

    m_pCurrentFrame = (m_pCurrentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void VKP::Renderer::SetWireframeMode(bool wireframeActive) {
    /* FIXME: VK
    if (wireframeActive) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
     */
}

VKP::Renderer::~Renderer() {
}

void VKP::Renderer::Init(const std::shared_ptr<VKP::Window> &window) {
    _InitInstance(window);
    glfwGetWindowSize(window->GetGlfwWindow().get(), &m_pWindowWidth, &m_pWindowHeight);
}

void VKP::Renderer::Cleanup() {
    vkDeviceWaitIdle(m_pDevice);
#ifndef NDEBUG
    _DestroyDebugUtilsMessengerEXT(m_pInstance, m_pDebugMessenger, nullptr);
#endif
    for (auto &&buf : RenderData::GetInstance()->m_VertexBuffers) {
        buf->MarkForDeletion();
    }

    for (auto &&buf : RenderData::GetInstance()->m_VertexBuffers) {
        if (buf->MarkedForDeletion()) buf->Cleanup(m_pAllocator);
    }

    RenderData::GetInstance()->m_VertexBuffers.clear();

    for (auto &&buf : RenderData::GetInstance()->m_IndexBuffers) {
        buf->MarkForDeletion();
    }

    for (auto &&buf : RenderData::GetInstance()->m_IndexBuffers) {
        if (buf->MarkedForDeletion()) buf->Cleanup(m_pAllocator);
    }

    RenderData::GetInstance()->m_IndexBuffers.clear();

    for (auto &&buf : RenderData::GetInstance()->m_UniformBuffers) {
        buf->MarkForDeletion();
    }

    for (auto &&buf : RenderData::GetInstance()->m_UniformBuffers) {
        if (buf->MarkedForDeletion()) buf->Cleanup(m_pAllocator);
    }

    RenderData::GetInstance()->m_UniformBuffers.clear();

    for (auto &&buf : RenderData::GetInstance()->m_StagingBuffers) {
        buf->MarkForDeletion();
    }

    for (auto &&buf : RenderData::GetInstance()->m_StagingBuffers) {
        if (buf->MarkedForDeletion()) buf->Cleanup(m_pAllocator);
    }

    RenderData::GetInstance()->m_StagingBuffers.clear();

    for (auto &&img : RenderData::GetInstance()->m_Textures) {
        img->MarkForDeletion();
    }

    for (auto &&img : RenderData::GetInstance()->m_Textures) {
        if (img->MarkedForDeletion()) img->Cleanup();
    }

    RenderData::GetInstance()->m_Textures.clear();

    m_pSampler->Unload();

    vkDestroyDescriptorPool(m_pDevice, m_pImguiDescPool, nullptr);

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(m_pDevice, m_pRenderFinishedSemaphores[i], nullptr);
        vkDestroySemaphore(m_pDevice, m_pImageAvailableSemaphores[i], nullptr);
        vkDestroyFence(m_pDevice, m_pInFlightFences[i], nullptr);
    }
    for (auto &&pipeline : RenderData::GetInstance()->m_Pipelines) {
        pipeline->Unload();
    }
    for (auto &&shader : RenderData::GetInstance()->m_ShaderModules) {
        shader.Unload();
    }
    for (auto &&framebuffer : m_pSwapChainFrameBuffers) {
        framebuffer.MarkForDeletion();
        framebuffer.Cleanup();
    }
    for (auto imageView : m_pSwapChainImageViews) {
        vkDestroyImageView(m_pDevice, imageView, nullptr);
    }
    m_pDepthTexture.Cleanup();
    vmaDestroyAllocator(m_pAllocator);
    vkFreeCommandBuffers(m_pDevice, m_pCommandPoll, static_cast<uint32_t>(m_pCommandBuffers.size()),
                         m_pCommandBuffers.data());
    vkDestroyCommandPool(m_pDevice, m_pCommandPoll, nullptr);
    m_pCameraRenderPass.Cleanup();
    vkDestroySwapchainKHR(m_pDevice, m_pSwapChain, nullptr);
    vkDestroyDevice(m_pDevice, nullptr);
    vkDestroySurfaceKHR(m_pInstance, m_pSurface, nullptr);
    /////////////////// LAST ///////////////////
    if (m_pInstance) vkDestroyInstance(m_pInstance, nullptr);
    glfwTerminate();
}

void VKP::Renderer::_InitInstance(const std::shared_ptr<VKP::Window> &window) {
    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "VKP";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "VKP";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_2;

    m_pMainWindow = window;

    _InitInstanceInternal(&appInfo);
#ifndef NDEBUG
    _InitDebuggingInternal();
#endif
    _InitSurfaceInternal(window);
    _InitPhysicalDevicesInternal();
    _InitLogicalDevicesInternal();
    _InitAllocatorInternal();
    _InitSwapChainInternal();
    _InitImageViewsInternal();
    m_pDepthTexture = VKP::DepthTexture(m_pAllocator);
    _InitMainRenderPasses();
    _InitFrameBuffersInternal();
    _InitCommandPollInternal();
    _InitCommandBuffersInternal();
    _InitSyncInternal();

    m_pSampler = std::make_shared<VKP::TextureSampler>();
    auto indices = _FindQueueFamiliesInternal(m_pPhysicalDevice);
    VKP_LOG_DEBUG("Initialized renderer");
}

void VKP::Renderer::_InitInstanceInternal(VkApplicationInfo *appInfo) {
    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = appInfo;

    auto extensionsR = _GetRequiredExtensionsInternal();

    createInfo.enabledExtensionCount = static_cast<uint32_t>(extensionsR.size());
    createInfo.ppEnabledExtensionNames = extensionsR.data();



#ifndef NDEBUG
    createInfo.enabledLayerCount = static_cast<uint32_t>(m_pValidationLayers.size());
    createInfo.ppEnabledLayerNames = m_pValidationLayers.data();
#else
    createInfo.enabledLayerCount = 0;
#endif
    auto res = vkCreateInstance(&createInfo, nullptr, &m_pInstance);
    if (res != VK_SUCCESS) {
        throw VKP::InitException("Failed to create Vulkan instance");
    }

    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    for (const VkLayerProperties& layerProperties : availableLayers)
    {
        std::cout << layerProperties.layerName << std::endl;
    }
    //VKP_LOG_DEBUG("Initialized instance");
}

//region VALIDATION_LAYERS
#ifndef NDEBUG

bool VKP::Renderer::_CheckValidationLayerSupport() {
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    for (const char *layerName : m_pValidationLayers) {
        bool layerFound = false;

        for (const auto &layerProperties : availableLayers) {
            if (strcmp(layerName, layerProperties.layerName) == 0) {
                layerFound = true;
                break;
            }
        }

        if (!layerFound) {
            return false;
        }
    }

    return true;
}

VkResult
VKP::Renderer::_CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo,
                                             const VkAllocationCallbacks *pAllocator,
                                             VkDebugUtilsMessengerEXT *pDebugMessenger) {
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr) {
        return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    } else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void VKP::Renderer::_DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger,
                                                   const VkAllocationCallbacks *pAllocator) {
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance,
                                                                            "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) {
        func(instance, debugMessenger, pAllocator);
    }
}

void VKP::Renderer::_InitDebuggingInternal() {
    VkDebugUtilsMessengerCreateInfoEXT createInfoDBG{};
    createInfoDBG.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfoDBG.messageSeverity =
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfoDBG.messageType =
            VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfoDBG.pfnUserCallback = debugCallback;
    createInfoDBG.pUserData = nullptr; // Optional

    if (_CreateDebugUtilsMessengerEXT(m_pInstance, &createInfoDBG, nullptr, &m_pDebugMessenger) != VK_SUCCESS) {
        throw VKP::InitException("failed to set up debug messenger!");
    }
    //VKP_LOG_DEBUG("Initialized debugging messenger");
}

#endif

//endregion

std::vector<const char *> VKP::Renderer::_GetRequiredExtensionsInternal() {
    uint32_t glfwExtensionCount = 0;
    const char **glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char *> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);
#ifndef NDEBUG
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif
    return extensions;
}

void VKP::Renderer::_InitPhysicalDevicesInternal() {
    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(m_pInstance, &deviceCount, nullptr);
    if (deviceCount == 0) {
        throw VKP::InitException("No GPUs Are available!");
    }
    std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
    vkEnumeratePhysicalDevices(m_pInstance, &deviceCount, physicalDevices.data());
    if (deviceCount == 0) {
        throw VKP::InitException("Fetched 0 gpu datas");
    }
    for (const auto &device : physicalDevices) {
        if (_IsDeviceSuitable(device)) {
            m_pPhysicalDevice = device;
            VkPhysicalDeviceProperties deviceProperties{};
            vkGetPhysicalDeviceProperties(device, &deviceProperties);
            VKP_LOG_INFO("Selected GPU : {}", deviceProperties.deviceName);
            break;
        }
    }

    if (m_pPhysicalDevice == VK_NULL_HANDLE) {
        throw VKP::InitException("failed to find a suitable GPU!");
    }
    //VKP_LOG_DEBUG("Initialized physical device");
}

bool VKP::Renderer::_IsDeviceSuitable(VkPhysicalDevice device) {
    VKP::QueueFamilies indices = _FindQueueFamiliesInternal(device);

    auto extensionsSupported = _CheckDeviceExtensionSupport(device);

    bool swapChainSupported = false;

    if (extensionsSupported) {
        VKP::SwapChainSupportDetails swapChainSupport = _GetSwapChainSupport(device);
        swapChainSupported = !swapChainSupport.m_Formats.empty() && !swapChainSupport.m_PresentModes.empty();
    }

    VkPhysicalDeviceFeatures supportedFeatures;
    vkGetPhysicalDeviceFeatures(device, &supportedFeatures);

    return indices.IsComplete() && extensionsSupported && swapChainSupported && supportedFeatures.samplerAnisotropy;
}

VKP::QueueFamilies VKP::Renderer::_FindQueueFamiliesInternal(VkPhysicalDevice device) {
    VKP::QueueFamilies indices;

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

    int i = 0;
    for (const auto &queueFamily : queueFamilies) {
        if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            indices.m_pGraphicsFamily = i;
        }
        VkBool32 presentSupport = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, m_pSurface, &presentSupport);

        if (presentSupport) {
            indices.m_pPresentFamily = i;
        }

        if (indices.IsComplete()) {
            break;
        }

        i++;
    }

    return indices;
}

void VKP::Renderer::_InitLogicalDevicesInternal() {
    VKP::QueueFamilies indices = _FindQueueFamiliesInternal(m_pPhysicalDevice);

    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    std::set<uint32_t> uniqueQueueFamilies = {
            indices.m_pGraphicsFamily.value(), indices.m_pPresentFamily.value()
    };

    float queuePriority = 1.0f;

    for (uint32_t queueFamily : uniqueQueueFamilies) {
        VkDeviceQueueCreateInfo queueCreateInfo{};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = indices.m_pGraphicsFamily.value();
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    VkPhysicalDeviceFeatures deviceFeatures{};
    deviceFeatures.samplerAnisotropy = VK_TRUE;

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = queueCreateInfos.data();
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pEnabledFeatures = &deviceFeatures;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(m_pRequiredExtensions.size());
    createInfo.ppEnabledExtensionNames = m_pRequiredExtensions.data();

#ifndef NDEBUG
    createInfo.enabledLayerCount = static_cast<uint32_t>(m_pValidationLayers.size());
    createInfo.ppEnabledLayerNames = m_pValidationLayers.data();
#else
    createInfo.enabledLayerCount = 0;
#endif

    if (vkCreateDevice(m_pPhysicalDevice, &createInfo, nullptr, &m_pDevice) != VK_SUCCESS) {
        throw VKP::InitException("failed to create logical device!");
    }

    vkGetDeviceQueue(m_pDevice, indices.m_pGraphicsFamily.value(), 0, &m_pGraphicsQueue);
    vkGetDeviceQueue(m_pDevice, indices.m_pPresentFamily.value(), 0, &m_pPresentQueue);

    //VKP_LOG_DEBUG("Initialized logical device");
}

void VKP::Renderer::_InitSurfaceInternal(const std::shared_ptr<VKP::Window> &window) {
    if (glfwCreateWindowSurface(m_pInstance, window->GetGlfwWindow().get(), nullptr, &m_pSurface) != VK_SUCCESS) {
        throw VKP::InitException("failed to create window surface!");
    }

    //VKP_LOG_DEBUG("Initialized surface");
}

bool VKP::Renderer::_CheckDeviceExtensionSupport(VkPhysicalDevice device) {
    uint32_t extensionCount;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

    std::vector<VkExtensionProperties> availableExtensions(extensionCount);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

    std::set<std::string> requiredExtensions(m_pRequiredExtensions.begin(), m_pRequiredExtensions.end());

    for (const auto &extension : availableExtensions) {
        requiredExtensions.erase(extension.extensionName);
    }

    return requiredExtensions.empty();
}

VKP::SwapChainSupportDetails VKP::Renderer::_GetSwapChainSupport(VkPhysicalDevice device) {
    VKP::SwapChainSupportDetails details;

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, m_pSurface, &details.m_Capabilities);

    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, m_pSurface, &formatCount, nullptr);

    if (formatCount != 0) {
        details.m_Formats.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, m_pSurface, &formatCount, details.m_Formats.data());
    }

    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, m_pSurface, &presentModeCount, nullptr);

    if (presentModeCount != 0) {
        details.m_PresentModes.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, m_pSurface, &presentModeCount, details.m_PresentModes.data());
    }

    return details;
}

void VKP::Renderer::_InitSwapChainInternal() {
    VKP::SwapChainSupportDetails swapChainSupport = _GetSwapChainSupport(m_pPhysicalDevice);

    m_pSurfaceFormat = swapChainSupport.ChooseSwapSurfaceFormat();
    m_pSurfacePresentMode = swapChainSupport.ChooseSwapPresentMode();
    m_pSurfaceExtent = swapChainSupport.ChooseSwapExtent(m_pMainWindow);

    uint32_t imageCount = swapChainSupport.m_Capabilities.minImageCount + 2;
    if (swapChainSupport.m_Capabilities.maxImageCount > 0 &&
        imageCount > swapChainSupport.m_Capabilities.maxImageCount) {
        imageCount = swapChainSupport.m_Capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = m_pSurface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = m_pSurfaceFormat.format;
    createInfo.imageColorSpace = m_pSurfaceFormat.colorSpace;
    createInfo.imageExtent = m_pSurfaceExtent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    VKP::QueueFamilies indices = _FindQueueFamiliesInternal(m_pPhysicalDevice);
    uint32_t queueFamilyIndices[] = {indices.m_pGraphicsFamily.value(), indices.m_pPresentFamily.value()};

    if (indices.m_pGraphicsFamily != indices.m_pPresentFamily) {
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    } else {
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0; // Optional
        createInfo.pQueueFamilyIndices = nullptr; // Optional
    }

    createInfo.preTransform = swapChainSupport.m_Capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = m_pSurfacePresentMode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = VK_NULL_HANDLE;

    if (vkCreateSwapchainKHR(m_pDevice, &createInfo, nullptr, &m_pSwapChain) != VK_SUCCESS) {
        throw VKP::InitException("failed to create swap chain!");
    }

    m_pSwapChainImageFormat = m_pSurfaceFormat.format;
    m_pSwapChainExtent = m_pSurfaceExtent;

    vkGetSwapchainImagesKHR(m_pDevice, m_pSwapChain, &imageCount, nullptr);
    m_pSwapChainImages.resize(imageCount);
    vkGetSwapchainImagesKHR(m_pDevice, m_pSwapChain, &imageCount, m_pSwapChainImages.data());
    //VKP_LOG_DEBUG("Initialized SwapChain");
}

void VKP::Renderer::_InitImageViewsInternal() {
    m_pSwapChainImageViews.resize(m_pSwapChainImages.size());

    for (size_t i = 0; i < m_pSwapChainImages.size(); i++) {
        VkImageViewCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = m_pSwapChainImages[i];
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = m_pSwapChainImageFormat;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;

        if (vkCreateImageView(m_pDevice, &createInfo, nullptr, &m_pSwapChainImageViews[i]) != VK_SUCCESS) {
            throw VKP::InitException("failed to create image views!");
        }
    }
    //VKP_LOG_DEBUG("Initialized ImageViews");
}

void VKP::Renderer::_InitGraphicsPipelinesInternal() {
    auto &renderData = RenderData::GetInstance();
    for (auto &&pipeline : renderData->m_Pipelines) {
        pipeline->Load();
    }

    for (auto &&shader : renderData->m_ShaderModules) {
        shader.Unload();
    }
}

VkDevice VKP::Renderer::GetDevice() const {
    return m_pDevice;
}

VkExtent2D VKP::Renderer::GetSwapChainExtent() const {
    return m_pSwapChainExtent;
}

void VKP::Renderer::_InitMainRenderPasses() {
    //region Main RenderPass

    auto colorAttachment = VKP::AttachmentBlueprint(m_pSwapChainImageFormat);
    colorAttachment.m_SampleCount = 1;
    colorAttachment.m_Layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    auto mainSubpass = VKP::SubPassBlueprint(colorAttachment);

    auto depthAttachment = VKP::AttachmentBlueprint(m_pDepthTexture.GetFormat());
    depthAttachment.m_SampleCount = 1;
    depthAttachment.m_FinalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    depthAttachment.m_StoreOP = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.m_Layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    mainSubpass.SetDepthAttachment(depthAttachment);
    auto blueprint = VKP::RenderPassBlueprint();
    blueprint.AddSubPass(mainSubpass);

    m_pCameraRenderPass = VKP::RenderPass(blueprint);
    //endregion

    //VKP_LOG_DEBUG("Initialized render passes");
}

VkRenderPass VKP::Renderer::GetMainRenderPass() const {
    return m_pCameraRenderPass.GetRenderPass();
}

void VKP::Renderer::_InitFrameBuffersInternal() {
    m_pSwapChainFrameBuffers.resize(m_pSwapChainImageViews.size());

    for (size_t i = 0; i < m_pSwapChainImageViews.size(); i++) {
        //const VKP::RenderPass &renderPass, uint32_t width, uint32_t height,std::vector<VkImageView> attachments, uint32_t layers = 1
        VKP::FrameBufferBlueprint bp{m_pCameraRenderPass, m_pSwapChainExtent.width, m_pSwapChainExtent.height,
                                     {m_pSwapChainImageViews[i],m_pDepthTexture.GetView()}};
        m_pSwapChainFrameBuffers.at(i) = VKP::FrameBuffer(bp);
    }

    //VKP_LOG_DEBUG("Initialized frame buffers");
}

void VKP::Renderer::_InitCommandPollInternal() {
    VKP::QueueFamilies queueFamilyIndices = _FindQueueFamiliesInternal(m_pPhysicalDevice);

    VkCommandPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndices.m_pGraphicsFamily.value();
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    if (vkCreateCommandPool(m_pDevice, &poolInfo, nullptr, &m_pCommandPoll) != VK_SUCCESS) {
        throw VKP::InitException("failed to create command pool!");
    }

    //VKP_LOG_DEBUG("Initialized command poll");
}

void VKP::Renderer::_InitCommandBuffersInternal() {
    m_pCommandBuffers.resize(m_pSwapChainFrameBuffers.size());

    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = m_pCommandPoll;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = (uint32_t) m_pCommandBuffers.size();

    if (vkAllocateCommandBuffers(m_pDevice, &allocInfo, m_pCommandBuffers.data()) != VK_SUCCESS) {
        throw VKP::InitException("failed to allocate command buffers!");
    }

    //VKP_LOG_DEBUG("Initialized command buffers");
}

void VKP::Renderer::_InitSyncInternal() {
    m_pImageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    m_pRenderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    m_pInFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
    m_pImagesInFlight.resize(m_pSwapChainImages.size(), VK_NULL_HANDLE);

    VkSemaphoreCreateInfo semaphoreInfo{};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if (vkCreateSemaphore(m_pDevice, &semaphoreInfo, nullptr, &m_pImageAvailableSemaphores[i]) != VK_SUCCESS ||
            vkCreateSemaphore(m_pDevice, &semaphoreInfo, nullptr, &m_pRenderFinishedSemaphores[i]) != VK_SUCCESS ||
            vkCreateFence(m_pDevice, &fenceInfo, nullptr, &m_pInFlightFences[i]) != VK_SUCCESS) {

            throw VKP::InitException("failed to create semaphores!");
        }
    }

    //VKP_LOG_DEBUG("Initialized semaphores");
}

void VKP::Renderer::_RebuildSwapChain() {
    int width = 0, height = 0;
    glfwGetFramebufferSize(m_pMainWindow->GetGlfwWindow().get(), &width, &height);
    while (width == 0 || height == 0) {
        glfwGetFramebufferSize(m_pMainWindow->GetGlfwWindow().get(), &width, &height);
        glfwWaitEvents();
    }

    vkDeviceWaitIdle(m_pDevice);
    // Unload old one
    for (size_t i = 0; i < m_pSwapChainFrameBuffers.size(); i++) {
        m_pSwapChainFrameBuffers.at(i).Cleanup();
    }

    m_pDepthTexture.Cleanup();

    vkFreeCommandBuffers(m_pDevice, m_pCommandPoll, static_cast<uint32_t>(m_pCommandBuffers.size()),
                         m_pCommandBuffers.data());

    RenderData::GetInstance()->m_Pipelines.at(0)->Unload();

    m_pCameraRenderPass.Unload();

    for (size_t i = 0; i < m_pSwapChainImageViews.size(); i++) {
        vkDestroyImageView(m_pDevice, m_pSwapChainImageViews[i], nullptr);
    }
    vkDestroySwapchainKHR(m_pDevice, m_pSwapChain, nullptr);

    // Load new one

    _InitSwapChainInternal();
    _InitImageViewsInternal();
    m_pCameraRenderPass.Load();
    _InitGraphicsPipelinesInternal();
    m_pDepthTexture = VKP::DepthTexture(m_pAllocator);
    _InitFrameBuffersInternal();
    _InitCommandBuffersInternal();
    //TODO: Uniform reload on all pipelines
    m_pImagesInFlight.resize(m_pSwapChainImages.size(), VK_NULL_HANDLE);
}

void VKP::Renderer::_InitAllocatorInternal() {
    VmaAllocatorCreateInfo allocatorInfo = {};
    allocatorInfo.vulkanApiVersion = VK_API_VERSION_1_2;
    allocatorInfo.physicalDevice = m_pPhysicalDevice;
    allocatorInfo.device = m_pDevice;
    allocatorInfo.instance = m_pInstance;

    auto res = vmaCreateAllocator(&allocatorInfo, &m_pAllocator);
    assert(res == VK_SUCCESS);
}

ImGui_ImplVulkan_InitInfo VKP::Renderer::CreateImGuiInitInfo() {
    VKP::SwapChainSupportDetails swapChainSupport = _GetSwapChainSupport(m_pPhysicalDevice);

    VkDescriptorPoolSize pool_sizes[] =
            {
                    {VK_DESCRIPTOR_TYPE_SAMPLER,                1000},
                    {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000},
                    {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,          1000},
                    {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,          1000},
                    {VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER,   1000},
                    {VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER,   1000},
                    {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,         1000},
                    {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,         1000},
                    {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000},
                    {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000},
                    {VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,       1000}
            };

    VkDescriptorPoolCreateInfo pool_info = {};
    pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    pool_info.maxSets = 1000;
    pool_info.poolSizeCount = std::size(pool_sizes);
    pool_info.pPoolSizes = pool_sizes;

    vkCreateDescriptorPool(m_pDevice, &pool_info, nullptr, &m_pImguiDescPool);

    ImGui_ImplVulkan_InitInfo init_info = {};
    init_info.Instance = m_pInstance;
    init_info.PhysicalDevice = m_pPhysicalDevice;
    init_info.Device = m_pDevice;
    init_info.Queue = m_pGraphicsQueue;
    init_info.DescriptorPool = m_pImguiDescPool;
    init_info.MinImageCount = swapChainSupport.m_Capabilities.minImageCount;
    init_info.ImageCount = m_pSwapChainImages.size();
    return init_info;
}

void VKP::Renderer::_checkVkResult(VkResult res) {

}

void VKP::Renderer::BeginMainRenderPass() {
    VkRenderPassBeginInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = m_pCameraRenderPass.GetRenderPass();
    renderPassInfo.framebuffer = m_pSwapChainFrameBuffers.at(m_pCurrentImageIndex).GetFrameBuffer();
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = m_pSwapChainExtent;

    auto bgColor = RenderData::GetInstance()->m_ClearColor;

    std::array<VkClearValue, 2> clearValues{};

    clearValues.at(0).color = {bgColor.r, bgColor.g, bgColor.b, bgColor.a};
    clearValues.at(1).depthStencil = {1.0f, 0};

    renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
    renderPassInfo.pClearValues = clearValues.data();

    vkCmdBeginRenderPass(m_pCommandBuffers.at(m_pCurrentImageIndex), &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
}

void VKP::Renderer::EndMainRenderPass() {
    vkCmdEndRenderPass(m_pCommandBuffers.at(m_pCurrentImageIndex));
}

void VKP::Renderer::WaitIdle() {
    vkDeviceWaitIdle(m_pDevice);
}

std::shared_ptr<VKP::VertexBuffer> &VKP::Renderer::CreateVertexBuffer(uint64_t size) {
    return RenderData::GetInstance()->m_VertexBuffers.emplace_back(
            std::make_shared<VKP::VertexBuffer>(m_pAllocator, size));
}

std::shared_ptr<VKP::IndexBuffer> &VKP::Renderer::CreateIndexBuffer(uint32_t count) {
    return RenderData::GetInstance()->m_IndexBuffers.emplace_back(
            std::make_shared<VKP::IndexBuffer>(m_pAllocator, count));
}

std::shared_ptr<VKP::UniformBuffer> &VKP::Renderer::CreateUniformBuffer(uint64_t size) {
    return RenderData::GetInstance()->m_UniformBuffers.emplace_back(
            std::make_shared<VKP::UniformBuffer>(m_pAllocator, size));
}

uint32_t VKP::Renderer::GetSwapImageCount() const {
    return m_pSwapChainImages.size();
}

uint32_t VKP::Renderer::GetCurrentImageIndex() const {
    return m_pCurrentImageIndex;
}

VKP::FrameDrawData VKP::Renderer::GetFrameDrawData() {
    return VKP::FrameDrawData(m_pCommandBuffers.at(m_pCurrentImageIndex), m_pCurrentImageIndex);
}

VkPhysicalDevice VKP::Renderer::GetPhysicalDevice() const {
    return m_pPhysicalDevice;
}

VkCommandBuffer VKP::Renderer::BeginSingleCommandBuffer() {
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = m_pCommandPoll;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(m_pDevice, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    return commandBuffer;
}

void VKP::Renderer::EndSingleCommandBuffer(VkCommandBuffer commandBuffer) {
    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    vkQueueSubmit(m_pGraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(m_pGraphicsQueue);

    vkFreeCommandBuffers(m_pDevice, m_pCommandPoll, 1, &commandBuffer);
}

void VKP::Renderer::CopyBuffer(VKP::Buffer &srcBuffer, VKP::Buffer &dstBuffer, VkDeviceSize size) {
    VkCommandBuffer commandBuffer = BeginSingleCommandBuffer();

    VkBufferCopy copyRegion{};
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer.GetVulkanBuffer(), dstBuffer.GetVulkanBuffer(), 1, &copyRegion);

    EndSingleCommandBuffer(commandBuffer);
}

std::shared_ptr<VKP::Texture> &
VKP::Renderer::CreateTexture(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
                             VkImageUsageFlags usage, bool createImageView) {
    return RenderData::GetInstance()->m_Textures.emplace_back(
            std::make_shared<VKP::Texture>(m_pAllocator,width,height,format,tiling,usage,createImageView));
}

std::shared_ptr<VKP::StagingBuffer> &VKP::Renderer::CreateStagingBuffer(uint64_t size) {
    return RenderData::GetInstance()->m_StagingBuffers.emplace_back(std::make_shared<VKP::StagingBuffer>(m_pAllocator, size));
}

void VKP::Renderer::TransitionTextureLayout(VKP::Texture &texture, VkFormat format, VkImageLayout oldLayout,
                                            VkImageLayout newLayout) {
    auto cmdBuf = BeginSingleCommandBuffer();

    VkImageMemoryBarrier barrier{};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;

    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

    barrier.image = texture.GetImage();
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    barrier.srcAccessMask = 0; // TODO
    barrier.dstAccessMask = 0; // TODO

    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;

    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else {
        throw std::invalid_argument("unsupported layout transition!");
    }

    vkCmdPipelineBarrier(
            cmdBuf,
            sourceStage, destinationStage,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier
    );

    EndSingleCommandBuffer(cmdBuf);
}

void
VKP::Renderer::CopyBufferToTexture(VKP::Buffer &srcBuffer, VKP::Texture &dstTexture, uint32_t width, uint32_t height) {
    auto cmdBuf = BeginSingleCommandBuffer();
    VkBufferImageCopy region{};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;

    region.imageOffset = {0, 0, 0};
    region.imageExtent = {
            width,
            height,
            1
    };

    vkCmdCopyBufferToImage(
            cmdBuf,
            srcBuffer.GetVulkanBuffer(),
            dstTexture.GetImage(),
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1,
            &region
    );
    EndSingleCommandBuffer(cmdBuf);
}

std::pair<int32_t, int32_t> VKP::Renderer::GetWindowSize() const {
    return std::make_pair(m_pWindowWidth, m_pWindowHeight);
}

std::pair<int32_t, int32_t> VKP::Renderer::GetScreenSize() const {
    return std::pair<int32_t, int32_t>();
}

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
        void *pUserData) {

    if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
        VKP_LOG_ERROR("Validation layer : {}\n", pCallbackData->pMessage);
    }

    return VK_FALSE;
}

std::unique_ptr<VKP::Renderer>VKP::Renderer::m_Renderer;
