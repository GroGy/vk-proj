//
// Created by Matty on 2021-04-25.
//

#include "../app/engine/PerfData.h"

VKP_Engine::PerfData::PerfData() {

}

std::unique_ptr<VKP_Engine::PerfData> &VKP_Engine::PerfData::GetInstance() {
    if(!s_pInstance) s_pInstance = std::make_unique<VKP_Engine::PerfData>();
    return s_pInstance;
}


std::unique_ptr<VKP_Engine::PerfData> VKP_Engine::PerfData::s_pInstance;