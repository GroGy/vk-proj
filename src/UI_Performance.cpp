//
// Created by Matty on 2021-03-27.
//

#include "../app/ui/windows/UI_Performance.h"
#include "../app/imgui/imgui.h"
#include "../app/render/RenderData.h"

void VKP::UI_Performance::CustomDraw() {
    ImGui::Text("FPS : %.1f (%.3f ms)", ImGui::GetIO().Framerate,1000.0f / ImGui::GetIO().Framerate);

    auto & rData = VKP::RenderData::GetInstance();

    ImGui::Text("Vertex buffers : %zu", rData->m_VertexBuffers.size());
    ImGui::Text("Index buffers : %zu",  rData->m_IndexBuffers.size());
    ImGui::Text("Uniform buffers : %zu",  rData->m_UniformBuffers.size());
    ImGui::Text("Textures : %zu", rData->m_Textures.size());
    ImGui::Text("Staging buffers : %zu", rData->m_StagingBuffers.size());
}

VKP::UI_Performance::UI_Performance() : UI_Window("Performance") {

}
