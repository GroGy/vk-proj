//
// Created by Matty on 2021-03-27.
//

#include "../app/render/IndexBuffer.h"

bool VKP::IndexBuffer::MarkedForDeletion() const {
    if(!m_pValid) {
        VKP_LOG_WARN("Invalid index buffer marked for deletion");
        return false;
    }
    return m_pBuffer.MarkedForDeletion();
}

void VKP::IndexBuffer::MarkForDeletion() {
    if(!m_pValid) {
        VKP_LOG_WARN("Tried to invalid index buffer for deletion");
        return;
    }
    m_pBuffer.MarkForDeletion();
}

VKP::IndexBuffer::~IndexBuffer() {
    assert(!m_pValid || m_pBuffer.MarkedForDeletion());
}

VKP::IndexBuffer::IndexBuffer(VmaAllocator &allocator, uint32_t count) : m_pValid(true) {
    assert(count > 0);

    VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
    bufferInfo.size = count*sizeof(uint32_t);
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bufferInfo.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT;

    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;
    m_pBuffer = VKP::Buffer(allocator, bufferInfo, allocInfo);
}

VKP::IndexBuffer::IndexBuffer(VKP::IndexBuffer &&o) noexcept {
    m_pBuffer = std::move(o.m_pBuffer);
    m_pValid = o.m_pValid;
    m_pIndexCount = o.m_pIndexCount;
    o.m_pValid = false;
}

VKP::IndexBuffer &VKP::IndexBuffer::operator=(VKP::IndexBuffer &&o) noexcept {
    m_pBuffer = std::move(o.m_pBuffer);
    m_pValid = o.m_pValid;
    m_pIndexCount = o.m_pIndexCount;
    o.m_pValid = false;
    return *this;
}

void VKP::IndexBuffer::Cleanup(VmaAllocator const &allocator) {
    m_pBuffer.Cleanup(allocator);
}

void VKP::IndexBuffer::BindIndexBuffer(VkCommandBuffer cmdBuffer) {
    VkDeviceSize offset = 0;
    vkCmdBindIndexBuffer(cmdBuffer, m_pBuffer.GetVulkanBuffer(), offset, VK_INDEX_TYPE_UINT32);
}

uint32_t VKP::IndexBuffer::GetIndexCount() const {
    return m_pIndexCount;
}

void VKP::IndexBuffer::LoadData(const uint32_t *data, size_t count) {
    if(!m_pValid) {
        VKP_LOG_WARN("Tried to load data to invalid index buffer");
        return;
    }
    if(count == 0) return;
    m_pBuffer.LoadData(data,count*sizeof(uint32_t));
    m_pIndexCount = count;
}

VKP::Buffer &VKP::IndexBuffer::GetBuffer() {
    return m_pBuffer;
}
