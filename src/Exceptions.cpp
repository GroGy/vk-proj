//
// Created by Matty on 2021-03-22.
//

#include "../app/Exceptions.h"

#include <utility>

VKP::InitException::InitException(std::string what) : m_pWhat(std::move(what)) {
}

const char *VKP::InitException::what() const noexcept {
    return m_pWhat.c_str();
}

VKP::RuntimeException::RuntimeException(std::string what) : m_pWhat(std::move(what)) {

}

const char *VKP::RuntimeException::what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_NOTHROW {
    return m_pWhat.c_str();
}
