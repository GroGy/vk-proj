//
// Created by Matty on 2021-03-28.
//

#include "../app/engine/ImageLoader.h"

#define STB_IMAGE_IMPLEMENTATION

#include "../app/lib/stb_image.h"
#include "../app/render/Renderer.h"

std::pair<std::shared_ptr<VKP::Texture>, std::optional<std::string>>
VKP_Engine::ImageLoader::LoadTexture(const std::string &path) {
    int texWidth, texHeight, texChannels;
    stbi_uc *pixels = stbi_load(path.c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
    VkDeviceSize imageSize = texWidth * texHeight * 4;

    if (!pixels) {
        return std::pair<std::shared_ptr<VKP::Texture>, std::optional<std::string>>(nullptr, "Failed to load texture");
    }

    auto &renderer = VKP::Renderer::m_Renderer;

    //On main thread part:
    auto &image = renderer->CreateTexture(texWidth, texHeight, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL,
                                          VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    auto & stagingBuffer = renderer->CreateStagingBuffer(imageSize);
    stagingBuffer->LoadData(pixels, imageSize);

    image->LoadTexture(stagingBuffer);

    stagingBuffer->MarkForDeletion();
    //

    return std::pair<std::shared_ptr<VKP::Texture>, std::optional<std::string>>(image,std::optional<std::string>());
}
