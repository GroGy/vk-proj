//
// Created by Matty on 2021-03-21.
//
#include "../app/App.h"
#include "../app/render/RenderData.h"
#include "../app/game/Game.h"
#include "../app/input/InputController.h"
#include "../app/engine/ModelUniformBuffer.h"
#include "../app/engine/EngineGlobals.h"

void VKP::App::Run() {
    VKP_LOG_INFO("Starting VKP")
#ifdef NDEBUG
    VKP_LOG_LEVEL_INFO()
#else
    VKP_LOG_LEVEL_DEBUG()
#endif
    s_pRestartGame = false;
    _Init();
    _MainLoop();
    _Cleanup();
}

void VKP::App::_Init() {
    //OPTICK_APP("VKP");
    auto &renderData = RenderData::GetInstance();
    renderData->m_ShaderModules.reserve(10);
    auto &fragment = renderData->m_ShaderModules.emplace_back("shaders/base.frag.spv", ShaderType::VKP_SHADER_FRAGMENT);
    auto &vertex = renderData->m_ShaderModules.emplace_back("shaders/base.vert.spv", ShaderType::VKP_SHADER_VERTEX);
    renderData->m_Pipelines.reserve(10);

    Renderer::m_Renderer = std::make_unique<Renderer>();
    _InitWindow();
    Renderer::m_Renderer->Init(m_pWindow);

    PipelineBlueprint blueprint{};
    blueprint.AddShaderStage(vertex);
    blueprint.AddShaderStage(fragment);

    VkPipelineDepthStencilStateCreateInfo depthStencil{};
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.minDepthBounds = 0.0f; // Optional
    depthStencil.maxDepthBounds = 1.0f; // Optional
    depthStencil.stencilTestEnable = VK_FALSE;
    depthStencil.front = {}; // Optional
    depthStencil.back = {}; // Optional

    blueprint.SetDepthStencilState(depthStencil);

    blueprint.SetVertexInputDescription(VKP_Engine::ModelVertex::GetBindingDescription(),
                                        VKP_Engine::ModelVertex::GetAttributeDescriptions());
    blueprint.AddPushConstant(VK_SHADER_STAGE_VERTEX_BIT, 0, 2*sizeof(glm::mat4));

    auto uniformBP = VKP::UniformDescriptorBlueprint(VK_SHADER_STAGE_VERTEX_BIT,0,1,0,sizeof(VKP_Engine::ModelUniformBuffer));

    VKP_Engine::EngineGlobals::s_ModelUniformDesc = uniformBP;

    blueprint.AddUniformLayout(uniformBP);
    blueprint.SetCachedDescriptorSets(1024);

    auto &pipeline = renderData->m_Pipelines.emplace_back(std::make_shared<VKP::Pipeline>(std::move(blueprint)));

    pipeline->Load();

    m_pUIInternals = std::make_shared<VKP::UIInternals>();
    m_pUIInternals->Init(m_pWindow);
    m_pUI = std::make_shared<VKP::UI>();

    auto &input = InputController::GetInstance();
    input->Init(m_pWindow);

    auto &game = VKP_Game::Game::GetInstance();
    VKP_Game::GameInitData gameInitData{};

    gameInitData.m_MainWindow = m_pWindow;

    game->Init(gameInitData);
}

void VKP::App::_MainLoop() {
    static double tickTime = 1.0 / 60.0;

    double lastTime = glfwGetTime(), timer = lastTime;
    double deltaTime = 0, nowTime = 0;

    while (!glfwWindowShouldClose(m_pWindow->GetGlfwWindow().get())) {
        glfwPollEvents();

        _RestartGame();

        nowTime = glfwGetTime();
        deltaTime += (nowTime - lastTime) / tickTime;
        lastTime = nowTime;

        while (deltaTime >= tickTime) {
            VKP_Game::Game::GetInstance()->Update((float) tickTime);
            deltaTime -= tickTime;
            Renderer::m_Renderer->Update();
        }

        _Render();
    }
}

void VKP::App::_Cleanup() {
    Renderer::m_Renderer->WaitIdle();
    m_pUIInternals->Cleanup();
    Renderer::m_Renderer->Cleanup();
    Renderer::m_Renderer.reset();
}

void VKP::App::_InitWindow() {
    auto windowBuilder = std::make_shared<WindowBuilder>(1920, 1080)->setWindowName(
            "Vulkan Learning")->setBackgroundColor({0.2f, 0.1f, 0.2f, 1.0f});
    m_pWindow = std::shared_ptr<VKP::Window>(windowBuilder->finish());
}

void VKP::App::_Render() {
    Renderer::m_Renderer->BeginFrame();
    auto frameData = Renderer::m_Renderer->GetFrameDrawData();
    if (!m_pUIInternals->FontUploaded()) {
        m_pUIInternals->UploadFonts(frameData.frameCommandBuffer);
        Renderer::m_Renderer->BeginMainRenderPass();
        Renderer::m_Renderer->EndMainRenderPass();
        Renderer::m_Renderer->FinishFrame();
        Renderer::m_Renderer->PresentRender();
        return;
    }
    Renderer::m_Renderer->BeginMainRenderPass();

    VKP_Game::Game::GetInstance()->Render(frameData);

    m_pUI->Draw(m_pUIInternals, frameData);

    Renderer::m_Renderer->EndMainRenderPass();
    Renderer::m_Renderer->FinishFrame();

    Renderer::m_Renderer->PresentRender();
}

void VKP::App::RestartGame() {
    s_pRestartGame = true;
}

void VKP::App::_RestartGame() {
    if (s_pRestartGame) {
        VKP_LOG_INFO("Restarting game...")

        VKP_LOG_INFO("Restarted game")
        s_pRestartGame = false;
    }
}

bool VKP::App::s_pRestartGame;
