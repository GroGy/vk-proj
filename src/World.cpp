//
// Created by Matty on 2021-03-28.
//

#include "../app/game/World.h"
#include "../app/game/data/Game_WorldData.h"

void VKP_Game::World::Update(float delta) {
    m_pPhysicsWorld->update(delta);
}

reactphysics3d::PhysicsWorld *VKP_Game::World::GetPhysicsWorld() {
    return m_pPhysicsWorld;
}

void VKP_Game::World::Draw(VKP::FrameDrawData &frame) {
    m_pTerrain.Draw(frame);
}

VKP_Game::World::World(float gravity, bool sleepingEnabled) : m_pValid(true) {
    reactphysics3d::PhysicsWorld::WorldSettings settings;
    settings.defaultVelocitySolverNbIterations = 20;
    settings.isSleepingEnabled = sleepingEnabled;
    settings.gravity = reactphysics3d::Vector3(0, gravity, 0);

    m_pPhysicsWorld = Game_WorldData::GetInstance()->m_PhysicsCommon.createPhysicsWorld(settings);

    m_pTerrain = VKP_Game::TerrainChunk(m_pPhysicsWorld,{0,0,0},{0,0,0},{100,1,100});
}

bool VKP_Game::World::IsValid() const {
    return m_pValid;
}
