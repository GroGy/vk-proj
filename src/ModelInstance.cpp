//
// Created by Matty on 2021-04-23.
//

#include "../app/render/ModelInstance.h"

#include <utility>
#include "../app/engine/EngineGlobals.h"

void VKP::ModelInstance::Draw(const VKP::FrameDrawData &frame) {
    if(!m_pValid) return;
    if (m_pModelMatrixInvalid) {
        m_pModelMatrix = glm::mat4(1.0f);
        m_pModelMatrix = glm::translate(m_pModelMatrix, m_pPosition);
        m_pModelMatrix = glm::rotate(m_pModelMatrix, glm::radians(m_pRotation.x), {1.0f, 0.0f, 0.0f});
        m_pModelMatrix = glm::rotate(m_pModelMatrix, glm::radians(m_pRotation.y), {0.0f, 1.0f, 0.0f});
        m_pModelMatrix = glm::rotate(m_pModelMatrix, glm::radians(m_pRotation.z), {0.0f, 0.0f, 1.0f});
        m_pModelMatrix = glm::scale(m_pModelMatrix, {m_pScale.x,m_pScale.z,m_pScale.y});
        m_pModelMatrixInvalid = false;
    }

    m_pModel->Draw(frame, m_pModelMatrix, m_pModelBindings);
}

const glm::vec3 &VKP::ModelInstance::GetPosition() const {
    return m_pPosition;
}

void VKP::ModelInstance::SetPosition(const glm::vec3 &mPPosition) {
    m_pPosition = mPPosition;
    m_pModelMatrixInvalid = true;
}

const glm::vec3 &VKP::ModelInstance::GetRotation() const {
    return m_pRotation;
}

void VKP::ModelInstance::SetRotation(const glm::vec3 &mPRotation) {
    m_pRotation = mPRotation;
    m_pModelMatrixInvalid = true;
}

const glm::vec3 &VKP::ModelInstance::GetScale() const {
    return m_pScale;
}

void VKP::ModelInstance::SetScale(const glm::vec3 &mPScale) {
    m_pScale = mPScale;
    m_pModelMatrixInvalid = true;
}

VKP::ModelInstance::ModelInstance(std::shared_ptr<VKP::Model> model, const glm::vec3 &pos, const glm::vec3 &rot,
                                  const glm::vec3 &scale) : m_pValid(true), m_pModel(std::move(model)), m_pPosition(pos),
                                                            m_pRotation(rot), m_pScale(scale) {
    m_pModelBindings.reserve(m_pModel->m_pMeshes.size());
    for(uint32_t i = 0; i < m_pModel->m_pMeshes.size(); i++) {
        m_pModelBindings.emplace_back(m_pModel->m_pMeshes.at(i).m_pPipeline->CreateUniformBuffer(VKP_Engine::EngineGlobals::s_ModelUniformDesc));
    }
}

bool VKP::ModelInstance::IsValid() const {
    return m_pValid;
}
