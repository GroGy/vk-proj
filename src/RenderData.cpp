//
// Created by Matty on 2021-03-24.
//

#include "../app/render/RenderData.h"

std::unique_ptr<VKP::RenderData> VKP::RenderData::s_pInstance;

std::unique_ptr<VKP::RenderData> &VKP::RenderData::GetInstance() {
    if(!s_pInstance) s_pInstance = std::make_unique<VKP::RenderData>();
    return s_pInstance;
}

VKP::RenderData::RenderData() {
    m_VertexBuffers.reserve(128);
    m_IndexBuffers.reserve(128);
    m_UniformBuffers.reserve(128);
}
