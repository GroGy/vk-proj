//
// Created by Matty on 2021-04-24.
//

#include "../app/game/data/Game_WorldData.h"

std::unique_ptr<VKP_Game::Game_WorldData> &VKP_Game::Game_WorldData::GetInstance() {
    if(!s_pInstance) s_pInstance = std::make_unique<VKP_Game::Game_WorldData>();
    return s_pInstance;
}

VKP_Game::Game_WorldData::Game_WorldData() {
}

std::unique_ptr<VKP_Game::Game_WorldData> VKP_Game::Game_WorldData::s_pInstance;