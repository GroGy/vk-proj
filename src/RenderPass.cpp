//
// Created by Matty on 2021-03-27.
//

#include "../app/render/platform/RenderPass.h"
#include "../app/Log.h"
#include "../app/Exceptions.h"
#include "../app/render/Renderer.h"

VkRenderPass VKP::RenderPass::GetRenderPass() const {
    if (!m_pValid) {
        VKP_LOG_ERROR("Requested render pass from invalid object");
        return nullptr;
    }
    return m_pRenderPass;
}

VKP::RenderPass::RenderPass(VKP::RenderPassBlueprint blueprint) : m_pBlueprint(std::move(blueprint)), m_pValid(true) {
    Load();
}

void VKP::RenderPass::Cleanup() {
    if (m_pValid) {
        vkDestroyRenderPass(VKP::Renderer::m_Renderer->GetDevice(), m_pRenderPass, nullptr);
    }
}

void VKP::RenderPass::Load() {
    m_pRenderPass = m_pBlueprint._BuildRenderPass().m_RenderPass;
}

void VKP::RenderPass::Unload() {
    Cleanup();
}

VkSampleCountFlagBits VKP::RenderPassBlueprint::_GetSamples(uint32_t sampleCount) {
    if (sampleCount == 1) {
        return VK_SAMPLE_COUNT_1_BIT;
    }
    if (sampleCount == 2) {
        return VK_SAMPLE_COUNT_2_BIT;
    }
    if (sampleCount == 4) {
        return VK_SAMPLE_COUNT_4_BIT;
    }
    if (sampleCount == 8) {
        return VK_SAMPLE_COUNT_8_BIT;
    }
    if (sampleCount == 16) {
        return VK_SAMPLE_COUNT_16_BIT;
    }
    if (sampleCount == 32) {
        return VK_SAMPLE_COUNT_32_BIT;
    }
    if (sampleCount == 64) {
        return VK_SAMPLE_COUNT_64_BIT;
    }
    throw VKP::RuntimeException("Invalid sample count passed to renderpass");
}

VKP::AttachmentBlueprint::AttachmentBlueprint(VkFormat format) : m_Format(format), m_Valid(true) {

}


VKP::RenderPassBlueprintResult VKP::RenderPassBlueprint::_BuildRenderPass() {
    VKP::RenderPassBlueprintResult res{};

    uint32_t depIndexOffset = 0;
    uint32_t depCount = 0;

    for (auto &&pass : m_pSubPasses) {
        depCount += pass.m_pAttachments.size();
        if (pass.m_pDepthStencilAttachment.m_Valid) depCount++;
    }

    std::vector<VkAttachmentDescription> renderPassAttachments{depCount};
    std::vector<VkSubpassDescription> subpasses{m_pSubPasses.size()};
    std::vector<VkSubpassDependency> subpassDeps{m_pSubPasses.size()};

    std::vector<std::vector<VkAttachmentDescription>> sp_attachments{m_pSubPasses.size()};
    std::vector<std::vector<VkAttachmentReference>> sp_attachmentReferences{m_pSubPasses.size()};

    std::vector<VkAttachmentDescription> sp_depthStencilAttachment{m_pSubPasses.size()};
    std::vector<VkAttachmentReference> sp_depthStencilReference{m_pSubPasses.size()};

    for (uint32_t pass = 0; pass < m_pSubPasses.size(); pass++) {
        auto & attachments = sp_attachments.at(pass);
        attachments.resize(m_pSubPasses.at(pass).m_pAttachments.size());
        auto & attachmentReferences = sp_attachmentReferences.at(pass);
        attachmentReferences.resize(m_pSubPasses.at(pass).m_pAttachments.size());

        auto & depthStencilAttachment = sp_depthStencilAttachment.at(pass);
        auto & depthStencilReference = sp_depthStencilReference.at(pass);

        for (uint32_t i = 0; i < m_pSubPasses.at(pass).m_pAttachments.size(); i++) {
            auto &attachmentBP = m_pSubPasses.at(pass).m_pAttachments.at(i);
            auto &attachment = attachments.at(i);
            auto &reference = attachmentReferences.at(i);

            uint32_t sampleCount = attachmentBP.m_SampleCount;
            assert(sampleCount == 1 || sampleCount == 2 || sampleCount == 4 || sampleCount == 8 || sampleCount == 16 ||
                   sampleCount == 32 || sampleCount == 64);
            attachment.format = attachmentBP.m_Format;
            attachment.samples = _GetSamples(sampleCount);
            attachment.loadOp = attachmentBP.m_LoadOP;
            attachment.storeOp = attachmentBP.m_StoreOP;
            attachment.stencilLoadOp = attachmentBP.m_StencilLoadOP;
            attachment.stencilStoreOp = attachmentBP.m_StencilStoreOP;
            attachment.initialLayout = attachmentBP.m_InitialLayout;
            attachment.finalLayout = attachmentBP.m_FinalLayout;

            renderPassAttachments.at(depIndexOffset) = attachment;
            reference.attachment = depIndexOffset++;
            reference.layout = attachmentBP.m_Layout;
        }

        if (m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_Valid) {
            uint32_t sampleCount = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_SampleCount;
            assert(sampleCount == 1 || sampleCount == 2 || sampleCount == 4 || sampleCount == 8 || sampleCount == 16 ||
                   sampleCount == 32 || sampleCount == 64);
            depthStencilAttachment.format = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_Format;
            depthStencilAttachment.samples = _GetSamples(sampleCount);
            depthStencilAttachment.loadOp = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_LoadOP;
            depthStencilAttachment.storeOp = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_StoreOP;
            depthStencilAttachment.stencilLoadOp = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_StencilLoadOP;
            depthStencilAttachment.stencilStoreOp = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_StencilStoreOP;
            depthStencilAttachment.initialLayout = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_InitialLayout;
            depthStencilAttachment.finalLayout = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_FinalLayout;

            depthStencilReference.attachment = 1;
            depthStencilReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

            renderPassAttachments.at(depIndexOffset++) = depthStencilAttachment;
        }


        subpasses.at(pass).pipelineBindPoint = m_pSubPasses.at(pass).m_BindPoint;
        subpasses.at(pass).colorAttachmentCount = static_cast<uint32_t>(attachmentReferences.size());
        subpasses.at(pass).pColorAttachments = attachmentReferences.data();
        if (m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_Valid) {
            subpasses.at(pass).pDepthStencilAttachment = &depthStencilReference;
        }


        subpassDeps.at(pass).srcSubpass = VK_SUBPASS_EXTERNAL;
        subpassDeps.at(pass).dstSubpass = 0;
        subpassDeps.at(pass).srcAccessMask = 0;
        subpassDeps.at(pass).srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpassDeps.at(pass).dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpassDeps.at(pass).dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

        if (m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_Valid) {
            subpassDeps.at(pass).srcStageMask =
                    subpassDeps.at(pass).srcStageMask | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
            subpassDeps.at(pass).dstStageMask =
                    subpassDeps.at(pass).srcStageMask | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
            subpassDeps.at(pass).dstAccessMask =
                    subpassDeps.at(pass).srcStageMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        }
    }

    VkRenderPassCreateInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = static_cast<uint32_t>(renderPassAttachments.size());
    renderPassInfo.pAttachments = renderPassAttachments.data();
    renderPassInfo.subpassCount = static_cast<uint32_t>(subpasses.size());
    renderPassInfo.pSubpasses = subpasses.data();
    renderPassInfo.dependencyCount = static_cast<uint32_t>(subpassDeps.size());
    renderPassInfo.pDependencies = subpassDeps.data();

    if (vkCreateRenderPass(VKP::Renderer::m_Renderer->GetDevice(), &renderPassInfo, nullptr, &res.m_RenderPass) !=
        VK_SUCCESS) {
        throw VKP::InitException("Failed to create render pass!");
    }
    return res;
}

void VKP::RenderPassBlueprint::AddSubPass(SubPassBlueprint pass) {
    m_pSubPasses.emplace_back(std::move(pass));
}

void VKP::SubPassBlueprint::AddAttachment(AttachmentBlueprint attachment) {
    m_pAttachments.emplace_back(std::move(attachment));
}

void VKP::SubPassBlueprint::SetDepthAttachment(VKP::AttachmentBlueprint depthAttachment) {
    m_pDepthStencilAttachment = (depthAttachment);
}

VKP::SubPassBlueprint::SubPassBlueprint(VKP::AttachmentBlueprint attachment) : m_pValid(true) {
    m_pAttachments.reserve(4);
    m_pAttachments.emplace_back(attachment);
}

VkSubpassDescription VKP::SubPassBlueprint::_BuildSubPass() {
    VkSubpassDescription res{};


    return res;
}
