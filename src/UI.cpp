//
// Created by Matty on 2021-03-26.
//

#include "../app/ui/UI.h"
#include "../app/ui/windows/UI_Performance.h"
#include "../app/ui/windows/UI_Tools.h"
#include "../app/App.h"

void VKP::UIInternals::Init(const std::shared_ptr<VKP::Window> &window) {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGuiIO& io = ImGui::GetIO();
    (void) io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
    //io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows (not working for GLFW)
    // Sets styles
    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
        style.WindowRounding = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }

    ImGui_ImplGlfw_InitForVulkan(window->GetGlfwWindow().get(), true);
    auto & renderer = Renderer::m_Renderer;
    auto imguiCreateInfo = renderer->CreateImGuiInitInfo();
    ImGui_ImplVulkan_Init(&imguiCreateInfo, renderer->GetMainRenderPass());
}

void VKP::UIInternals::Cleanup() {
    ImGui_ImplVulkan_DestroyFontUploadObjects();
    ImGui_ImplVulkan_Shutdown();
    ImGui::DestroyContext();
}

bool VKP::UIInternals::FontUploaded() const {
    return m_pFontsUploaded;
}

void VKP::UIInternals::UploadFonts( VkCommandBuffer cmd) {
    ImGui_ImplVulkan_CreateFontsTexture(cmd);
    m_pFontsUploaded = true;
}

void VKP::UI::Draw(const std::shared_ptr<UIInternals> &data, VKP::FrameDrawData frameCmdBuffer) {
    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    for (auto &window : m_Windows) {
        if(!window->GetCollapsed()) window->Draw();
    }

    //ImGui::ShowMetricsWindow();

    ImGui::Render();
    ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(),frameCmdBuffer.frameCommandBuffer);
}

bool VKP::UI::IsWorldViewVisible() {
    return true;
}

VKP::UI::UI() {
    m_Windows.reserve(16);
    m_Windows.emplace_back(std::make_shared<UI_Performance>());
    m_Windows.emplace_back(std::make_shared<UI_Tools>());
}
