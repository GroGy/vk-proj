//
// Created by Matty on 2021-03-28.
//

#include "../app/input/InputController.h"

std::unique_ptr<VKP::InputController> &VKP::InputController::GetInstance() {
    if(!s_pInstance) s_pInstance = std::make_unique<VKP::InputController>();
    return s_pInstance;
}

glm::vec2 VKP::InputController::GetTrigger2D(const std::string &inputName) {
    return glm::vec2();
}

float VKP::InputController::GetTrigger1D(const std::string &inputName) {
    return 0;
}

bool VKP::InputController::OnKeyReleased(const std::string &inputName) {
    return false;
}

bool VKP::InputController::OnKeyPressed(const std::string &inputName) {
    assert(m_pInputMap.contains(inputName));
    if(m_pInputMap.contains(inputName)) {
        auto & val = m_pInputMap.at(inputName);
        switch(val.type) {
            case VKP_INPUT_KEYBOARD: {
                bool pressed = glfwGetKey(m_pWindow->GetGlfwWindow().get(),val.key) == GLFW_PRESS;
                bool wasPressed = val.wasPressed;
                val.wasPressed = pressed;
                return pressed && !wasPressed; }
            case VKP_INPUT_MOUSE: {
                bool pressed = glfwGetMouseButton(m_pWindow->GetGlfwWindow().get(),val.key) == GLFW_PRESS;
                bool wasPressed = val.wasPressed;
                val.wasPressed = pressed;
                return pressed && !wasPressed; }
            case VKP_INPUT_CONTROLLER_BUTTON:
                return false;
            case VKP_INPUT_CONTROLLER_AXIS:
                return false;
        }
    }
    return false;
}

bool VKP::InputController::IsKeyReleased(const std::string &inputName) {
    assert(m_pInputMap.contains(inputName));
    if(m_pInputMap.contains(inputName)) {
        auto & val = m_pInputMap.at(inputName);
        switch(val.type) {
            case VKP_INPUT_KEYBOARD: {
                bool pressed = glfwGetKey(m_pWindow->GetGlfwWindow().get(),val.key) == GLFW_RELEASE;
                val.wasPressed = pressed;
                return pressed; }
            case VKP_INPUT_MOUSE: {
                bool pressed = glfwGetMouseButton(m_pWindow->GetGlfwWindow().get(),val.key) == GLFW_RELEASE;
                val.wasPressed = pressed;
                return pressed; }
            case VKP_INPUT_CONTROLLER_BUTTON:
                return false;
            case VKP_INPUT_CONTROLLER_AXIS:
                return false;
        }
    }
    return false;
}

bool VKP::InputController::IsKeyPressed(const std::string &inputName) {
    assert(m_pInputMap.contains(inputName));
    if(m_pInputMap.contains(inputName)) {
        auto & val = m_pInputMap.at(inputName);
        switch(val.type) {
            case VKP_INPUT_KEYBOARD:
                return glfwGetKey(m_pWindow->GetGlfwWindow().get(),val.key) == GLFW_PRESS;
            case VKP_INPUT_MOUSE:
                return glfwGetMouseButton(m_pWindow->GetGlfwWindow().get(),val.key) == GLFW_PRESS;
            case VKP_INPUT_CONTROLLER_BUTTON:
                return false;
            case VKP_INPUT_CONTROLLER_AXIS:
                return false;
        }
    }
    return false;
}

glm::vec2 VKP::InputController::_GetVec2Input(uint32_t joystick) {
    auto res = glm::vec2();
    //TODo
    return res;
}

void VKP::InputController::Init(const std::shared_ptr<VKP::Window> &window) {
    m_pWindow = window;
}

void VKP::InputController::RegisterKeyInputs(std::map<std::string, InputKey> data) {
    m_pInputMap = std::move(data);
}

void VKP::InputController::RegisterKeyInput(const std::string & name, InputKey data) {
    m_pInputMap[name] = data;
}

std::pair<double, double> VKP::InputController::GetMousePos() {
    double x, y;
    glfwGetCursorPos(m_pWindow->GetGlfwWindow().get(), &x,&y);
    return std::make_pair(x,y);
}

glm::vec2 VKP::InputController::GetMousePosNormalized() {
    int32_t x, y;
    glfwGetWindowSize(m_pWindow->GetGlfwWindow().get(), &x,&y);
    double mx, my;
    glfwGetCursorPos(m_pWindow->GetGlfwWindow().get(), &mx,&my);
    return glm::vec2(mx/x, my/y);
}

double VKP::InputController::GetMouseScroll() {
    return ImGui::GetScrollY();
}

glm::vec3
VKP::InputController::ScreenToWorldSpace(const glm::mat4 &pers, const glm::mat4 &viewMat) {
    glm::mat4 invMat= glm::inverse(pers*viewMat);
    auto screenSize = VKP::Renderer::m_Renderer->GetWindowSize();
    auto halfScreenWidth = screenSize.first / 2;
    auto halfScreenHeight = screenSize.second / 2;
    auto pos = GetMousePos();
    glm::vec4 nearS = glm::vec4((pos.first - halfScreenWidth) / halfScreenWidth, -1*(pos.first - halfScreenHeight) / halfScreenHeight, -1, 1.0);
    glm::vec4 farS = glm::vec4((pos.first - halfScreenWidth) / halfScreenWidth, -1*(pos.first - halfScreenHeight) / halfScreenHeight, 1, 1.0);
    glm::vec4 nearResult = invMat*nearS;
    glm::vec4 farResult = invMat*farS;
    nearResult /= nearResult.w;
    farResult /= farResult.w;
    glm::vec3 dir = glm::vec3(farResult - nearResult );
    return normalize(dir);

}

std::unique_ptr<VKP::InputController> VKP::InputController::s_pInstance;

VKP::InputKey::InputKey(VKP::InputType type, uint32_t key) : type(type), key(key), valid(true) {}
