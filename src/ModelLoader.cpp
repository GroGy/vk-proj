//
// Created by Matty on 2021-03-28.
//

#include <fstream>
#include "../app/engine/ModelLoader.h"
#include "../app/engine/ModelLoadData.h"
#include "../app/engine/Serialization.h"
#include "../app/Log.h"

std::pair<std::shared_ptr<VKP::Model>, std::optional<std::string>> VKP_Engine::ModelLoader::LoadModel(const std::string &path) {
    std::ifstream file(path,std::ios::binary);
    if(file.is_open()) {
        //TODO: LZ4 compression
        uint32_t version;
        VKP_Engine::ModelBaseData header{};
        VKP_Engine::ModelImportData data{};

        file.read((char*)&version, sizeof(uint32_t));
        file.read((char*)&header, sizeof(struct VKP_Engine::ModelBaseData));

        data.meshes.resize(header.meshCount);

        for(uint32_t i = 0; i < header.meshCount; i++) {
            uint32_t vertexCount;
            uint32_t indexCount;

            file.read((char*)&vertexCount, sizeof(uint32_t));
            file.read((char*)&indexCount, sizeof(uint32_t));

            data.meshes.at(i).vertices.resize(vertexCount, {{0.0f,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,0.0f,0.0f}}); //Fill with empty vertices

            file.read((char*)data.meshes.at(i).vertices.data(), vertexCount * sizeof(VKP_Engine::ModelVertex));
            if(!file.good())
                assert(false);

            data.meshes.at(i).indices.resize(indexCount);
            file.read((char*)data.meshes.at(i).indices.data(), sizeof(uint32_t)*indexCount);
            if(!file.good())
                assert(false);
        }

        file.close();

        std::vector<VKP::Mesh> meshes(header.meshCount);


        for(uint32_t i=0; i < header.meshCount; i++) {
            auto & mesh = data.meshes.at(i);
            meshes.at(i).Load(mesh.vertices.data(),mesh.vertices.size()*sizeof(VKP_Engine::ModelVertex),mesh.indices.data(),mesh.indices.size(),mesh.vertices.size());
        }

        auto model = std::make_shared<VKP::Model>(std::move(meshes));
        return std::pair<std::shared_ptr<VKP::Model>, std::optional<std::string>>(std::move(model),std::optional<std::string>());
    } else {
        return std::pair<std::shared_ptr<VKP::Model>, std::optional<std::string>>(nullptr, fmt::format("Failed to open file {}",path));
    }
}
