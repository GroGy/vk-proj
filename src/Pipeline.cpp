//
// Created by Matty on 2021-03-24.
//

#include <set>
#include "../app/render/platform/Pipeline.h"
#include "../app/render/RenderData.h"
#include "../app/render/Renderer.h"

void VKP::PipelineBlueprint::PrepareShaders() {
    auto &data = VKP::RenderData::GetInstance();

    std::set<uint64_t> remainingShaders(m_pShaders.begin(), m_pShaders.end());

    for (auto &&shader : data->m_ShaderModules) {
        for (auto &&localShader : m_pShaders) {
            if (shader.GetID() == localShader) {
                if (!shader.IsLoaded()) {
                    shader.Load();
                }
                remainingShaders.erase(localShader);
            }
        }
    }

    if (!remainingShaders.empty()) {
        throw VKP::InitException("Not all required shaders found");
    }
}

VKP::PipelineBlueprintResult VKP::PipelineBlueprint::_BuildPipeline() {
    std::vector<VkPipelineShaderStageCreateInfo> stages(m_pShaders.size());
    auto &data = VKP::RenderData::GetInstance();

    VKP::PipelineBlueprintResult result{};

    bool createDescriptors = false;

    for (uint32_t i = 0; i < m_pShaders.size(); i++) {
        for (auto &&shader : data->m_ShaderModules) {
            if (shader.GetID() == m_pShaders.at(i)) {
                stages.at(i).sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
                switch (shader.GetType()) {
                    case VKP_SHADER_VERTEX:
                        stages.at(i).stage = VK_SHADER_STAGE_VERTEX_BIT;
                        break;
                    case VKP_SHADER_FRAGMENT:
                        stages.at(i).stage = VK_SHADER_STAGE_FRAGMENT_BIT;
                        break;
                    case VKP_SHADER_GEOMETRY:
                        stages.at(i).stage = VK_SHADER_STAGE_GEOMETRY_BIT;
                        break;
                    case VKP_SHADER_TESSELATION:
                        stages.at(i).stage = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
                        break;
                }
                stages.at(i).module = shader.GetModule();
                stages.at(i).pName = "main";
            }
        }
    }

    VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(m_pVertexInputDescription.size());
    vertexInputInfo.pVertexBindingDescriptions = m_pVertexInputDescription.empty() ? nullptr
                                                                                   : m_pVertexInputDescription.data();
    vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(m_pVertexAttributeDescription.size());
    vertexInputInfo.pVertexAttributeDescriptions = m_pVertexAttributeDescription.empty() ? nullptr
                                                                                         : m_pVertexAttributeDescription.data();

    VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    auto &renderer = VKP::Renderer::m_Renderer;
    auto swapChainExtent = renderer->GetSwapChainExtent();

    VkViewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float) swapChainExtent.width;
    viewport.height = (float) swapChainExtent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor{};
    scissor.offset = {0, 0};
    scissor.extent = swapChainExtent;

    VkPipelineViewportStateCreateInfo viewportState{};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizer{};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = VK_CULL_MODE_NONE;
    rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;


    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f; // Optional
    rasterizer.depthBiasClamp = 0.0f; // Optional
    rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

    VkPipelineMultisampleStateCreateInfo multisampling{};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f; // Optional
    multisampling.pSampleMask = nullptr; // Optional
    multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
    multisampling.alphaToOneEnable = VK_FALSE; // Optional

    VkPipelineColorBlendAttachmentState colorBlendAttachment{};
    colorBlendAttachment.colorWriteMask =
            VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_FALSE;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

    VkPipelineColorBlendStateCreateInfo colorBlending{};
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f; // Optional
    colorBlending.blendConstants[1] = 0.0f; // Optional
    colorBlending.blendConstants[2] = 0.0f; // Optional
    colorBlending.blendConstants[3] = 0.0f; // Optional

    VkDynamicState dynamicStates[] = {
            VK_DYNAMIC_STATE_VIEWPORT,
            VK_DYNAMIC_STATE_LINE_WIDTH
    };

    VkPipelineDynamicStateCreateInfo dynamicState{};
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.dynamicStateCount = 2;
    dynamicState.pDynamicStates = dynamicStates;

    if (!m_pUniformDescriptorBlueprints.empty()) {
        std::vector<VkDescriptorSetLayoutBinding> uboLayoutBindings{m_pUniformDescriptorBlueprints.size()};

        for (uint32_t i = 0; i < m_pUniformDescriptorBlueprints.size(); i++) {
            uboLayoutBindings.at(i).binding = m_pUniformDescriptorBlueprints.at(i).binding;
            uboLayoutBindings.at(i).descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            uboLayoutBindings.at(i).descriptorCount = m_pUniformDescriptorBlueprints.at(i).descriptorCount;
            uboLayoutBindings.at(i).stageFlags = m_pUniformDescriptorBlueprints.at(i).shaderStage;
            uboLayoutBindings.at(i).pImmutableSamplers = nullptr; // TODO
        }

        VkDescriptorSetLayoutCreateInfo layoutInfo{};
        layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layoutInfo.bindingCount = static_cast<uint32_t>(uboLayoutBindings.size());
        layoutInfo.pBindings = uboLayoutBindings.data();

        if (vkCreateDescriptorSetLayout(renderer->GetDevice(), &layoutInfo, nullptr, &result.uDescriptor) !=
            VK_SUCCESS) {
            throw VKP::InitException("failed to create descriptor set layout!");
        }
        createDescriptors = true;
    }
    std::vector<VkPushConstantRange> pushConstantInfos(m_pPushConstants.size());

    for (uint32_t i = 0; i < m_pPushConstants.size(); i++) {
        pushConstantInfos.at(i).stageFlags = m_pPushConstants.at(i).shaderStage;
        pushConstantInfos.at(i).offset = m_pPushConstants.at(i).offset;
        pushConstantInfos.at(i).size = m_pPushConstants.at(i).size;
    }

    VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = m_pUniformDescriptorBlueprints.empty() ? 0 : 1;
    pipelineLayoutInfo.pSetLayouts = &result.uDescriptor;
    pipelineLayoutInfo.pushConstantRangeCount = pushConstantInfos.size();
    pipelineLayoutInfo.pPushConstantRanges = m_pPushConstants.empty() ? nullptr : pushConstantInfos.data();

    if (vkCreatePipelineLayout(renderer->GetDevice(), &pipelineLayoutInfo, nullptr, &result.layout) != VK_SUCCESS) {
        throw VKP::InitException("failed to create pipeline layout!");
    }

    VkGraphicsPipelineCreateInfo pipelineInfo{};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = static_cast<uint32_t>(stages.size());
    pipelineInfo.pStages = stages.data();
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = nullptr; // Optional
    pipelineInfo.layout = result.layout;
    pipelineInfo.pDepthStencilState = m_pHasDepthStencilState ? &m_pDepthStencilState : nullptr;
    pipelineInfo.renderPass = renderer->GetMainRenderPass();
    pipelineInfo.subpass = 0;

    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
    pipelineInfo.basePipelineIndex = -1; // Optional

    if (vkCreateGraphicsPipelines(renderer->GetDevice(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &result.pipeline) !=
        VK_SUCCESS) {
        throw VKP::InitException("failed to create pipeline!");
    }
    VKP_LOG_DEBUG("Loaded pipeline")

    if (createDescriptors) {
        std::vector<VkDescriptorPoolSize> poolSizes{m_pUniformDescriptorBlueprints.size()};
        for (uint32_t i = 0; i < m_pUniformDescriptorBlueprints.size(); i++) {
            poolSizes.at(i).type = m_pUniformDescriptorBlueprints.at(i).type == VKP_UNIFORM_DESCRIPTOR_BUFFER
                                   ? VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER : VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            poolSizes.at(i).descriptorCount = m_pCacheSize * renderer->GetSwapImageCount();
        }

        VkDescriptorPoolCreateInfo poolInfo{};
        poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
        poolInfo.pPoolSizes = poolSizes.data();
        poolInfo.maxSets = static_cast<uint32_t>(poolSizes.size() * m_pCacheSize * renderer->GetSwapImageCount());

        if (vkCreateDescriptorPool(renderer->GetDevice(), &poolInfo, nullptr, &result.descriptorPool) != VK_SUCCESS) {
            throw VKP::InitException("failed to create descriptor pool!");
        }

        VKP_LOG_DEBUG("Created desc pool")

        result.descriptorSets.resize(renderer->GetSwapImageCount() * m_pCacheSize * m_pUniformDescriptorBlueprints.size());
        std::vector<VkDescriptorSetLayout> layouts(
                renderer->GetSwapImageCount() * m_pCacheSize * m_pUniformDescriptorBlueprints.size(),
                result.uDescriptor);

        VkDescriptorSetAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocInfo.descriptorPool = result.descriptorPool;
        allocInfo.descriptorSetCount =
                renderer->GetSwapImageCount() * m_pCacheSize * m_pUniformDescriptorBlueprints.size();
        allocInfo.pSetLayouts = layouts.data();

        if (vkAllocateDescriptorSets(renderer->GetDevice(), &allocInfo, result.descriptorSets.data()) !=
            VK_SUCCESS) {
            throw VKP::InitException("failed to allocate descriptor sets!");
        }

        result.hasUniforms = true;
    }

    return result;
}

void VKP::PipelineBlueprint::AddShaderStage(const VKP::Shader &shader) {
    m_pShaders.emplace_back(shader.GetID());
    m_pAssignedStages.emplace_back(shader.GetType());
}

VKP::PipelineBlueprint::PipelineBlueprint() {

}

void VKP::PipelineBlueprint::Prepare() {
    m_pAssignedStages.reserve(4);
    m_pShaders.reserve(4);
}

VKP::PipelineBlueprint::PipelineBlueprint(VKP::PipelineBlueprint &&o) {
    m_pAssignedStages = std::move(o.m_pAssignedStages);
    m_pShaders = std::move(o.m_pShaders);
    m_pVertexInputDescription = std::move(o.m_pVertexInputDescription);
    m_pVertexAttributeDescription = std::move(o.m_pVertexAttributeDescription);
    m_pPushConstants = std::move(o.m_pPushConstants);
    m_pDepthStencilState = o.m_pDepthStencilState;
    m_pHasDepthStencilState = o.m_pHasDepthStencilState;
    m_pUniformDescriptorBlueprints = std::move(o.m_pUniformDescriptorBlueprints);
    m_pCacheSize = o.m_pCacheSize;
}

VKP::PipelineBlueprint &VKP::PipelineBlueprint::operator=(VKP::PipelineBlueprint &&o) noexcept {
    m_pAssignedStages = std::move(o.m_pAssignedStages);
    m_pShaders = std::move(o.m_pShaders);
    m_pVertexInputDescription = std::move(o.m_pVertexInputDescription);
    m_pVertexAttributeDescription = std::move(o.m_pVertexAttributeDescription);
    m_pHasDepthStencilState = o.m_pHasDepthStencilState;
    m_pDepthStencilState = o.m_pDepthStencilState;
    m_pPushConstants = std::move(o.m_pPushConstants);
    m_pUniformDescriptorBlueprints = std::move(o.m_pUniformDescriptorBlueprints);
    m_pCacheSize = o.m_pCacheSize;
    return *this;
}

void VKP::PipelineBlueprint::SetVertexInputDescription(
        const std::vector<VkVertexInputBindingDescription> &inputBindingDescription,
        const std::vector<VkVertexInputAttributeDescription> &attributeDescription) {
    m_pVertexInputDescription = inputBindingDescription;
    m_pVertexAttributeDescription = attributeDescription;
}

void VKP::PipelineBlueprint::AddUniformLayout(const UniformDescriptorBlueprint &uniformDescriptor) {
    m_pUniformDescriptorBlueprints.emplace_back(uniformDescriptor);
}

void VKP::PipelineBlueprint::AddPushConstant(VkShaderStageFlags shaderStage, uint32_t offset, uint32_t size) {
    m_pPushConstants.emplace_back(shaderStage, offset, size);
}

void VKP::PipelineBlueprint::SetDepthStencilState(const VkPipelineDepthStencilStateCreateInfo &info) {
    m_pDepthStencilState = info;
    m_pHasDepthStencilState = true;
}

void VKP::PipelineBlueprint::SetCachedDescriptorSets(uint32_t cacheSize) {
    m_pCacheSize = cacheSize;
}

VKP::Pipeline::Pipeline(VKP::PipelineBlueprint blueprint) : m_pBlueprint(std::move(blueprint)), m_pValid(true) {
}

void VKP::Pipeline::Load() {
    if (!m_pValid) {
        VKP_LOG_WARN("Attempted to load invalid pipeline");
        return;
    }
    if (!m_pLoaded) {
        m_pBlueprint.PrepareShaders();
        auto data = m_pBlueprint._BuildPipeline();

        m_pPipeline = data.pipeline;
        m_pPipelineLayout = data.layout;
        m_pUniformDescriptor = data.uDescriptor;
        m_pUniformDescriptorSets = std::move(data.descriptorSets);
        m_pUniformDescriptionPool = data.descriptorPool;
        m_pHasUniforms = data.hasUniforms;

        m_pLoaded = true;
        VKP_LOG_DEBUG("Loaded pipeline");
    }
}

void VKP::Pipeline::Unload() {
    if (!m_pValid) {
        VKP_LOG_WARN("Attempted to unload invalid pipeline");
        return;
    }
    if (m_pLoaded) {
        auto &renderer = Renderer::m_Renderer;
        m_pDescSetOffset = 0;
        if (m_pHasUniforms) {
            vkDestroyDescriptorSetLayout(renderer->GetDevice(), m_pUniformDescriptor, nullptr);
            vkDestroyDescriptorPool(renderer->GetDevice(), m_pUniformDescriptionPool, nullptr);
        }
        vkDestroyPipelineLayout(renderer->GetDevice(), m_pPipelineLayout, nullptr);
        vkDestroyPipeline(renderer->GetDevice(), m_pPipeline, nullptr);
        m_pLoaded = false;
        m_pVersion++;
        VKP_LOG_DEBUG("Unloaded pipeline");
    }
}

void VKP::Pipeline::Refresh() {
    if (!m_pValid) {
        VKP_LOG_WARN("Attempted to refresh invalid pipeline");
        return;
    }
    if (m_pLoaded) {
        Unload();
        Load();
    }
}

VkPipeline VKP::Pipeline::GetPipeline() const {
    if (!m_pValid) {
        throw VKP::InitException("Tried to get invalid pipeline");
    }
    return m_pPipeline;
}

VkPipelineLayout VKP::Pipeline::GetLayout() const {
    if (!m_pValid) {
        throw VKP::InitException("Tried to get invalid pipeline layout");
    }
    return m_pPipelineLayout;
}

VKP::Pipeline::~Pipeline() {
    if (m_pValid) {
        //Unload();
    }
}

VKP::Pipeline::Pipeline(VKP::Pipeline &&o) {
    m_pPipeline = o.m_pPipeline;
    m_pPipelineLayout = o.m_pPipelineLayout;
    m_pBlueprint = std::move(o.m_pBlueprint);
    m_pValid = o.m_pValid;
    o.m_pValid = false;
    m_pLoaded = o.m_pLoaded;
    m_pVersion = o.m_pVersion;
}

void VKP::Pipeline::ReloadUniformBuffers() {

}

bool VKP::Pipeline::HasUniforms() const {
    return m_pHasUniforms;
}

void VKP::Pipeline::Bind(const VKP::FrameDrawData &frame) {
    vkCmdBindPipeline(frame.frameCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_pPipeline);
}

VKP::UniformBufferBinding VKP::Pipeline::CreateUniformBuffer(const UniformDescriptorBlueprint &descriptorBlueprint) {
    auto &renderer = VKP::Renderer::m_Renderer;
    VKP::UniformBufferBinding res{};
    std::vector<VkWriteDescriptorSet> descriptorWrites{renderer->GetSwapImageCount()};
    VkDescriptorBufferInfo bufferInfo{};
    VkDescriptorImageInfo imageInfo{};

    res.m_DescriptorSets.resize(renderer->GetSwapImageCount());

    res.m_Buffer = renderer->CreateUniformBuffer(descriptorBlueprint.size);

    for (uint32_t i = 0; i < renderer->GetSwapImageCount(); i++) {
        res.m_DescriptorSets.at(i) = m_pUniformDescriptorSets.at(m_pDescSetOffset++);
        bufferInfo.buffer = res.m_Buffer->GetBuffer().GetVulkanBuffer();
        bufferInfo.offset = descriptorBlueprint.offset;
        bufferInfo.range = descriptorBlueprint.size;

        descriptorWrites.at(i).sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites.at(i).dstSet = res.m_DescriptorSets.at(i);
        descriptorWrites.at(i).dstBinding = descriptorBlueprint.binding;
        descriptorWrites.at(i).dstArrayElement = 0;//FIXME: Add support for array of uniform buffers

        descriptorWrites.at(i).descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites.at(i).descriptorCount = descriptorBlueprint.descriptorCount;

        descriptorWrites.at(i).pBufferInfo = &bufferInfo;
        descriptorWrites.at(i).pImageInfo = nullptr; // TODO: Sampler support
        descriptorWrites.at(i).pTexelBufferView = nullptr; // Optional
    }
    res.m_Version = m_pVersion;
    vkUpdateDescriptorSets(renderer->GetDevice(), static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
    return res;
}

bool VKP::Pipeline::BindUniformBuffer(const VKP::FrameDrawData &frame, const VKP::UniformBufferBinding &uniform) {
    if(m_pVersion != uniform.m_Version) return false;
    vkCmdBindDescriptorSets(frame.frameCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_pPipelineLayout, 0, 1,
                            &uniform.m_DescriptorSets.at(VKP::Renderer::m_Renderer->GetCurrentImageIndex()), 0, nullptr);
    return true;
}

void VKP::Pipeline::RecreateUniformBuffer(VKP::UniformBufferBinding &uniform,
                                          const VKP::UniformDescriptorBlueprint &descriptorBlueprint) {
    auto &renderer = VKP::Renderer::m_Renderer;
    std::vector<VkWriteDescriptorSet> descriptorWrites{renderer->GetSwapImageCount()};
    VkDescriptorBufferInfo bufferInfo{};
    VkDescriptorImageInfo imageInfo{};
    uniform.m_Version = m_pVersion;
    for (uint32_t i = 0; i < renderer->GetSwapImageCount(); i++) {
        uniform.m_DescriptorSets.at(i) = m_pUniformDescriptorSets.at(m_pDescSetOffset++);
        bufferInfo.buffer = uniform.m_Buffer->GetBuffer().GetVulkanBuffer();
        bufferInfo.offset = descriptorBlueprint.offset;
        bufferInfo.range = descriptorBlueprint.size;

        descriptorWrites.at(i).sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites.at(i).dstSet = uniform.m_DescriptorSets.at(i);
        descriptorWrites.at(i).dstBinding = descriptorBlueprint.binding;
        descriptorWrites.at(i).dstArrayElement = 0;//FIXME: Add support for array of uniform buffers

        descriptorWrites.at(i).descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites.at(i).descriptorCount = descriptorBlueprint.descriptorCount;

        descriptorWrites.at(i).pBufferInfo = &bufferInfo;
        descriptorWrites.at(i).pImageInfo = nullptr; // TODO: Sampler support
        descriptorWrites.at(i).pTexelBufferView = nullptr; // Optional
    }
    vkUpdateDescriptorSets(renderer->GetDevice(), static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
}

VKP::PushConstantInfo::PushConstantInfo(VkShaderStageFlags shaderStage, uint32_t offset, uint32_t size) : shaderStage(
        shaderStage), offset(offset), size(size) {}
