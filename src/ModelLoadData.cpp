//
// Created by Matty on 2021-04-22.
//

#include "../app/engine/ModelLoadData.h"

std::ostream& VKP_Engine::operator<<(std::ostream& os, const VKP_Engine::ModelBaseData& obj) {
    os << obj.meshCount;
    os << obj.matCount;
    return os;
}

std::istream& VKP_Engine::operator>>(std::istream& is, VKP_Engine::ModelBaseData& obj) {
    is >> obj.meshCount;
    is >> obj.matCount;
    return is;
}