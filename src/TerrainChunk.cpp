//
// Created by Matty on 2021-04-24.
//

#include "../app/game/world_objects/TerrainChunk.h"
#include "../app/game/data/Game_WorldData.h"

VKP_Game::TerrainChunk::TerrainChunk(reactphysics3d::PhysicsWorld *physicsWorld, const glm::vec3 &pos, const glm::vec3 &rot,
                           const glm::vec3 &scale) : m_pValid(true),
                                                     m_pModelInstance(
                                                             Game_RenderData::GetInstance()->m_Cube1x1Model), m_pScale(scale), m_pRotation(rot) {

    reactphysics3d::Vector3 position(pos.x,pos.y,pos.z);
    reactphysics3d::Quaternion orientation = reactphysics3d::Quaternion::identity();
    reactphysics3d::Transform transform(position, orientation);

    m_pRigidBody = physicsWorld->createRigidBody(transform);
    m_pRigidBody->enableGravity(false);
    m_pRigidBody->setType(rp3d::BodyType::STATIC);

    const reactphysics3d::Vector3 halfExtents(1.0f * scale.x, 1.0f * scale.y, 1.0f * scale.z);

    m_pCollideShape = Game_WorldData::GetInstance()->m_PhysicsCommon.createBoxShape(halfExtents);

    m_pCollider = m_pRigidBody->addCollider(m_pCollideShape,reactphysics3d::Transform::identity());

    auto & unitRegistry = VKP_Game::Game_UnitRegistry::s_Instance;
    for(uint32_t i = 0; i < CHUNK_RES; i++) {
        for(uint32_t j = 0; j < CHUNK_RES; j++) {
            unitRegistry.AddGridEntity<VKP_Game::EmptyEntity>({i,j});
        }
    }
}


void VKP_Game::TerrainChunk::Draw(VKP::FrameDrawData &frame) {
    if (!m_pValid) return;

    auto & unitRegistry = VKP_Game::Game_UnitRegistry::s_Instance;

    for(uint32_t i = 0; i < CHUNK_RES; i++) {
        for(uint32_t j = 0; j < CHUNK_RES; j++) {
            auto entity = unitRegistry.GetIGridEntity({i,j});
            if(entity->IsRendered()) {
                if(!entity->m_ModelInstance.IsValid()) {
                    entity->m_ModelInstance = std::move(VKP::ModelInstance{entity->GetModel()});
                }
                auto pos = m_pPosition;
                float multiplier = (m_pScale.x * 2) / CHUNK_RES;
                auto posCornerX = m_pPosition.x - m_pScale.x + 0.5f * multiplier;
                auto posCornerZ = m_pPosition.z - m_pScale.z + 0.5f * multiplier;


                entity->m_ModelInstance.SetPosition({posCornerX + i * multiplier,posCornerZ + j * multiplier, pos.y + m_pScale.y * 2});

                entity->m_ModelInstance.Draw(frame);
            }
        }
    }

    auto physTransform = m_pRigidBody->getTransform();
    m_pPosition.x = physTransform.getPosition().x;
    m_pPosition.y = physTransform.getPosition().z;
    m_pPosition.z = physTransform.getPosition().y;

    auto & quatRot = physTransform.getOrientation();

    glm::quat quat{quatRot.w,quatRot.x,quatRot.y,quatRot.z};

    auto rotRad = glm::eulerAngles(quat);

    m_pRotation = glm::degrees(rotRad);
    m_pModelInstance.SetPosition(m_pPosition);
    m_pModelInstance.SetRotation(m_pRotation);
    m_pModelInstance.SetScale(m_pScale);
    m_pModelInstance.Draw(frame);
}

VKP_Game::TerrainChunk::TerrainChunk() {

}
