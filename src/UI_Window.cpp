//
// Created by Matty on 2021-03-27.
//

#include "../app/ui/UI_Window.h"
#include "../app/imgui/imgui.h"

void VKP::UI_Window::Draw() {
    CustomWindowSetup();

    if(ImGui::Begin(m_pName.c_str(), &m_pCollapsed, m_Flags)) {
        CustomDraw();
    }
    ImGui::End();

    CustomPostDraw();
}
