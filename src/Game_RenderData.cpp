//
// Created by Matty on 2021-04-23.
//

#include "../app/game/data/Game_RenderData.h"

std::unique_ptr<VKP_Game::Game_RenderData> &VKP_Game::Game_RenderData::GetInstance() {
    if(!s_pInstance) s_pInstance = std::make_unique<VKP_Game::Game_RenderData>();
    return s_pInstance;
}

std::unique_ptr<VKP_Game::Game_RenderData> VKP_Game::Game_RenderData::s_pInstance;
