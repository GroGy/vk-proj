//
// Created by Matty on 2021-03-27.
//

#include "../app/render/UniformBuffer.h"
#include "../app/render/Renderer.h"

VKP::UniformBuffer::UniformBuffer(VmaAllocator &allocator, uint64_t size) : m_pValid(true) {
    assert(size > 0);

    VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
    bufferInfo.size = size;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

    m_pBufferInfo = bufferInfo;

    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;

    m_pBuffers.resize(Renderer::m_Renderer->GetSwapImageCount());

    for(auto && buf : m_pBuffers) {
        buf = std::move(VKP::Buffer(allocator, m_pBufferInfo, allocInfo));
    }
}

VKP::UniformBuffer::UniformBuffer(VKP::UniformBuffer &&o) noexcept {
    m_pValid = o.m_pValid;
    m_pBuffers = std::move(o.m_pBuffers);
    m_pBufferInfo = o.m_pBufferInfo;
    o.m_pValid = false;
}

VKP::UniformBuffer &VKP::UniformBuffer::operator=(VKP::UniformBuffer &&o) noexcept {
    m_pValid = o.m_pValid;
    m_pBuffers = std::move(o.m_pBuffers);
    m_pBufferInfo = o.m_pBufferInfo;
    o.m_pValid = false;
    return *this;
}

void VKP::UniformBuffer::Cleanup(VmaAllocator const &allocator) {
    for(auto && b : m_pBuffers) {
        b.Cleanup(allocator);
    }
}

void VKP::UniformBuffer::MarkForDeletion() {
    for(auto && a : m_pBuffers){
        a.MarkForDeletion();
    }
}

bool VKP::UniformBuffer::MarkedForDeletion() const {
    for(auto && a : m_pBuffers){
        if(!a.MarkedForDeletion()) return false;
    }
    return true;
}

void VKP::UniformBuffer::LoadData(void *data, size_t size) {
    if(!m_pValid) {
        VKP_LOG_WARN("Tried to load data to invalid uniform buffer");
        return;
    }
    if(size == 0) return;
    auto currentIndex = VKP::Renderer::m_Renderer->GetCurrentImageIndex();
    m_pBuffers.at(currentIndex).LoadData(data,size);
}

void VKP::UniformBuffer::Reload(VmaAllocator & allocator) {
    if(!m_pValid) {
        VKP_LOG_WARN("Tried to reload invalid uniform buffer");
        return;
    }
    Cleanup(allocator);

    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;

    for(auto && buf : m_pBuffers) {
        buf = std::move(VKP::Buffer(allocator, m_pBufferInfo, allocInfo));
    }
}

void VKP::UniformBuffer::Bind() {

}

VKP::Buffer &VKP::UniformBuffer::GetBuffer() {
    return m_pBuffers.at(VKP::Renderer::m_Renderer->GetCurrentImageIndex());
}
