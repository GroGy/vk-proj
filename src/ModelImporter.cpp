//
// Created by Matty on 2021-03-28.
//

#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <fstream>
#include "../app/editor/ModelImporter.h"
#include "../app/engine/Serialization.h"

std::pair<bool, std::optional<std::string>> VKP_Editor::ModelImporter::ImportModel(const std::string &path) {
    using namespace VKP_Engine;
    VKP_LOG_DEBUG("Importing model: {}", path.c_str());
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(path, aiProcessPreset_TargetRealtime_Fast);

    if (!scene) {
        return std::pair<bool, std::optional<std::string>>(false, importer.GetErrorString());
    }

    VKP_Engine::ModelBaseData header{};
    VKP_Engine::ModelImportData data{};

    uint32_t meshIndex = 0;

    data.meshes.resize(scene->mNumMeshes);
    data.materials.resize(scene->mNumMaterials);

    auto root = scene->mRootNode;

    glm::mat4 offsetFinal = glm::mat4(1.0f);

    offsetFinal[0][0] = root->mTransformation.a1;
    offsetFinal[1][0] = root->mTransformation.a2;
    offsetFinal[2][0] = root->mTransformation.a3;
    offsetFinal[3][0] = root->mTransformation.a4*0.01;
    offsetFinal[0][1] = root->mTransformation.b1;
    offsetFinal[1][1] = root->mTransformation.b2;
    offsetFinal[2][1] = root->mTransformation.b3;
    offsetFinal[3][1] = root->mTransformation.c4*0.01;
    offsetFinal[0][2] = root->mTransformation.c1;
    offsetFinal[1][2] = root->mTransformation.c2;
    offsetFinal[2][2] = root->mTransformation.c3;
    offsetFinal[3][2] = root->mTransformation.b4*0.01;
    offsetFinal[0][3] = root->mTransformation.d1;
    offsetFinal[1][3] = root->mTransformation.d2;
    offsetFinal[2][3] = root->mTransformation.d3;
    offsetFinal[3][3] = root->mTransformation.d4*0.01;

    offsetFinal = glm::scale(offsetFinal, glm::vec3(0.01f));
    offsetFinal = glm::rotate(offsetFinal, glm::radians(90.0f),glm::vec3(1,0,0));
    offsetFinal = glm::rotate(offsetFinal, glm::radians(180.0f),glm::vec3(0,0,1));

    _CrawlNode(root, offsetFinal, scene, data, meshIndex);

    for (uint32_t i = 0; i < scene->mNumMaterials; i++) {
        data.materials.at(i);
    }

    assert(meshIndex == scene->mNumMeshes);

    header.meshCount = scene->mNumMeshes;
    header.matCount = scene->mNumMaterials;

    auto oPath = path.substr(0, path.find_last_of('.')) + ".vk_model";
    //Ojáj kebab
    std::ofstream file(oPath, std::ios::out | std::ios::binary);
    if (file.is_open()) {
        uint32_t version = VKP_MODEL_VERSION;
        file.write((char *) &version, sizeof(uint32_t));
        file.write((char *) &header, sizeof(struct VKP_Engine::ModelBaseData));
        for (uint32_t i = 0; i < data.meshes.size(); i++) {
            uint32_t vertexCount = data.meshes.at(i).vertices.size();
            uint32_t indexCount = data.meshes.at(i).indices.size();

            file.write((char *) &vertexCount, sizeof(uint32_t));
            file.write((char *) &indexCount, sizeof(uint32_t));
            file.write((char *) data.meshes.at(i).vertices.data(),
                       vertexCount * sizeof(struct VKP_Engine::ModelVertex));
            file.write((char *) data.meshes.at(i).indices.data(), indexCount * sizeof(uint32_t));
        }
        //TODO: Materials
        file.close();
        return std::pair<bool, std::optional<std::string>>(true, std::optional<std::string>());
    } else {
        return std::pair<bool, std::optional<std::string>>(false, "Failed to save file");
    }
}

uint32_t VKP_Editor::ModelImporter::GetVersion(std::ifstream &file) {
    auto versionRaw = std::vector<char>(sizeof(uint32_t) / 8);
    file.seekg(0, std::ios::beg);
    file.read(versionRaw.data(), sizeof(uint32_t));

    return (uint32_t) *versionRaw.data();
}

void VKP_Editor::ModelImporter::_BuildModel(const aiMesh *mesh, const glm::mat4 &transform, const aiMaterial *mat,
                                            VKP_Engine::ModelImportData &data, uint32_t meshIndex) {
    auto &meshData = data.meshes.at(meshIndex);

    meshData.vertices.resize(mesh->mNumVertices, VKP_Engine::ModelVertex{{},
                                                                         {},
                                                                         {}});

    bool hasNormals = mesh->HasNormals();
    bool hasUV = mesh->HasTextureCoords(0);
    bool hasColors = mesh->HasVertexColors(0);


    for (int32_t i = 0; i < mesh->mNumVertices; i++) {
        const auto importedVertex = mesh->mVertices[i];
        const auto convertedVertex = glm::vec4(importedVertex.x, importedVertex.y, importedVertex.z, 1.0f);

        aiVector3t<ai_real> importedNormal;
        if (hasNormals) {
            importedNormal = mesh->mNormals[i];
        }
        const auto convertedNormal = hasNormals ? glm::vec3(importedNormal.x, importedNormal.y, importedNormal.z)
                                                : glm::vec3(0.0f);

        aiVector3t<ai_real> importedUV;
        if (hasUV) {
            importedUV = mesh->mTextureCoords[0][i];
        }
        const auto convertedUV = hasUV ? glm::vec2(importedUV.x, importedUV.y) : glm::vec2(0.0f);

        aiColor4t<ai_real> importedVertexColor;
        if (hasColors) {
            importedVertexColor = mesh->mColors[i][0];
        }
        const auto convertedVertexColor = hasColors ? glm::vec3(importedVertexColor.r, importedVertexColor.g,
                                                                importedVertexColor.b)
                                                    : glm::vec3(1.0f);

        const auto vertPos4 = transform * convertedVertex;
        meshData.vertices.at(i).m_Pos = {vertPos4.x, vertPos4.y, vertPos4.z};
        meshData.vertices.at(i).m_Color = convertedVertexColor;
        meshData.vertices.at(i).m_Normal = convertedNormal;
    }

    meshData.indices.resize(mesh->mNumFaces * 3);

    for (uint32_t i = 0; i < mesh->mNumFaces; i++) {
        if (mesh->mFaces[i].mNumIndices == 3) {
            for (uint32_t j = 0; j < mesh->mFaces[i].mNumIndices; j++) {
                meshData.indices.at(i * 3 + j) = (mesh->mFaces[i].mIndices[j]);
            }
        }
    }
}

void
VKP_Editor::ModelImporter::_CrawlNode(aiNode *node, const glm::mat4 &transform, const aiScene *scene,
                                      VKP_Engine::ModelImportData &data,
                                      uint32_t &meshIndex) {
    glm::mat4 offsetFinal = glm::mat4(1.0f);

    offsetFinal[0][0] = node->mTransformation.a1;
    offsetFinal[1][0] = node->mTransformation.a2;
    offsetFinal[2][0] = node->mTransformation.a3;
    offsetFinal[3][0] = node->mTransformation.a4*0.01;

    offsetFinal[0][1] = node->mTransformation.b1;
    offsetFinal[1][1] = node->mTransformation.b2;
    offsetFinal[2][1] = node->mTransformation.b3;
    offsetFinal[3][1] = node->mTransformation.c4*0.01;

    offsetFinal[0][2] = node->mTransformation.c1;
    offsetFinal[1][2] = node->mTransformation.c2;
    offsetFinal[2][2] = node->mTransformation.c3;
    offsetFinal[3][2] = node->mTransformation.b4*0.01;

    offsetFinal[0][3] = node->mTransformation.d1;
    offsetFinal[1][3] = node->mTransformation.d2;
    offsetFinal[2][3] = node->mTransformation.d3;
    offsetFinal[3][3] = node->mTransformation.d4*0.01;
    const auto nextTransform = offsetFinal * transform;
    for (uint32_t i = 0; i < node->mNumChildren; i++) {
        for (uint32_t j = 0; j < node->mChildren[i]->mNumMeshes; j++) {

            glm::mat4 modelTrans = glm::mat4(1.0f);

            modelTrans[0][0] = node->mChildren[i]->mTransformation.a1;
            modelTrans[1][0] = node->mChildren[i]->mTransformation.a2;
            modelTrans[2][0] = node->mChildren[i]->mTransformation.a3;
            modelTrans[3][0] = node->mChildren[i]->mTransformation.a4*0.01;
            modelTrans[0][1] = node->mChildren[i]->mTransformation.b1;
            modelTrans[1][1] = node->mChildren[i]->mTransformation.b2;
            modelTrans[2][1] = node->mChildren[i]->mTransformation.b3;
            modelTrans[3][1] = node->mChildren[i]->mTransformation.c4*0.01;
            modelTrans[0][2] = node->mChildren[i]->mTransformation.c1;
            modelTrans[1][2] = node->mChildren[i]->mTransformation.c2;
            modelTrans[2][2] = node->mChildren[i]->mTransformation.c3;
            modelTrans[3][2] = node->mChildren[i]->mTransformation.b4*0.01;
            modelTrans[0][3] = node->mChildren[i]->mTransformation.d1;
            modelTrans[1][3] = node->mChildren[i]->mTransformation.d2;
            modelTrans[2][3] = node->mChildren[i]->mTransformation.d3;
            modelTrans[3][3] = node->mChildren[i]->mTransformation.d4*0.01;

            _BuildModel(scene->mMeshes[node->mChildren[i]->mMeshes[j]],
                        modelTrans * nextTransform,
                        scene->mMaterials[scene->mMeshes[node->mChildren[i]->mMeshes[j]]->mMaterialIndex], data,
                        meshIndex);
            meshIndex++;
        }
        _CrawlNode(node->mChildren[i], nextTransform, scene, data, meshIndex);
    }
}