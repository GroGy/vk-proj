//
// Created by Matty on 2021-03-26.
//

#include "../app/render/Texture.h"
#include "../app/Exceptions.h"
#include "../app/render/Renderer.h"

VkFormat VKP::Texture::_FindSupportedFormat(const std::vector<VkFormat> &candidates, VkImageTiling tiling,
                                            VkFormatFeatureFlags features) {
    for (VkFormat format : candidates) {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(VKP::Renderer::m_Renderer->GetPhysicalDevice(), format, &props);

        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
            return format;
        } else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
            return format;
        }
    }

    throw VKP::RuntimeException("failed to find supported format!");
}

VKP::Texture::Texture(const VmaAllocator &allocator, uint32_t width, uint32_t height, VkFormat format,
                      VkImageTiling tiling, VkImageUsageFlags usage, bool createImageView) : m_pValid(true),
                                                                                             m_pAllocator(allocator),
                                                                                             m_pFormat(format),
                                                                                             m_pWidth(width),
                                                                                             m_pHeight(height) {
    VkImageCreateInfo imageInfo{};
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width = width;
    imageInfo.extent.height = height;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = 1;
    imageInfo.arrayLayers = 1;
    imageInfo.format = format;
    imageInfo.tiling = tiling;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage = usage;
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    auto &renderer = VKP::Renderer::m_Renderer;

    if (vkCreateImage(renderer->GetDevice(), &imageInfo, nullptr, &m_pImage) != VK_SUCCESS) {
        throw VKP::RuntimeException("failed to create image!");
    }

    VkMemoryRequirements memReq;
    vkGetImageMemoryRequirements(renderer->GetDevice(), m_pImage, &memReq);

    VmaAllocationCreateInfo allocCreateInfo = {};
    allocCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

    vmaAllocateMemory(allocator, &memReq, &allocCreateInfo, &m_pAllocation, nullptr);

    vmaBindImageMemory(allocator, m_pAllocation, m_pImage);

    if (createImageView) {
        _CreateImageView(format, VK_IMAGE_ASPECT_COLOR_BIT);
    }
}

void VKP::Texture::LoadTexture(const std::shared_ptr<VKP::StagingBuffer> &buffer) {
    auto &renderer = VKP::Renderer::m_Renderer;
    renderer->TransitionTextureLayout(*this,m_pFormat,VK_IMAGE_LAYOUT_UNDEFINED,VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    renderer->CopyBufferToTexture(buffer->GetBuffer(),*this,m_pWidth,m_pHeight);
    renderer->TransitionTextureLayout(*this,m_pFormat,VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
}

bool VKP::Texture::MarkedForDeletion() const {
    return m_pMarkedForDeletion;
}

void VKP::Texture::MarkForDeletion() {
    m_pMarkedForDeletion = true;
}

void VKP::Texture::_CreateImageView(VkFormat format, VkImageAspectFlags aspectFlags) {

    VkImageViewCreateInfo viewInfo{};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = m_pImage;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;
    viewInfo.subresourceRange.aspectMask = aspectFlags;

    if (vkCreateImageView(VKP::Renderer::m_Renderer->GetDevice(), &viewInfo, nullptr, &m_pImageView) != VK_SUCCESS) {
        throw VKP::RuntimeException("failed to create texture image view!");
    }
    m_pHasImageView = true;
}

void VKP::Texture::Cleanup() {
    if (m_pValid) {
        auto &renderer = VKP::Renderer::m_Renderer;
        if (m_pHasImageView) {
            vkDestroyImageView(renderer->GetDevice(), m_pImageView, nullptr);
        }
        vmaDestroyImage(m_pAllocator, m_pImage, m_pAllocation);
    }
}

VkImageView VKP::Texture::GetView() const {
    return m_pImageView;
}

VkImage VKP::Texture::GetImage() const {
    return m_pImage;
}

VkFormat VKP::Texture::GetFormat() const {
    return m_pFormat;
}
