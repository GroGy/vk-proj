//
// Created by Matty on 2021-03-26.
//

#include "../app/render/VertexBuffer.h"

bool VKP::VertexBuffer::MarkedForDeletion() const {
    return m_pBuffer.MarkedForDeletion();
}

VKP::VertexBuffer::VertexBuffer(VmaAllocator &allocator, uint64_t size) : m_pValid(true) {
    assert(size > 0);

    VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
    bufferInfo.size = size;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
    m_pBuffer = VKP::Buffer(allocator, bufferInfo, allocInfo);
}

void VKP::VertexBuffer::MarkForDeletion() {
    m_pBuffer.MarkForDeletion();
}

void VKP::VertexBuffer::LoadData(const void *data,size_t size, uint32_t vertexCount) {
    if(!m_pValid) {
        VKP_LOG_WARN("Tried to load data to invalid vertex buffer");
        return;
    }
    if(size == 0) return;
    m_pBuffer.LoadData(data,size);
    m_pVertexCount = vertexCount;
}

VKP::VertexBuffer::VertexBuffer(VKP::VertexBuffer &&o) noexcept {
    m_pValid = o.m_pValid;
    m_pBuffer = std::move(o.m_pBuffer);
    m_pVertexCount = o.m_pVertexCount;
    o.m_pValid = false;
}

VKP::VertexBuffer &VKP::VertexBuffer::operator=(VKP::VertexBuffer &&o) noexcept {
    m_pValid = o.m_pValid;
    m_pBuffer = std::move(o.m_pBuffer);
    m_pVertexCount = o.m_pVertexCount;
    o.m_pValid = false;
    return *this;
}

void VKP::VertexBuffer::Cleanup(VmaAllocator const &allocator) {
    m_pBuffer.Cleanup(allocator);
}

VKP::VertexBuffer::~VertexBuffer() {
    assert(!m_pValid || m_pBuffer.MarkedForDeletion());
}

void VKP::VertexBuffer::BindVertexBuffer(VkCommandBuffer cmdBuffer) {
    VkBuffer vertexBuffers[] = {m_pBuffer.GetVulkanBuffer() };
    VkDeviceSize offsets[] = {0};
    vkCmdBindVertexBuffers(cmdBuffer, 0, 1, vertexBuffers, offsets);
}

uint32_t VKP::VertexBuffer::GetVertexCount() const {
    return m_pVertexCount;
}

void VKP::VertexBuffer::LoadData(const VKP::StagingBuffer &staging) {

}

VKP::Buffer &VKP::VertexBuffer::GetBuffer() {
    return m_pBuffer;
}
