#version 450
#extension GL_ARB_separate_shader_objects : enable
layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 normal;

layout(location = 0) out vec4 outColor;

void main() {
    vec3 lightColor = vec3(1.0);

    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

    vec3 lightDir = vec3(-1,1,1);
    lightDir = normalize(lightDir);
    vec3 norm = normalize(normal);

    float diff = max(dot(norm, lightDir), 0.2);
    vec3 diffuse = diff * lightColor;

    vec3 result = (diffuse + ambient) * fragColor;

    result -= (norm / 20.0f);

    outColor = vec4(result,1.0);
}