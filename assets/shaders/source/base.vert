#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inCol;
layout(location = 2) in vec3 inNormal;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec3 normal;

layout( push_constant ) uniform constants
{
    mat4 model;
    mat4 MV;
} push_block;

void main() {
    gl_Position = ubo.proj*ubo.view*ubo.model*vec4(inPos, 1.0);
    fragColor = inCol;
    mat4 noTransMat = push_block.model;
    noTransMat[3][0] = 0;
    noTransMat[3][1] = 0;
    noTransMat[3][2] = 0;
    normal = (noTransMat*vec4(normalize(inNormal),1.0)).xyz;
}