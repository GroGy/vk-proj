//
// Created by Matty on 2021-03-24.
//

#ifndef VK_PROJ_UTILFILE_H
#define VK_PROJ_UTILFILE_H

#include <string>
#include <fstream>
#include <sys/stat.h>

namespace VKP {
    class UtilFile {
    public:
        /// Checks efficiently if file exists
        /// \param path Path to file that should be checked
        /// \return If file exists
        static bool FileExists(const std::string &path) {
            struct stat buffer;
            return (stat(path.c_str(), &buffer) == 0);
        }
    };
}


#endif //VK_PROJ_UTILFILE_H
