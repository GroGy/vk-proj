//
// Created by Matty on 2021-03-27.
//

#ifndef VK_PROJ_UI_PERFORMANCE_H
#define VK_PROJ_UI_PERFORMANCE_H

#include "../UI_Window.h"

namespace VKP {
    class UI_Performance : public UI_Window {
    public:
        UI_Performance();
    protected:
        void CustomDraw() override;
    };
}


#endif //VK_PROJ_UI_PERFORMANCE_H
