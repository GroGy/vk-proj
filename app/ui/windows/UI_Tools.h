//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_UI_TOOLS_H
#define VK_PROJ_UI_TOOLS_H

#include "../UI_Window.h"
#include "../../imgui/imgui.h"
#include "../../imgui/imfilebrowser.h"

namespace VKP {
    class UI_Tools : public UI_Window {
    private:
        int32_t m_pSelectedItem = 0;
        bool m_pSelectingModel = false;
        bool m_pLoadingModel = false;
        bool m_pLoadingImage = false;
        bool m_pFileBrowserOpen = false;
        ImGui::FileBrowser m_pFileBrowser;

        bool m_pPrefabEditorOpenned = false;
    public:
        UI_Tools();
    protected:
        void CustomDraw() override;

        void CustomPostDraw() override;
    };

}

#endif //VK_PROJ_UI_TOOLS_H
