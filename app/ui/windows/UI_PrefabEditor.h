//
// Created by Matty on 2021-06-27.
//

#ifndef VK_PROJ_UI_PREFABEDITOR_H
#define VK_PROJ_UI_PREFABEDITOR_H

#include "../UI_Window.h"
#include "../../imgui/imgui.h"
#include "../../imgui/imfilebrowser.h"

namespace VKP {
    enum SelectingTextureType {
        SELECTING_ALBEDO,
        SELECTING_NORMAL,
        SELECTING_ROUGHNESS,
        SELECTING_CUSTOM
    };

    class UI_PrefabEditor : public UI_Window {
    private:
        int32_t m_pSelectedItem = 0;
        bool m_pSelectingMesh = false;
        SelectingTextureType m_SelectingTextureType = SelectingTextureType::SELECTING_ALBEDO;
        bool m_pSelectingTexture = false;
        ImGui::FileBrowser m_pFileBrowser;
    public:
        UI_PrefabEditor();
    protected:
        void CustomDraw() override;

        void CustomPostDraw() override;
    };

}


#endif //VK_PROJ_UI_PREFABEDITOR_H
