//
// Created by Matty on 2021-03-27.
//

#ifndef VK_PROJ_UI_WINDOW_H
#define VK_PROJ_UI_WINDOW_H

#include <string>

namespace VKP {
    class UI_Window {
    private:
        bool m_pCollapsed = false;
        std::string m_pName;
        bool m_pPopupInit = false;
    public:
        UI_Window(const std::string& name) : m_pName(name){ };
        void Draw();
        bool GetCollapsed() const { return m_pCollapsed;}
    protected:
        int m_Flags = 0;
        virtual void CustomPostDraw() {};
        virtual void CustomDraw() = 0;
        virtual void CustomWindowSetup() {};
    };
}


#endif //VK_PROJ_UI_WINDOW_H
