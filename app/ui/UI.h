//
// Created by Matty on 2021-03-26.
//

#ifndef VK_PROJ_UI_H
#define VK_PROJ_UI_H

#include "../app/imgui/imgui.h"
#include "../app/imgui/imgui_impl_glfw.h"
#include "../app/imgui/imgui_impl_vulkan.h"
#include "../render/Window.h"
#include "UI_Window.h"

namespace VKP {
    class UIInternals {
    private:
        bool m_pFontsUploaded = false;
    public:
        void Init(const std::shared_ptr<VKP::Window> & window);
        void Cleanup();
        bool FontUploaded() const;
        void UploadFonts(VkCommandBuffer cmd);
    };

    class UI {
    public:
        std::vector<std::shared_ptr<UI_Window>> m_Windows;

        UI();

        void Draw(const std::shared_ptr<UIInternals> &data, VKP::FrameDrawData frameCmdBuffer);

        bool IsWorldViewVisible();
    };
}

#endif //VK_PROJ_UI_H
