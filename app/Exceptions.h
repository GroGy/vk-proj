//
// Created by Matty on 2021-03-22.
//

#ifndef VK_PROJ_EXCEPTIONS_H
#define VK_PROJ_EXCEPTIONS_H

#include <exception>

namespace VKP {
    class InitException : public std::exception {
    private:
        std::string m_pWhat;
    public:
        InitException(std::string what);
        const char *what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_NOTHROW override;
    };

    class RuntimeException : public std::exception {
    private:
        std::string m_pWhat;
    public:
        RuntimeException(std::string what);
        const char *what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_NOTHROW override;
    };
}


#endif //VK_PROJ_EXCEPTIONS_H
