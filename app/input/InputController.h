//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_INPUTCONTROLLER_H
#define VK_PROJ_INPUTCONTROLLER_H

#include <map>
#include "../render/Window.h"

#define VKP_LEFT_JOYSTICK 1
#define VKP_RIGHT_JOYSTICK 2

namespace VKP {
    enum InputType {
        VKP_INPUT_KEYBOARD = 0,
        VKP_INPUT_MOUSE = 1,
        VKP_INPUT_CONTROLLER_BUTTON = 2,
        VKP_INPUT_CONTROLLER_AXIS = 3
    };

    struct InputKey {
        InputType type;
        uint32_t key;
        bool wasPressed = false;
        bool valid = false;
        InputKey(InputType type, uint32_t key);
        InputKey() = default;
    };

    class InputController {
    private:
        static std::unique_ptr<InputController> s_pInstance;
        std::map<std::string, InputKey> m_pInputMap;
        glm::vec2 _GetVec2Input(uint32_t joystick);
        std::shared_ptr<VKP::Window> m_pWindow;
    public:
        static std::unique_ptr<InputController> & GetInstance();
        void Init(const std::shared_ptr<VKP::Window> & window);

        void RegisterKeyInputs(std::map<std::string, InputKey> data);
        void RegisterKeyInput(const std::string & name, InputKey data);

        bool IsKeyPressed(const std::string & inputName);
        bool IsKeyReleased(const std::string & inputName);
        bool OnKeyPressed(const std::string & inputName);
        bool OnKeyReleased(const std::string & inputName);

        std::pair<double, double> GetMousePos();
        glm::vec2 GetMousePosNormalized();

        double GetMouseScroll();

        float GetTrigger1D(const std::string & inputName);
        glm::vec2 GetTrigger2D(const std::string & inputName);

        glm::vec3 ScreenToWorldSpace(const glm::mat4 & pers, const glm::mat4 & viewMat);
    };
}


#endif //VK_PROJ_INPUTCONTROLLER_H
