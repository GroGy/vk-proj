//
// Created by Matty on 2021-03-26.
//

#ifndef VK_PROJ_MESH_H
#define VK_PROJ_MESH_H

#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "UniformBuffer.h"
#include "platform/Pipeline.h"
#include "../engine/ModelVertex.h"
#include "FrameDrawData.h"

namespace VKP {
    struct PushConstMesh {
        glm::mat4 model;
        glm::mat4 viewModel;
    };

    class Mesh {
    public:
        Mesh();
        virtual void Load(const void * vertices, uint32_t verticesLength, const uint32_t * indices,uint32_t indexCount, uint32_t vertexCount);
        void Draw(const VKP::FrameDrawData & frameCmdBuffer, const glm::mat4 & matrix);
        void Bind(const VKP::FrameDrawData & frameCmdBuffer);
        void BindUniformBuffer(const VKP::FrameDrawData & frameCmdBuffer, VKP::UniformBufferBinding & uniformBuffer);
        ~Mesh();
        Mesh(Mesh&& o) noexcept;
        Mesh& operator=(Mesh&& o) noexcept;
        Mesh(const Mesh& o) = delete;
        Mesh& operator=(const Mesh& o) = delete;
        friend class ModelInstance;
    private:
        bool m_pValid = false;
        std::shared_ptr<VKP::VertexBuffer> m_pVertexBuffer;
        std::shared_ptr<VKP::IndexBuffer> m_pIndexBuffer;
        std::shared_ptr<VKP::Pipeline> m_pPipeline;
        //TODO: Material
    };
}


#endif //VK_PROJ_MESH_H
