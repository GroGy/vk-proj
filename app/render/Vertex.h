//
// Created by Matty on 2021-03-26.
//

#ifndef VK_PROJ_VERTEX_H
#define VK_PROJ_VERTEX_H

#include <array>
#include <cstddef>

//This is just helper for how to write, vertex, just inherit

/* formats
 * float:   VK_FORMAT_R32_SFLOAT
 * vec2:    VK_FORMAT_R32G32_SFLOAT
 * vec3:    VK_FORMAT_R32G32B32_SFLOAT
 * vec4:    VK_FORMAT_R32G32B32A32_SFLOAT
 */

namespace VKP {
    struct Vertex {
    private:
        //glm::vec3 m_pPos;
        //glm::vec3 m_pColor;
    public:
        //static std::vector<VkVertexInputBindingDescription> getBindingDescription() {
        //        std::vector<VkVertexInputBindingDescription> bindingDescription(1);
        //        bindingDescription[0].binding = 0;
        //        bindingDescription[0].stride = sizeof(Vertex);
        //        bindingDescription[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        //        return bindingDescription;
        //    }

        //static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions() {
        //    std::vector<VkVertexInputAttributeDescription> attributeDescriptions(2);
        //
        //    attributeDescriptions[0].binding = 0;
        //    attributeDescriptions[0].location = 0;
        //    attributeDescriptions[0].format = VK_FORMAT_R32G32R32_SFLOAT;
        //    attributeDescriptions[0].offset = offsetof(Vertex, m_pPos);

        //    attributeDescriptions[1].binding = 0;
        //    attributeDescriptions[1].location = 1;
        //    attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
        //    attributeDescriptions[1].offset = offsetof(Vertex, m_pColor);
        //
        //    return attributeDescriptions;
        //}
    };
}


#endif //VK_PROJ_VERTEX_H
