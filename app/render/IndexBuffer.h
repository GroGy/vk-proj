//
// Created by Matty on 2021-03-27.
//

#ifndef VK_PROJ_INDEXBUFFER_H
#define VK_PROJ_INDEXBUFFER_H

#include "platform/Buffer.h"

namespace VKP {
    class IndexBuffer {
    private:
        VKP::Buffer m_pBuffer;
        bool m_pValid = false;
        uint32_t m_pIndexCount = 0;
    public:
        IndexBuffer() = default;
        ~IndexBuffer();
        IndexBuffer(VmaAllocator & allocator, uint32_t count);

        IndexBuffer(IndexBuffer&& o) noexcept;
        IndexBuffer& operator=(IndexBuffer&& o) noexcept;
        IndexBuffer(const IndexBuffer& o) = delete;
        IndexBuffer& operator=(const IndexBuffer& o) = delete;

        void Cleanup(const VmaAllocator & allocator);
        void MarkForDeletion();
        bool MarkedForDeletion() const;
        void LoadData(const uint32_t *data,size_t count);
        void BindIndexBuffer(VkCommandBuffer cmdBuffer);
        uint32_t GetIndexCount() const;
        VKP::Buffer & GetBuffer();
    };
}


#endif //VK_PROJ_INDEXBUFFER_H
