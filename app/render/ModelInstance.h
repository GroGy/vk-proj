//
// Created by Matty on 2021-04-23.
//

#ifndef VK_PROJ_MODELINSTANCE_H
#define VK_PROJ_MODELINSTANCE_H

#include "Model.h"

namespace VKP {
    class ModelInstance {
    private:
        std::shared_ptr<VKP::Model> m_pModel;
        bool m_pValid = false;
        glm::vec3 m_pPosition{0.0f, 0.0f, 0.0f};
        glm::vec3 m_pRotation{0.0f, 0.0f, 0.0f};
        glm::vec3 m_pScale{1.0f, 1.0f, 1.0f};
        glm::mat4 m_pModelMatrix;

        std::vector<VKP::UniformBufferBinding> m_pModelBindings;

        bool m_pModelMatrixInvalid = true;
    public:
        ModelInstance() = default;

        ModelInstance(std::shared_ptr<VKP::Model>  model, const glm::vec3 &pos = {0.0f, 0.0f, 0.0f},
                      const glm::vec3 &rot = {0.0f, 0.0f, 0.0f}, const glm::vec3 &scale = {1.0f, 1.0f, 1.0f});

        const glm::vec3 &GetPosition() const;

        void SetPosition(const glm::vec3 &position);

        const glm::vec3 &GetRotation() const;

        void SetRotation(const glm::vec3 &rotation);

        const glm::vec3 &GetScale() const;

        void SetScale(const glm::vec3 &scale);

        bool IsValid() const;

        void Draw(const VKP::FrameDrawData &frame);
    };
}

#endif //VK_PROJ_MODELINSTANCE_H
