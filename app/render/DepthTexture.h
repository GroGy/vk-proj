//
// Created by Matty on 2021-04-05.
//

#ifndef VK_PROJ_DEPTHTEXTURE_H
#define VK_PROJ_DEPTHTEXTURE_H

#include "Texture.h"

namespace VKP {
    class DepthTexture : public Texture {
    private:
        VkFormat _FindDepthFormat();
    public:
        DepthTexture() = default;
        DepthTexture(const VmaAllocator & allocator);
    };
}


#endif //VK_PROJ_DEPTHTEXTURE_H
