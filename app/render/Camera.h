//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_CAMERA_H
#define VK_PROJ_CAMERA_H

#include <reactphysics3d/collision/Collider.h>
#include <reactphysics3d/collision/shapes/BoxShape.h>

namespace VKP {
    struct Camera {
    private:
        glm::mat4 m_pViewMatrix;
        glm::vec3 m_pPosition{0.01f,0.0f,0.0f};
        glm::vec3 m_pPositionTarget{0.0f,0.0f,0.0f};
        glm::vec3 m_pFocusPosition{0.0f,0.0f,0.0f};
        glm::vec3 m_pRotation{0.0f,0.0f,0.0f};
        bool m_pViewMatrixInvalid = true;
        float m_pFOV = 90.0f;
        float m_pScreenRatio = 16.0f/9.0f;
        float m_pNearPlane = 0.001f;
        float m_pFarPlane = 1000.0f;
        reactphysics3d::CollisionBody* m_pCollisionBody = nullptr;
        reactphysics3d::Collider* m_pViewFrustum = nullptr;
        reactphysics3d::BoxShape * m_pBoxShape = nullptr;
    public:
        void Init();
        void Cleanup();

        [[nodiscard]] const glm::vec3 &GetPosition() const;
        void SetPosition(const glm::vec3 &position);
        [[nodiscard]] const glm::vec3 &GetFocusPosition() const;
        void SetFocusPosition(const glm::vec3 &position);
        [[nodiscard]] const glm::vec3 &GetRotation() const;
        void SetRotation(const glm::vec3 &rotation);
        void CalcView();

        void UpdateScreenRatio(float ratio);
        void Update(float delta);

        [[nodiscard]] glm::mat4 GetViewMatrix() const;
        [[nodiscard]] glm::mat4 GetProjMatrix() const;

        glm::vec3 GetForwardVector() const;
        glm::vec2 GetForwardVectorXZ() const;
        glm::vec2 GetRightVectorXZ() const;
    };
}


#endif //VK_PROJ_CAMERA_H
