//
// Created by Matty on 2021-04-05.
//

#ifndef VK_PROJ_STAGINGBUFFER_H
#define VK_PROJ_STAGINGBUFFER_H

#include "platform/Buffer.h"

namespace VKP {
    class StagingBuffer{
    private:
        VKP::Buffer m_pBuffer;
        bool m_pValid = false;
    public:
        StagingBuffer() = default;
        StagingBuffer(VmaAllocator & allocator, uint64_t size);

        StagingBuffer(StagingBuffer&& o) noexcept;
        StagingBuffer& operator=(StagingBuffer&& o) noexcept;
        StagingBuffer(const StagingBuffer& o) = delete;
        StagingBuffer& operator=(const StagingBuffer& o) = delete;

        void Cleanup(const VmaAllocator & allocator);
        void MarkForDeletion();
        bool MarkedForDeletion() const;

        void LoadData(const void* data, uint32_t size);

        VKP::Buffer & GetBuffer();
    };
}


#endif //VK_PROJ_STAGINGBUFFER_H
