//
// Created by Matty on 29.8.2020.
//

#ifndef VKP_WINDOWDESTRUCTOR_H
#define VKP_WINDOWDESTRUCTOR_H

#include <GLFW/glfw3.h>

namespace VKP {
    struct GLFWWindowDestructor {
        void operator()(GLFWwindow *ptr) {
            glfwDestroyWindow(ptr);
        }
    };
}

#endif //VKP_WINDOWDESTRUCTOR_H
