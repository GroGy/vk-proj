//
// Created by Matty on 2021-04-22.
//

#ifndef VK_PROJ_FRAMEBUFFER_H
#define VK_PROJ_FRAMEBUFFER_H

#include "platform/RenderPass.h"

namespace VKP {
    struct FrameBufferBlueprintResult {
        VkFramebuffer m_FrameBuffer;
    };

    class FrameBufferBlueprint {
    public:
        FrameBufferBlueprint() = default;

        FrameBufferBlueprint(const VKP::RenderPass &renderPass, uint32_t width, uint32_t height,
                             std::vector<VkImageView> attachments, uint32_t layers = 1);

        friend class FrameBuffer;

        bool IsValid() const;
    private:
        bool m_pValid = false;
        uint32_t m_pWidth;
        uint32_t m_pHeight;
        uint32_t m_pLayers;
        std::vector<VkImageView> m_pAttachments;
        VkRenderPass m_pRenderPass;

        FrameBufferBlueprintResult _BuildFrameBuffer();
    };

    class FrameBuffer {
    private:
        bool m_pValid = false;
        VkFramebuffer m_pFrameBuffer;
        FrameBufferBlueprint m_pBlueprint;
        bool m_pMarkedForDeletion = false;
    public:
        FrameBuffer() = default;
        FrameBuffer(FrameBufferBlueprint blueprint);

        void SetSize(uint32_t width, uint32_t height);

        void Load();

        void Unload();

        void Cleanup();

        void MarkForDeletion();

        bool MarkedForDeletion() const;

        VkFramebuffer GetFrameBuffer() const;
    };
}


#endif //VK_PROJ_FRAMEBUFFER_H
