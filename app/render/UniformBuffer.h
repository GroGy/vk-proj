//
// Created by Matty on 2021-03-27.
//

#ifndef VK_PROJ_UNIFORMBUFFER_H
#define VK_PROJ_UNIFORMBUFFER_H

#include "platform/Buffer.h"

namespace VKP {
    class UniformBuffer {
    private:
        VkBufferCreateInfo m_pBufferInfo;
        std::vector<VKP::Buffer> m_pBuffers;
        bool m_pValid = false;
    public:
        UniformBuffer() = default;
        UniformBuffer(VmaAllocator & allocator, uint64_t size);

        UniformBuffer(UniformBuffer&& o) noexcept;
        UniformBuffer& operator=(UniformBuffer&& o) noexcept;
        UniformBuffer(const UniformBuffer& o) = delete;
        UniformBuffer& operator=(const UniformBuffer& o) = delete;

        void Cleanup(const VmaAllocator & allocator);
        void MarkForDeletion();
        bool MarkedForDeletion() const;
        void LoadData(void *data,size_t size);
        void Reload(VmaAllocator & allocator);

        VKP::Buffer & GetBuffer();

        void Bind();
    };
}


#endif //VK_PROJ_UNIFORMBUFFER_H
