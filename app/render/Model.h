//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_MODEL_H
#define VK_PROJ_MODEL_H

#include "Mesh.h"
#include "FrameDrawData.h"

namespace VKP {
    class Model {
    private:
        std::vector<VKP::Mesh> m_pMeshes;
        bool m_pLoaded = false;
        bool m_pValid = false;
        bool m_pModelMatrixInvalid = true;
    public:
        Model() : m_pValid(false){};
        Model(std::vector<VKP::Mesh> meshes);
        bool IsValid() const;
        void Draw(const VKP::FrameDrawData & frame, const glm::mat4 & modelMatrix, std::vector<VKP::UniformBufferBinding> & bindings);

        Model(Model&& o) noexcept;
        Model& operator=(Model&& o) noexcept;
        Model(const Model& o) = delete;
        Model& operator=(const Model& o) = delete;

        friend class ModelInstance;
    };
}


#endif //VK_PROJ_MODEL_H
