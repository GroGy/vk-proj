//
// Created by Matty on 2021-03-24.
//

#ifndef VK_PROJ_RENDERDATA_H
#define VK_PROJ_RENDERDATA_H

#include "platform/Shader.h"
#include "platform/Pipeline.h"
#include "VertexBuffer.h"
#include "UniformBuffer.h"
#include "IndexBuffer.h"
#include "Model.h"
#include "Texture.h"

namespace VKP {
    class RenderData {
    private:
        static std::unique_ptr<RenderData> s_pInstance;
    public:
        RenderData();
        static std::unique_ptr<RenderData> & GetInstance();

        std::vector<VKP::Shader> m_ShaderModules;
        std::vector<std::shared_ptr<VKP::Pipeline>> m_Pipelines;
        std::vector<std::shared_ptr<VKP::VertexBuffer>> m_VertexBuffers;
        std::vector<std::shared_ptr<VKP::IndexBuffer>> m_IndexBuffers;
        std::vector<std::shared_ptr<VKP::UniformBuffer>> m_UniformBuffers;
        std::vector<std::shared_ptr<VKP::StagingBuffer>> m_StagingBuffers;
        std::vector<std::shared_ptr<VKP::Texture>> m_Textures;
        std::vector<std::shared_ptr<VKP::Model>> m_Models;

        glm::vec4 m_ClearColor;
    };
}


#endif //VK_PROJ_RENDERDATA_H
