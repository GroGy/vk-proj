//
// Created by Matty on 2021-03-24.
//

#ifndef VK_PROJ_PIPELINE_H
#define VK_PROJ_PIPELINE_H

#include <vector>
#include "Shader.h"
#include "../UniformBuffer.h"
#include "../FrameDrawData.h"
#include "../Texture.h"
#include "../UniformBufferBinding.h"

namespace VKP {
    class Pipeline;

    struct PushConstantInfo {
        PushConstantInfo() = default;

        PushConstantInfo(VkShaderStageFlags shaderStage, uint32_t offset, uint32_t size);

        VkShaderStageFlags shaderStage;
        uint32_t offset;
        uint32_t size;
    };

    struct PipelineBlueprintResult {
        VkPipeline pipeline;
        VkPipelineLayout layout;
        VkDescriptorSetLayout uDescriptor;
        VkDescriptorPool descriptorPool;
        std::vector<VkDescriptorSet> descriptorSets;
        bool hasUniforms = false;
    };

    enum UniformDescriptorType {
        VKP_UNIFORM_DESCRIPTOR_BUFFER,
        VKP_UNIFORM_DESCRIPTOR_TEXTURE
    };

    struct UniformDescriptorBlueprint {
    public:
        UniformDescriptorBlueprint() = default;

        UniformDescriptorBlueprint(VkShaderStageFlags stage, uint32_t binding, uint32_t descriptorCount,
                                   uint32_t offset,
                                   uint32_t size) : shaderStage(
                stage), binding(binding), descriptorCount(descriptorCount), size(size),
                                                    offset(offset) , type(VKP_UNIFORM_DESCRIPTOR_BUFFER) {};
        VkShaderStageFlags shaderStage;
        uint32_t binding;
        uint32_t descriptorCount;
        uint32_t offset;
        uint32_t size;
        UniformDescriptorType type;
    };

    struct UniformImageSamplerBlueprint {
        VKP::Texture m_Texture;
    };

    class PipelineBlueprint {
    private:
        uint32_t m_pCacheSize = 1;

        std::vector<uint64_t> m_pShaders;
        std::vector<ShaderType> m_pAssignedStages;

        std::vector<VkVertexInputBindingDescription> m_pVertexInputDescription;
        std::vector<VkVertexInputAttributeDescription> m_pVertexAttributeDescription;

        std::vector<PushConstantInfo> m_pPushConstants;

        std::vector<UniformDescriptorBlueprint> m_pUniformDescriptorBlueprints;

        PipelineBlueprintResult _BuildPipeline();

        bool m_pHasDepthStencilState = false;
        VkPipelineDepthStencilStateCreateInfo m_pDepthStencilState;
    public:
        friend class Pipeline;

        PipelineBlueprint();

        PipelineBlueprint(PipelineBlueprint &&o);

        PipelineBlueprint &operator=(PipelineBlueprint &&o) noexcept;

        void Prepare();

        void PrepareShaders();

        void AddShaderStage(const VKP::Shader &shader);

        void AddUniformLayout(const UniformDescriptorBlueprint &uniformDescriptor);

        void AddPushConstant(VkShaderStageFlags shaderStage, uint32_t offset, uint32_t size);

        void SetVertexInputDescription(const std::vector<VkVertexInputBindingDescription> &inputBindingDescription,
                                       const std::vector<VkVertexInputAttributeDescription> &attributeDescription);

        void SetDepthStencilState(const VkPipelineDepthStencilStateCreateInfo &info);

        void SetCachedDescriptorSets(uint32_t cacheSize);
    };

    class Pipeline {
    private:
        uint16_t m_pVersion = 0;
        uint32_t m_pDescSetOffset = 0;
        VkPipelineLayout m_pPipelineLayout;
        VkPipeline m_pPipeline;
        VkDescriptorSetLayout m_pUniformDescriptor;
        VkDescriptorPool m_pUniformDescriptionPool;
        std::vector<VkDescriptorSet> m_pUniformDescriptorSets;
        bool m_pValid = false;
        bool m_pLoaded = false;
        bool m_pHasUniforms = false;

        PipelineBlueprint m_pBlueprint;
    public:
        Pipeline() = default;

        Pipeline(PipelineBlueprint blueprint);

        Pipeline(Pipeline &&o);

        ~Pipeline();

        void Refresh();

        void Load();

        void Unload();

        void ReloadUniformBuffers();

        bool HasUniforms() const;

        VkPipeline GetPipeline() const;

        VkPipelineLayout GetLayout() const;

        void Bind(const VKP::FrameDrawData &frame);

        UniformBufferBinding
        CreateUniformBuffer(const UniformDescriptorBlueprint & descriptorBlueprint);

        void RecreateUniformBuffer(UniformBufferBinding & uniform,const UniformDescriptorBlueprint & descriptorBlueprint);

        bool BindUniformBuffer(const VKP::FrameDrawData &frame,const UniformBufferBinding & uniform);
    };
}


#endif //VK_PROJ_PIPELINE_H
