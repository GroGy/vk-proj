//
// Created by Matty on 2021-03-24.
//

#ifndef VK_PROJ_SWAPCHAINSUPPORTDETAILS_H
#define VK_PROJ_SWAPCHAINSUPPORTDETAILS_H

#include <vector>
#include <vulkan/vulkan.h>
#include "../Window.h"

namespace VKP {
    class Window;
    class SwapChainSupportDetails {
    public:
        VkSurfaceCapabilitiesKHR m_Capabilities;
        std::vector<VkSurfaceFormatKHR> m_Formats;
        std::vector<VkPresentModeKHR> m_PresentModes;

        VkSurfaceFormatKHR ChooseSwapSurfaceFormat();
        VkPresentModeKHR ChooseSwapPresentMode();
        VkExtent2D ChooseSwapExtent(const std::shared_ptr<VKP::Window> & window);
    };
}

#endif //VK_PROJ_SWAPCHAINSUPPORTDETAILS_H
