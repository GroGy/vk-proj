//
// Created by Matty on 2021-03-22.
//

#ifndef VK_PROJ_QUEUEFAMILIES_H
#define VK_PROJ_QUEUEFAMILIES_H

#include <optional>
#include <cstdint>

namespace VKP {
    struct QueueFamilies {
        public:
        std::optional<uint32_t> m_pGraphicsFamily;
        std::optional<uint32_t> m_pPresentFamily;

        bool IsComplete() {
            return m_pGraphicsFamily.has_value() && m_pPresentFamily.has_value();
        }
    };
}




#endif //VK_PROJ_QUEUEFAMILIES_H
