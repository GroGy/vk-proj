//
// Created by Matty on 7.5.2020.
//

#ifndef WG_SHADER_H
#define WG_SHADER_H

#include <memory>
#include <sstream>
#include <glm/glm.hpp>
#include <vulkan/vulkan.h>
#include "../../Exceptions.h"

namespace VKP {
    enum ShaderType : uint8_t {
        VKP_SHADER_VERTEX = 0,
        VKP_SHADER_FRAGMENT = 1,
        VKP_SHADER_GEOMETRY = 2,
        VKP_SHADER_TESSELATION = 3,
    };

    class Shader {
    private:
        uint64_t m_pID;
        VkShaderModule m_pModule;
        std::string m_pPath;
        bool m_pLoaded = false;
        ShaderType m_pType;
        void _Cleanup();
        bool m_pValid = false;
        uint64_t _GetIDInternal();
    public:
        Shader() = default;
        template <typename STR>
        Shader(STR&& path, ShaderType type)
        : m_pPath(std::forward<STR>(path)), m_pType(type), m_pValid(true), m_pID(_GetIDInternal()){
        }
        Shader(Shader&& o);
        ~Shader();
        VkShaderModule GetModule() const;
        ShaderType GetType() const { return m_pType; }
        bool IsLoaded() const { return m_pLoaded; }
        void Refresh();
        void Load();
        void Unload();
        uint64_t GetID() const;
    };
}

#endif //WG_SHADER_H
