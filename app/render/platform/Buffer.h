//
// Created by Matty on 2021-03-27.
//

#ifndef VK_PROJ_BUFFER_H
#define VK_PROJ_BUFFER_H

namespace VKP {
    class Buffer {
    private:
        bool m_pValid = false;
        bool m_pMarkedForDeletion = false;
        VkBuffer m_pBuffer;
        VmaAllocation m_pAllocation;
        VmaAllocator m_pAllocator;
        VmaAllocationInfo m_pAllocInfo;
        size_t m_pSize;
    public:
        Buffer() = default;
        Buffer(VmaAllocator & allocator,VkBufferCreateInfo bufferInfo, VmaAllocationCreateInfo allocInfo);
        bool MarkedForDeletion() const;
        void MarkForDeletion();
        void Cleanup(const VmaAllocator & allocator);
        void LoadData(const void * data, size_t size);
        void LoadData(VKP::Buffer & storage);
        ~Buffer() = default;

        Buffer(Buffer&& o) noexcept;
        Buffer& operator=(Buffer&& o) noexcept;
        Buffer(const Buffer& o) = delete;
        Buffer& operator=(const Buffer& o) = delete;
        VkBuffer GetVulkanBuffer();
    };
}

#endif //VK_PROJ_BUFFER_H
