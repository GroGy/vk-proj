//
// Created by Matty on 2021-03-27.
//

#ifndef VK_PROJ_RENDERPASS_H
#define VK_PROJ_RENDERPASS_H

#include <vulkan/vulkan.h>
#include <vector>

namespace VKP {
    struct AttachmentBlueprint {
        bool m_Valid = false;
        VkFormat m_Format;
        uint32_t m_SampleCount = 1;
        VkImageLayout m_Layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        VkAttachmentLoadOp m_LoadOP = VK_ATTACHMENT_LOAD_OP_CLEAR;
        VkAttachmentStoreOp m_StoreOP = VK_ATTACHMENT_STORE_OP_STORE;
        VkAttachmentLoadOp m_StencilLoadOP = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        VkAttachmentStoreOp m_StencilStoreOP = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        VkImageLayout m_InitialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        VkImageLayout m_FinalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        AttachmentBlueprint(VkFormat format);
        AttachmentBlueprint() = default;
    };

    class SubPassBlueprint {
    private:
        bool m_pValid = false;
        std::vector<AttachmentBlueprint> m_pAttachments;
        AttachmentBlueprint m_pDepthStencilAttachment;
        VkSubpassDescription _BuildSubPass();
    public:
        VkPipelineBindPoint m_BindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

        friend class RenderPassBlueprint;
        SubPassBlueprint() = default;
        SubPassBlueprint(AttachmentBlueprint attachment);
        void SetDepthAttachment(AttachmentBlueprint depthAttachment);
        void AddAttachment(AttachmentBlueprint attachment);
    };

    struct RenderPassBlueprintResult {
        VkRenderPass m_RenderPass;
    };

    class RenderPassBlueprint {
    public:
        VkSampleCountFlagBits _GetSamples(uint32_t sampleCount);
        friend class RenderPass;
        void AddSubPass(SubPassBlueprint pass);
    private:
        std::vector<SubPassBlueprint> m_pSubPasses;
        RenderPassBlueprintResult _BuildRenderPass();
    };

    class RenderPass {
    private:
        VkRenderPass m_pRenderPass;
        bool m_pValid = false;

        RenderPassBlueprint m_pBlueprint;
    public:
        RenderPass() = default;

        RenderPass(RenderPassBlueprint blueprint);

        VkRenderPass GetRenderPass() const;

        void Load();

        void Unload();

        void Cleanup();
    };

}

#endif //VK_PROJ_RENDERPASS_H
