//
// Created by Matty on 2021-03-26.
//

#ifndef VK_PROJ_VERTEXBUFFER_H
#define VK_PROJ_VERTEXBUFFER_H

#include "platform/Buffer.h"
#include "Vertex.h"
#include "StagingBuffer.h"

namespace VKP {
    class VertexBuffer {
    private:
        bool m_pValid = false;
        VKP::Buffer m_pBuffer;
        uint32_t m_pVertexCount = 0;
    public:
        VertexBuffer() = default;
        ~VertexBuffer();
        VertexBuffer(VmaAllocator & allocator, uint64_t size);

        VertexBuffer(VertexBuffer&& o) noexcept;
        VertexBuffer& operator=(VertexBuffer&& o) noexcept;
        VertexBuffer(const VertexBuffer& o) = delete;
        VertexBuffer& operator=(const VertexBuffer& o) = delete;

        void Cleanup(const VmaAllocator & allocator);
        void MarkForDeletion();
        bool MarkedForDeletion() const;
        void LoadData(const void *data,size_t size, uint32_t vertexCount);
        void LoadData(const VKP::StagingBuffer & staging);
        void BindVertexBuffer(VkCommandBuffer cmdBuffer);
        uint32_t GetVertexCount() const;
        VKP::Buffer & GetBuffer();
    };
}


#endif //VK_PROJ_VERTEXBUFFER_H
