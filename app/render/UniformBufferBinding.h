//
// Created by Matty on 2021-04-29.
//

#ifndef VK_PROJ_UNIFORMBUFFERBINDING_H
#define VK_PROJ_UNIFORMBUFFERBINDING_H

#include "UniformBuffer.h"

namespace VKP {
    class UniformBufferBinding {
    public:
        uint16_t m_Version;
        std::vector<VkDescriptorSet> m_DescriptorSets;
        std::shared_ptr<VKP::UniformBuffer> m_Buffer;
    };
}


#endif //VK_PROJ_UNIFORMBUFFERBINDING_H
