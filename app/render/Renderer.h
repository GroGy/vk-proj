//
// Created by Matty on 29.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_RENDERER_H
#define WORLD_GENERATOR_CORE_RENDERER_H

#include <thread>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "platform/QueueFamilies.h"
#include "platform/SwapChainSupportDetails.h"
#include "platform/RenderPass.h"
#include "Framebuffer.h"
#include "Window.h"

#include "../imgui/imgui_impl_vulkan.h"

#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "UniformBuffer.h"
#include "FrameDrawData.h"
#include "DepthTexture.h"
#include "TextureSampler.h"

namespace VKP {
    class Window;
    class SwapChainSupportDetails;
    class Renderer {
    private:
        const int MAX_FRAMES_IN_FLIGHT = 2;
        //region DEBUG
#ifndef NDEBUG
        const std::vector<const char*> m_pValidationLayers = {
                "VK_LAYER_KHRONOS_validation"
        };
        bool _CheckValidationLayerSupport();
        VkDebugUtilsMessengerEXT m_pDebugMessenger;
        VkResult _CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger);
        void _DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator);
        void _InitDebuggingInternal();
#endif
        //endregion
        const std::vector<const char*> m_pRequiredExtensions = {
                VK_KHR_SWAPCHAIN_EXTENSION_NAME
        };

        VkInstance m_pInstance;
        VkPhysicalDevice m_pPhysicalDevice = VK_NULL_HANDLE;
        VkDevice m_pDevice;

        double m_pLastCleanup;
        bool m_pFirstCleanup = true;

        VkQueue m_pGraphicsQueue;
        VkQueue m_pPresentQueue;

        VkSwapchainKHR m_pSwapChain;
        VkFormat m_pSwapChainImageFormat;
        VkExtent2D m_pSwapChainExtent;

        VkSurfaceFormatKHR m_pSurfaceFormat;
        VkPresentModeKHR m_pSurfacePresentMode;
        VkExtent2D m_pSurfaceExtent;

        std::vector<VkImage> m_pSwapChainImages;
        std::vector<VkImageView> m_pSwapChainImageViews;
        VKP::DepthTexture m_pDepthTexture;

        std::vector<VKP::FrameBuffer> m_pSwapChainFrameBuffers;

        VKP::RenderPass m_pCameraRenderPass;

        VkCommandPool m_pCommandPoll;
        std::vector<VkCommandBuffer> m_pCommandBuffers;

        std::vector<VkSemaphore> m_pImageAvailableSemaphores;
        std::vector<VkSemaphore> m_pRenderFinishedSemaphores;

        VkSurfaceKHR m_pSurface;

        uint32_t m_pCurrentImageIndex = 0;

        std::vector<VkFence> m_pInFlightFences;
        std::vector<VkFence> m_pImagesInFlight;
        size_t m_pCurrentFrame = 0;

        VmaAllocator m_pAllocator;

        VkDescriptorPool m_pImguiDescPool;

        std::shared_ptr<VKP::Window> m_pMainWindow;

        std::shared_ptr<VKP::TextureSampler> m_pSampler;

        void _InitInstance(const std::shared_ptr<VKP::Window> & window);
        void _InitInstanceInternal(VkApplicationInfo * appInfo);
        void _InitPhysicalDevicesInternal();
        bool _IsDeviceSuitable(VkPhysicalDevice device);
        bool _CheckDeviceExtensionSupport(VkPhysicalDevice device);
        VKP::SwapChainSupportDetails _GetSwapChainSupport(VkPhysicalDevice device);
        std::vector<const char*> _GetRequiredExtensionsInternal();
        QueueFamilies _FindQueueFamiliesInternal(VkPhysicalDevice device);
        void _InitLogicalDevicesInternal();
        void _InitSurfaceInternal(const std::shared_ptr<VKP::Window> & window);
        void _InitSwapChainInternal();
        void _InitImageViewsInternal();
        void _InitMainRenderPasses();
        void _InitGraphicsPipelinesInternal();
        void _InitFrameBuffersInternal();
        void _InitCommandPollInternal();
        void _InitCommandBuffersInternal();
        void _InitSyncInternal();
        void _InitAllocatorInternal();

        void _RebuildSwapChain();

        int32_t m_pWindowWidth, m_pWindowHeight;
    public:
        std::pair<int32_t,int32_t> GetWindowSize() const;
        std::pair<int32_t,int32_t> GetScreenSize() const;
        /// Renderer
        static std::unique_ptr<Renderer> m_Renderer;

        void Init(const std::shared_ptr<VKP::Window> & window);

        void Cleanup();

        ~Renderer();

        Renderer();

        void Update();

        FrameDrawData GetFrameDrawData();

        void BeginFrame();

        void FinishFrame();

        void PresentRender();

        void SetWireframeMode(bool wireframeActive);

        VkDevice GetDevice() const;

        VkExtent2D GetSwapChainExtent() const;

        VkRenderPass GetMainRenderPass() const;

        ImGui_ImplVulkan_InitInfo CreateImGuiInitInfo();

        void WaitIdle();

        void _checkVkResult(VkResult res);

        void BeginMainRenderPass();

        void EndMainRenderPass();

        uint32_t GetSwapImageCount() const;

        uint32_t GetCurrentImageIndex() const;

        VkPhysicalDevice GetPhysicalDevice() const;

        VkCommandBuffer BeginSingleCommandBuffer();

        void EndSingleCommandBuffer(VkCommandBuffer commandBuffer);

        void CopyBuffer(VKP::Buffer & srcBuffer, VKP::Buffer & dstBuffer, VkDeviceSize size);

        void TransitionTextureLayout(VKP::Texture & texture, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);

        void CopyBufferToTexture(VKP::Buffer & srcBuffer, VKP::Texture & dstTexture, uint32_t width, uint32_t height);

        std::shared_ptr<VKP::VertexBuffer>& CreateVertexBuffer(uint64_t size);
        std::shared_ptr<VKP::IndexBuffer>& CreateIndexBuffer(uint32_t count);
        std::shared_ptr<VKP::UniformBuffer>& CreateUniformBuffer(uint64_t size);
        std::shared_ptr<VKP::Texture>& CreateTexture( uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
                                                      VkImageUsageFlags usage, bool createImageView = true);
        std::shared_ptr<VKP::StagingBuffer>& CreateStagingBuffer(uint64_t size);

    };
}


#endif //WORLD_GENERATOR_CORE_RENDERER_H
