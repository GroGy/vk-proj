//
// Created by Matty on 2021-04-29.
//

#ifndef VK_PROJ_TEXTURESAMPLER_H
#define VK_PROJ_TEXTURESAMPLER_H

namespace VKP {
    class TextureSampler {
    private:
        VkSampler m_pSampler;
    public:
        TextureSampler();
        void Unload();
        VkSampler GetSampler() const;
    };
}


#endif //VK_PROJ_TEXTURESAMPLER_H
