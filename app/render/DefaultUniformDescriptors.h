//
// Created by Matty on 2021-04-03.
//

#ifndef VK_PROJ_DEFAULTUNIFORMDESCRIPTORS_H
#define VK_PROJ_DEFAULTUNIFORMDESCRIPTORS_H

namespace VKP {
    class DefaultUniformDescriptors {
    public:
        static VkDescriptorSetLayout s_ModelLayout;
    };
}


#endif //VK_PROJ_DEFAULTUNIFORMDESCRIPTORS_H
