//
// Created by Matty on 2021-03-26.
//

#ifndef VK_PROJ_TEXTURE_H
#define VK_PROJ_TEXTURE_H

#include "StagingBuffer.h"

namespace VKP {
    class Texture {
    protected:
        bool m_pValid = false;
        bool m_pMarkedForDeletion = false;
        VkImage m_pImage;
        VkFormat m_pFormat;
        bool m_pHasImageView = false;
        VkImageView m_pImageView;
        VmaAllocation m_pAllocation;
        VmaAllocator m_pAllocator;
        uint32_t m_pWidth;
        uint32_t m_pHeight;

        VkFormat _FindSupportedFormat(const std::vector<VkFormat> &candidates, VkImageTiling tiling,
                                      VkFormatFeatureFlags features);

        void _CreateImageView(VkFormat format, VkImageAspectFlags aspectFlags);
    public:
        Texture() = default;

        Texture(const VmaAllocator &allocator, uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
                VkImageUsageFlags usage, bool createImageView = true);

        void LoadTexture(const std::shared_ptr<VKP::StagingBuffer> &buffer);

        void MarkForDeletion();

        bool MarkedForDeletion() const;

        void Cleanup();

        VkImageView GetView() const;
        VkImage GetImage() const;
        VkFormat GetFormat() const;
    };
}

#endif //VK_PROJ_TEXTURE_H
