//
// Created by Matty on 2021-04-03.
//

#ifndef VK_PROJ_FRAMEDRAWDATA_H
#define VK_PROJ_FRAMEDRAWDATA_H

#include "reactphysics3d/reactphysics3d.h"

namespace VKP {
    struct FrameDrawData {
        FrameDrawData(VkCommandBuffer frameCommandBuffer, uint32_t swapImageIndex) : frameCommandBuffer(
                frameCommandBuffer), swapImageIndex(swapImageIndex) {}
        VkCommandBuffer frameCommandBuffer;
        uint32_t swapImageIndex;
        glm::mat4 viewMatrix;
        glm::mat4 projMatrix;
        reactphysics3d::Collider* viewFrustum = nullptr;
    };
}


#endif //VK_PROJ_FRAMEDRAWDATA_H
