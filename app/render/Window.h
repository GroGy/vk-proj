//
// Created by Matty on 10.4.2020.
//

#ifndef LEIKO_WINDOW_H
#define LEIKO_WINDOW_H

#include "Renderer.h"
#include "WindowDestructor.h"

namespace VKP {
        class Renderer;
/// Window class, result of window builder object, containing data about window
        class Window final {
        private:
            /// Background color
            glm::vec4 m_pBackgroundColor;
            /// Window name
            std::string m_pWindowName;
            /// GLFW Window object
            std::unique_ptr<GLFWwindow, VKP::GLFWWindowDestructor> m_pGLFWWindow;
            /// Window dimensions
            static uint32_t m_pWindowWidth, m_pWindowHeight;
        public:
            /// Gets window width
            /// \return Width of window
            static uint32_t GetWindowWidth() { return m_pWindowWidth; };
            /// Gets window height
            /// \return Height of window
            static uint32_t GetWindowHeight() { return m_pWindowHeight; };
            friend class WindowBuilder;
            Window() = default;
            /// Gets window name
            /// \return Window name
            const std::string& GetWindowName();
            /// Returns GLFW window ptr
            /// \return GLFWWindow
            std::unique_ptr<GLFWwindow, VKP::GLFWWindowDestructor>& GetGlfwWindow();
            /// Resize callback for window
            /// \param window Window object
            /// \param width new Width of window
            /// \param height new Height of window
            void FbSizeCallback(GLFWwindow* window, int32_t width, int32_t height);
        };

        class WindowBuilder : public std::enable_shared_from_this<WindowBuilder> {
        public:
            WindowBuilder(uint32_t width, uint32_t height);
            std::shared_ptr<WindowBuilder> setWindowName(const std::string& name);
            std::shared_ptr<WindowBuilder> setBackgroundColor(const glm::vec4& bgColor);
            VKP::Window* finish();
        private:
            bool m_pHasColor = false;
            glm::vec4 m_pBackgroundColor;
            bool m_pHasName = false;
            std::string m_pWindowName;
            bool m_pHasDecorations = false;
            uint32_t m_pWidth, m_pHeight = 0;
        };
}
#endif //LEIKO_WINDOW_H
