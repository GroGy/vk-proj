//
// Created by Matty on 2021-03-27.
//

#ifndef VK_PROJ_MODELVERTEX_H
#define VK_PROJ_MODELVERTEX_H

#include <vulkan/vulkan.h>
#include <vector>
#include "../render/Vertex.h"
#include <fstream>
#include <glm/gtx/io.hpp>

namespace VKP_Engine {
    struct ModelVertex {
        ModelVertex(glm::vec3 pos, glm::vec3 col, glm::vec3 normal) : m_Pos(std::move(pos)), m_Color(std::move(col)), m_Normal(std::move(normal)) {};
        glm::vec3 m_Pos;
        glm::vec3 m_Color;
        glm::vec3 m_Normal;

        static std::vector<VkVertexInputBindingDescription> GetBindingDescription();
        static std::vector<VkVertexInputAttributeDescription> GetAttributeDescriptions();
    };
}


#endif //VK_PROJ_MODELVERTEX_H
