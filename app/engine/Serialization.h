//
// Created by Matty on 2021-04-22.
//

#ifndef VK_PROJ_SERIALIZATION_H
#define VK_PROJ_SERIALIZATION_H

#include <fstream>
#include "ModelVertex.h"

namespace VKP_Engine {
    std::ostream& operator<<(std::ostream& os, const VKP_Engine::ModelVertex& obj);
    std::istream& operator>>(std::istream& is, VKP_Engine::ModelVertex& obj);
}


#endif //VK_PROJ_SERIALIZATION_H
