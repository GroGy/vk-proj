//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_MODELLOADER_H
#define VK_PROJ_MODELLOADER_H

#include "../render/Model.h"

namespace VKP_Engine {
    class ModelLoader {
    private:

    public:
        static std::pair<std::shared_ptr<VKP::Model>, std::optional<std::string>> LoadModel(const std::string & path);
    };
}

#endif //VK_PROJ_MODELLOADER_H
