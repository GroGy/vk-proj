//
// Created by Matty on 2021-04-29.
//

#ifndef VK_PROJ_ENGINEGLOBALS_H
#define VK_PROJ_ENGINEGLOBALS_H

#include "../render/platform/Pipeline.h"

namespace VKP_Engine {
    class EngineGlobals {
    public:
        static VKP::UniformDescriptorBlueprint s_ModelUniformDesc;
    };
}


#endif //VK_PROJ_ENGINEGLOBALS_H
