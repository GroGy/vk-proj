//
// Created by Matty on 2021-04-03.
//

#ifndef VK_PROJ_MODELLOADDATA_H
#define VK_PROJ_MODELLOADDATA_H

#include "ModelVertex.h"

namespace VKP_Engine {
    struct MaterialImportData {
        uint32_t activeOptions;
        uint32_t albedoID;
        uint32_t normalID;
        uint32_t roughnessID;
    };

    struct MeshImportData {
        std::vector<VKP_Engine::ModelVertex> vertices;
        std::vector<uint32_t> indices;
    };

    struct ModelBaseData {
        uint32_t meshCount;
        uint32_t matCount;

    };

    std::ostream& operator<<(std::ostream& os, const ModelBaseData& obj);
    std::istream& operator>>(std::istream& is, ModelBaseData& obj);

    struct ModelImportData {
        std::vector<MeshImportData> meshes;
        std::vector<MaterialImportData> materials;
    };

}


#endif //VK_PROJ_MODELLOADDATA_H
