//
// Created by Matty on 2021-04-25.
//

#ifndef VK_PROJ_PERFDATA_H
#define VK_PROJ_PERFDATA_H


namespace VKP_Engine {
    class PerfData {
    private:
        static std::unique_ptr<PerfData> s_pInstance;
    public:
        PerfData();
        static std::unique_ptr<PerfData> &GetInstance();
    };
}


#endif //VK_PROJ_PERFDATA_H
