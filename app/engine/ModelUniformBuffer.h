//
// Created by Matty on 2021-04-03.
//

#ifndef VK_PROJ_MODELUNIFORMBUFFER_H
#define VK_PROJ_MODELUNIFORMBUFFER_H

#include <glm/glm.hpp>

namespace VKP_Engine {
    struct ModelUniformBuffer {
        ModelUniformBuffer(const glm::mat4 & model,const glm::mat4 &view, const glm::mat4 &projection) : m_Model(model),m_View(view),
                                                                                 m_Projection(projection) {}
        alignas(16) glm::mat4 m_Model;
        alignas(16) glm::mat4 m_View;
        alignas(16) glm::mat4 m_Projection;
    };
}


#endif //VK_PROJ_MODELUNIFORMBUFFER_H
