//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_IMAGELOADER_H
#define VK_PROJ_IMAGELOADER_H

#include "../render/Texture.h"

namespace VKP_Engine {
    class ImageLoader {
    private:

    public:
        static std::pair<std::shared_ptr<VKP::Texture>, std::optional<std::string>> LoadTexture(const std::string & path);
    };
}


#endif //VK_PROJ_IMAGELOADER_H
