//
// Created by Matty on 2021-03-21.
//

#ifndef VK_PROJ_APP_H
#define VK_PROJ_APP_H

#include "render/Window.h"
#include "ui/UI.h"
#include "render/Mesh.h"

namespace VKP {
class App {
public:
    void Run();
    static void RestartGame();
private:
    static bool s_pRestartGame;

    std::shared_ptr<VKP::Window> m_pWindow;
    std::shared_ptr<VKP::UI> m_pUI;
    std::shared_ptr<VKP::UIInternals> m_pUIInternals;

    /////////////////////////////
    void _Init();
    void _MainLoop();
    void _Render();
    void _Cleanup();

    void _RestartGame();
    /////////////////////////////
    void _InitWindow();
};
}


#endif //VK_PROJ_APP_H
