//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_MODELIMPORTER_H
#define VK_PROJ_MODELIMPORTER_H

#include <assimp/scene.h>
#include "../engine/ModelVertex.h"
#include "../engine/ModelLoadData.h"

#define VKP_MODEL_VERSION 1

namespace VKP_Editor {
    class ModelImporter {
    private:
        uint32_t GetVersion(std::ifstream& file);
        static void _BuildModel(const aiMesh *mesh, const glm::mat4 & transform, const aiMaterial *mat,VKP_Engine::ModelImportData& data, uint32_t meshIndex);
        static void _CrawlNode(aiNode *node,  const glm::mat4 & trans,const aiScene *scene,VKP_Engine::ModelImportData& data,uint32_t & meshIndex);
    public:
        static std::pair<bool,std::optional<std::string>> ImportModel(const std::string & path);
    };
}


#endif //VK_PROJ_MODELIMPORTER_H
