//
// Created by Matty on 2021-04-24.
//

#ifndef VK_PROJ_TERRAINCHUNK_H
#define VK_PROJ_TERRAINCHUNK_H

#include "../../render/ModelInstance.h"
#include "../data/Game_GridUnits.h"
#include <reactphysics3d/reactphysics3d.h>

namespace VKP_Game {
    class TerrainChunk {
    private:
        static constexpr uint32_t CHUNK_RES = 16;

        VKP::ModelInstance m_pModelInstance;
        glm::vec3 m_pPosition{0.0f, 0.0f, 0.0f};
        glm::vec3 m_pRotation{0.0f, 0.0f, 0.0f};
        glm::vec3 m_pScale{1.0f, 1.0f, 1.0f};
        reactphysics3d::BoxShape* m_pCollideShape = nullptr;
        reactphysics3d::Collider* m_pCollider = nullptr;
        reactphysics3d::RigidBody* m_pRigidBody = nullptr;
        bool m_pValid = false;
    public:
        TerrainChunk();
        TerrainChunk(reactphysics3d::PhysicsWorld * physicsWorld, const glm::vec3 & pos, const glm::vec3 & rot, const glm::vec3 & scale);
        void Draw(VKP::FrameDrawData &frame);
    };
}


#endif //VK_PROJ_TERRAINCHUNK_H
