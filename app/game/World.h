//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_WORLD_H
#define VK_PROJ_WORLD_H

#include <reactphysics3d/reactphysics3d.h>
#include "world_objects/TerrainChunk.h"

namespace VKP_Game {
    class World {
    private:
        VKP_Game::TerrainChunk m_pTerrain;
        reactphysics3d::PhysicsWorld * m_pPhysicsWorld = nullptr;
        bool m_pValid = false;
    public:
        World() = default;
        World(float gravity, bool sleepingEnabled);
        void Update(float delta);
        reactphysics3d::PhysicsWorld * GetPhysicsWorld();
        void Draw(VKP::FrameDrawData &frame);
        bool IsValid() const;
    };
}


#endif //VK_PROJ_WORLD_H
