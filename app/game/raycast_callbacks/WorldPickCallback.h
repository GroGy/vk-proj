//
// Created by Matty on 2021-09-18.
//

#ifndef VK_PROJ_WORLDPICKCALLBACK_H
#define VK_PROJ_WORLDPICKCALLBACK_H

#include <reactphysics3d/collision/RaycastInfo.h>

namespace VKP_Game {
    class WorldPickCallback : public reactphysics3d::RaycastCallback {
    private:

    public:
        reactphysics3d::decimal notifyRaycastHit(const reactphysics3d::RaycastInfo &raycastInfo) override;
    };
}


#endif //VK_PROJ_WORLDPICKCALLBACK_H
