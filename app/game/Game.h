//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_GAME_H
#define VK_PROJ_GAME_H

#include "GameInitData.h"
#include "../render/ModelInstance.h"
#include "../render/FrameDrawData.h"
#include "../render/Camera.h"
#include "World.h"

namespace VKP_Game {
    class Game {
    private:
        VKP_Game::GameInitData m_pInitData;
        static std::unique_ptr<Game> s_pInstance;
        bool m_pRotatingCam = false;
        glm::vec3 m_pCamRot;
        glm::vec3 m_pCamRotStart;
        glm::vec2 m_pMousePosStart;
        float m_pCamDist = 10.0f;

        void _LoadBaseModel(std::shared_ptr<VKP::Model> & model, const std::string & path);
    public:
        static std::unique_ptr<Game> & GetInstance();
        void Init(GameInitData initData);
        void Render(VKP::FrameDrawData & frame);
        void Update(float deltaTime);
        void Cleanup();
        void LoadWorld();
    };
}


#endif //VK_PROJ_GAME_H
