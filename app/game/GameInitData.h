//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_GAMEINITDATA_H
#define VK_PROJ_GAMEINITDATA_H

#include <functional>
#include <stdint.h>
#include "../render/Window.h"

namespace VKP_Game {
struct GameInitData {
    std::function<void(uint32_t,uint32_t)> m_ChangeResolutionCallback;
    std::shared_ptr<VKP::Window> m_MainWindow;
};
}


#endif //VK_PROJ_GAMEINITDATA_H
