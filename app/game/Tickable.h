//
// Created by Matty on 2021-08-26.
//

#ifndef VK_PROJ_TICKABLE_H
#define VK_PROJ_TICKABLE_H

namespace VKP_Game {
    class Tickable {
    public:
        void Tick();
    };
}


#endif //VK_PROJ_TICKABLE_H
