//
// Created by Matty on 2021-04-23.
//

#ifndef VK_PROJ_GAME_RENDERDATA_H
#define VK_PROJ_GAME_RENDERDATA_H

#include "../../render/Model.h"
#include "../../render/Camera.h"

namespace VKP_Game {
    class Game_RenderData {
    private:
        static std::unique_ptr<Game_RenderData> s_pInstance;
    public:
        static std::unique_ptr<Game_RenderData> &GetInstance();

        std::shared_ptr<VKP::Model> m_Cube1x1Model;
        std::shared_ptr<VKP::Model> m_FactoryModel;
        std::shared_ptr<VKP::Model> m_OreModel;
        std::shared_ptr<VKP::Model> m_CharacterModel;

        VKP::Camera m_Camera;
    };
}


#endif //VK_PROJ_GAME_RENDERDATA_H
