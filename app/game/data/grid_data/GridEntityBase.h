//
// Created by Matty on 2021-09-01.
//

#ifndef VK_PROJ_GRIDENTITYBASE_H
#define VK_PROJ_GRIDENTITYBASE_H

namespace VKP_Game {

    template <typename T>
    class GridEntityBase {
    public:
        static uint32_t ID();
        static bool     Registered();

    private:
        static uint32_t s_pID;

        friend class Game_UnitRegistry;
    };

    template <typename T>
    uint32_t VKP_Game::GridEntityBase<T>::s_pID = (std::numeric_limits<uint32_t>::max)();

    template<typename T>
    inline uint32_t VKP_Game::GridEntityBase<T>::ID()
    {
        //assert(s_pID != (std::numeric_limits<uint16_t>::max)(), "IGridEntity type was not registered");
        return s_pID;
    }
    template<typename T>
    inline bool VKP_Game::GridEntityBase<T>::Registered()
    {
        return s_pID != (std::numeric_limits<uint32_t>::max)();
    }

}


#endif //VK_PROJ_GRIDENTITYBASE_H
