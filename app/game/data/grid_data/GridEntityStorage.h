//
// Created by Matty on 2021-09-01.
//

#ifndef VK_PROJ_GRIDENTITYSTORAGE_H
#define VK_PROJ_GRIDENTITYSTORAGE_H

#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/hash.hpp"

#include "GridEntityBase.h"

namespace VKP_Game {
    class IGridEntityStorage {
    public:
        virtual                      ~IGridEntityStorage() = default;

        virtual void Clear() = 0;

        virtual IGridEntityStorage *Move(uint8_t *buffer) = 0;

        virtual IGridEntityStorage *Move() = 0;

        virtual IGridEntityStorage *Copy(uint8_t *buffer) const = 0;

        virtual IGridEntityStorage *Copy() const = 0;

        virtual void *GetRawGridEntity(const glm::ivec2 &pos) = 0;

        virtual const void *GetRawGridEntity(const glm::ivec2 &pos) const = 0;

        virtual void RemoveGridEntity(const glm::ivec2 &pos) = 0;

        virtual bool ContainsGridEntity(const glm::ivec2 &pos) const = 0;

        virtual uint32_t ID() const = 0;
    };

    template<typename T>
    class GridEntityStorage final : public IGridEntityStorage {
    public:
        GridEntityStorage() = default;

        GridEntityStorage(const GridEntityStorage<T> &other) = delete;

        GridEntityStorage(GridEntityStorage<T> &&other) noexcept
                :
                m_pData(std::move(other.m_pData)) {}

        GridEntityStorage<T> &operator=(const GridEntityStorage<T> &other) = delete;

        GridEntityStorage<T> &operator=(GridEntityStorage<T> &&other) {
            m_pData = std::move(other.m_pData);
            return *this;
        }

        void Clear() override {
            m_pData.clear();
        }

        IGridEntityStorage *Move(uint8_t *buffer) override {
            return new(buffer)GridEntityStorage<T>(std::move(*this));
        }

        IGridEntityStorage *Move() override {
            return new GridEntityStorage<T>(std::move(*this));
        }

        IGridEntityStorage *Copy(uint8_t *buffer) const override {
            GridEntityStorage<T> *copy = new(static_cast<void *>(buffer))GridEntityStorage<T>();
            copy->m_pData = m_pData;
            return copy;
        }

        IGridEntityStorage *Copy() const override {
            GridEntityStorage<T> *copy = new GridEntityStorage<T>();
            copy->m_pData = m_pData;
            return copy;
        }

        void *GetRawGridEntity(const glm::ivec2 &pos) override {
            return &m_pData[pos];
        }

        const void *GetRawGridEntity(const glm::ivec2 &pos) const override {
            return &m_pData.at(pos);
        }

        void RemoveGridEntity(const glm::ivec2 &pos) override {
            RemoveGridEntityBase(pos);
        }


        uint32_t ID() const override {
            return GridEntityBase<T>::ID();
        }

        T &AddGridEntityBase(const glm::ivec2 &pos, const T &entt) {
            m_pData.emplace(pos, entt);

            return m_pData.at(pos);
        }

        bool RemoveGridEntityBase(const glm::ivec2 &pos) {
            if (m_pData.contains(pos)) {
                m_pData.erase(pos);
                return true;
            }
            return false;
        }

        [[nodiscard]] bool ContainsGridEntity(const glm::ivec2 &pos) const override {
            return m_pData.contains(pos);
        }

        const T& GetGridEntity(const glm::ivec2 &pos) const {
            return m_pData.at(pos);
        };

        T& GetGridEntity(const glm::ivec2 &pos) {
            return m_pData.at(pos);
        };
    private:
        std::unordered_map<glm::ivec2, T> m_pData{};
    };
}


#endif //VK_PROJ_GRIDENTITYSTORAGE_H
