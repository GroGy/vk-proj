//
// Created by Matty on 2021-08-26.
//

#ifndef VK_PROJ_IGRIDENTITY_H
#define VK_PROJ_IGRIDENTITY_H

#include <cstdint>
#include "../Game_UnitRegistry.h"
#include "../Game_RenderData.h"
#include "../../../render/ModelInstance.h"

#define GRID_ENTITY_CONSTRUCTOR(type) type() { \
        Game_UnitRegistry::s_Instance.RegisterUnit<type>(NAME);\
};

namespace VKP_Game {
    struct IGridEntity {
        virtual bool IsTickable() { return false; };
        virtual bool IsRendered() const { return false; }
        virtual void RegisterTicking() {};
        virtual std::shared_ptr<VKP::Model> GetModel() const { return VKP_Game::Game_RenderData::GetInstance()->m_FactoryModel;}
        VKP::ModelInstance m_ModelInstance{};
    protected:
        virtual std::string GetName() = 0;
    };

}
#endif //VK_PROJ_IGRIDENTITY_H
