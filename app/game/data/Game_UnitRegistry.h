//
// Created by Matty on 2021-08-26.
//

#ifndef VK_PROJ_GAME_UNITREGISTRY_H
#define VK_PROJ_GAME_UNITREGISTRY_H

#include "grid_data/GridEntityStorage.h"
#include "grid_data/IGridEntity.h"

namespace VKP_Game {
    class IGridEntity;

    class Game_UnitRegistry {
    private:
        std::vector<IGridEntityStorage *> m_pStorages;
        uint32_t m_pStoragesCreated = 0;

        static uint32_t s_pNextComponentTypeID;

        std::unordered_map<std::string, uint32_t> m_pIDMap{};

        template<typename T>
        void createStorage() {
            size_t oldSize = m_pStorages.size();
            size_t id = (size_t) GridEntityBase<T>::ID();
            if (oldSize <= id)
                m_pStorages.resize(id + 1);
            for (size_t i = oldSize; i < m_pStorages.size(); ++i)
                m_pStorages[i] = nullptr;
            if (m_pStorages[id]) // Storage already exists
                return;

            m_pStorages[id] = new GridEntityStorage<T>();
            m_pStoragesCreated++;
        }

        template<typename T>
        static void registerGridEntityType() {
            if (!GridEntityBase<T>::Registered())
                GridEntityBase<T>::s_pID = s_pNextComponentTypeID++;
        }

        void destroyStorages();

    public:
        template<typename T>
        void RegisterUnit(const std::string &name);

        uint32_t GetUnitID(const std::string &name);

        std::vector<std::pair<std::string, uint32_t>> GetRegistredUnits() const;

        static Game_UnitRegistry s_Instance;

        template <typename T>
        void CreateStorage()
        {
            registerGridEntityType<T>();
            createStorage<T>();
        }

        template <typename T>
        void RemoveGridEntity(const glm::ivec2 & pos)
        {
            GridEntityStorage<T>& storage = GetStorage<T>();
            storage.RemoveComponent(pos);
        }

        IGridEntity* GetIGridEntity(const glm::ivec2 & pos)
        {
            for(uint32_t i = 0; i < m_pStorages.size(); i++) {
                if(m_pStorages[i]->ContainsGridEntity(pos)) {
                    return (IGridEntity*)m_pStorages[i]->GetRawGridEntity(pos);
                }
            }
            return nullptr;
        }

        const IGridEntity* GetIGridEntity(const glm::ivec2 & pos) const
        {
            for(uint32_t i = 0; i < m_pStorages.size(); i++) {
                if(m_pStorages[i]->ContainsGridEntity(pos)) {
                    return (const IGridEntity*)m_pStorages[i]->GetRawGridEntity(pos);
                }
            }
            return nullptr;
        }

        template <typename T>
        T& GetGridEntity(const glm::ivec2 & pos)
        {
            GridEntityStorage<T>& storage = GetStorage<T>();
            return storage.GetGridEntity(pos);
        }

        template <typename T>
        const T& GetGridEntity(const glm::ivec2 & pos) const
        {
            const GridEntityStorage<T>& storage = GetStorage<T>();
            return storage.GetGridEntity(pos);
        }

        template<typename T>
        T& AddGridEntity(const glm::ivec2 & pos);

        template <typename T>
        GridEntityStorage<T>& GetStorage()
        {
            return *static_cast<GridEntityStorage<T>*>(m_pStorages[(size_t)GridEntityBase<T>::ID()]);
        }

        template <typename T>
        const GridEntityStorage<T>& GetStorage() const
        {
            return *static_cast<GridEntityStorage<T>*>(m_pStorages[(size_t)GridEntityBase<T>::ID()]);
        }

        template <typename T>
        bool Is(const glm::ivec2 & pos) const {
            const GridEntityStorage<T>& storage = GetStorage<T>();
            return storage.ContainsGridEntity(pos);
        }
    };

    template<typename T>
    void Game_UnitRegistry::RegisterUnit(const std::string &name) {
        if (m_pIDMap.contains(name)) return;
        CreateStorage<T>();
        m_pIDMap[name] = GridEntityBase<T>::ID();
    }

    template<typename T>
    T &Game_UnitRegistry::AddGridEntity(const glm::ivec2 &pos) {
        GridEntityStorage<T>& storage = GetStorage<T>();

        if(storage.ContainsGridEntity(pos)) return storage.GetGridEntity(pos);

        for(uint32_t i = 0; i < m_pStorages.size(); i++) {
            if(m_pStorages[i]->ContainsGridEntity(pos)) {
                m_pStorages[i]->RemoveGridEntity(pos);
            }
        }

        auto & res = storage.AddGridEntityBase(pos, {});

        if(res.IsTickable()) {
            res.RegisterTicking();
        }

        return res;
    }
}


#endif //VK_PROJ_GAME_UNITREGISTRY_H
