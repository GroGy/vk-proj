//
// Created by Matty on 2021-08-26.
//

#ifndef VK_PROJ_GAME_GRIDUNITS_H
#define VK_PROJ_GAME_GRIDUNITS_H

#include "grid_data/IGridEntity.h"

namespace VKP_Game {
    struct EmptyEntity : public IGridEntity {
        static constexpr auto NAME = "vkp:empty";
        GRID_ENTITY_CONSTRUCTOR(EmptyEntity);
    protected:
        std::string GetName() override {
            return NAME;
        }
    };

    struct CubeEntity : public IGridEntity {
        static constexpr auto NAME = "vkp:cube";
        GRID_ENTITY_CONSTRUCTOR(CubeEntity);
        bool IsRendered() const override { return true; }

        std::shared_ptr<VKP::Model> GetModel() const override {
            return VKP_Game::Game_RenderData::GetInstance()->m_Cube1x1Model;
        }

    protected:
        std::string GetName() override {
            return NAME;
        }
    };
}


#endif //VK_PROJ_GAME_GRIDUNITS_H
