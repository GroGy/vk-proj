//
// Created by Matty on 2021-04-24.
//

#ifndef VK_PROJ_GAME_WORLDDATA_H
#define VK_PROJ_GAME_WORLDDATA_H

#include <reactphysics3d/reactphysics3d.h>
#include "../Player.h"
#include "../World.h"

namespace VKP_Game {
    class Game_WorldData {
    private:
        static std::unique_ptr<Game_WorldData> s_pInstance;
    public:
        Game_WorldData();
        static std::unique_ptr<Game_WorldData> &GetInstance();

        reactphysics3d::PhysicsCommon m_PhysicsCommon;
        Player m_Player;
        VKP_Game::World m_World;
    };
}


#endif //VK_PROJ_GAME_WORLDDATA_H
