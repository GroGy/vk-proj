//
// Created by Matty on 2021-03-28.
//

#ifndef VK_PROJ_PLAYER_H
#define VK_PROJ_PLAYER_H

#include "../render/ModelInstance.h"
#include <reactphysics3d/reactphysics3d.h>

namespace VKP_Game {
    class Player {
    private:
        VKP::ModelInstance m_pModel;
        bool m_pValid = false;
        glm::vec3 m_pPosition{0.0f, 0.0f, 0.0f};
        glm::vec3 m_pRotation{0.0f, 0.0f, 0.0f};
        glm::vec3 m_pRotationTarget{0.0f, 0.0f, 0.0f};
        bool m_pGrounded = true;
        reactphysics3d::CapsuleShape* m_pColliderShape = nullptr;
        reactphysics3d::Collider* m_pCollider = nullptr;
        reactphysics3d::RigidBody* m_pRigidBody = nullptr;
        float m_pScale = 1.0f;
        float m_pSpeed = 1.0f;
    public:
        Player() = default;
        Player(reactphysics3d::PhysicsWorld * physicsWorld, const glm::vec3 & pos = {0.0f, 100.0f, 0.0f}, const glm::vec3 & rotation = {0.0f, 0.0f, 0.0f}, float scale = 1.0f);
        const glm::vec3 &GetPosition() const;
        void SetPosition(const glm::vec3 &position);
        const glm::vec3 &GetRotation() const;
        void SetRotation(const glm::vec3 &rotation);
        float GetScale() const;
        void SetScale(float scale);
        void Draw(VKP::FrameDrawData &frame);
        void Update(float deltaTime);
        bool IsValid() const;
    };
}

#endif //VK_PROJ_PLAYER_H
