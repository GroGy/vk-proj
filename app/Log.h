//
// Created by Matty on 2021-03-24.
//

#ifndef VK_PROJ_LOG_H
#define VK_PROJ_LOG_H

#include <spdlog/spdlog.h>

namespace VKP {
#define VKP_LOG_INIT() spdlog::set_pattern("[%H:%M:%S %z] [%n] [%^---%L---%$] [thread %t] %v");

#define VKP_LOG_INFO(fmt, ...) spdlog::info(fmt __VA_OPT__(,) __VA_ARGS__);
#define VKP_LOG_WARN(fmt, ...) spdlog::warn(fmt __VA_OPT__(,) __VA_ARGS__);
#define VKP_LOG_ERROR(fmt, ...) spdlog::critical(fmt __VA_OPT__(,) __VA_ARGS__);
#define VKP_LOG_LEVEL_ERROR() spdlog::set_level(spdlog::level::critical);
#define VKP_LOG_LEVEL_WARN() spdlog::set_level(spdlog::level::warn);
#define VKP_LOG_LEVEL_INFO() spdlog::set_level(spdlog::level::info);

#ifdef NDEBUG
#define     VKP_LOG_DEBUG(fmt, ...)
#define     VKP_LOG_LEVEL_DEBUG()
#else
#define     VKP_LOG_DEBUG(fmt, ...) spdlog::debug(fmt __VA_OPT__(,) __VA_ARGS__);
#define     VKP_LOG_LEVEL_DEBUG() spdlog::set_level(spdlog::level::debug);
#endif
}

#endif //VK_PROJ_LOG_H
