cmake_minimum_required(VERSION 3.17)
project(vk_proj)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

find_package(Vulkan REQUIRED)
find_package(Assimp REQUIRED)
find_package(Lua51 REQUIRED)

add_definitions(-DVK_USE_PLATFORM_WIN32_KHR)

SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -fuse-ld=lld")

set(
        IMGUI_CODE
        app/imgui/imgui.h
        app/imgui/imgui.cpp
        app/imgui/imconfig.h
        app/imgui/imgui_draw.cpp
        app/imgui/imgui_demo.cpp
        app/imgui/imgui_impl_glfw.cpp
        app/imgui/imgui_impl_glfw.h
        app/imgui/imgui_impl_vulkan.cpp
        app/imgui/imgui_impl_vulkan.h
        app/imgui/imgui_tables.cpp
        app/imgui/imgui_internal.h
        app/imgui/imgui_widgets.cpp
        app/imgui/imstb_rectpack.h
        app/imgui/imstb_textedit.h
        app/imgui/imstb_truetype.h
        app/imgui/imfilebrowser.h

)


set(
        CODE
        main.cpp
        src/App.cpp app/App.h
        src/Renderer.cpp app/render/Renderer.h
        src/Shader.cpp app/render/platform/Shader.h
        src/Window.cpp app/render/Window.h app/render/WindowDestructor.h
        src/Exceptions.cpp app/Exceptions.h
        src/QueueFamilies.cpp app/render/platform/QueueFamilies.h
        src/SwapChainSupportDetails.cpp app/render/platform/SwapChainSupportDetails.h
        src/RenderData.cpp app/render/RenderData.h
        src/Pipeline.cpp app/render/platform/Pipeline.h
        src/UtilFile.cpp app/util/UtilFile.h
        app/Log.h
        src/Vertex.cpp app/render/Vertex.h
        src/VertexBuffer.cpp app/render/VertexBuffer.h
        src/Mesh.cpp app/render/Mesh.h
        src/Texture.cpp app/render/Texture.h
        src/UI.cpp app/ui/UI.h
        src/VulkanAllocator.cpp
        src/Buffer.cpp app/render/platform/Buffer.h
        src/ModelVertex.cpp app/engine/ModelVertex.h
        src/UI_Window.cpp app/ui/UI_Window.h
        src/UI_Performance.cpp app/ui/windows/UI_Performance.h
        src/RenderPass.cpp app/render/platform/RenderPass.h
        src/Material.cpp app/render/Material.h
        src/UniformBuffer.cpp app/render/UniformBuffer.h
        src/IndexBuffer.cpp app/render/IndexBuffer.h
        src/World.cpp app/game/World.h
        src/WorldSegment.cpp app/game/WorldSegment.h
        src/Game.cpp app/game/Game.h
        src/GameInitData.cpp app/game/GameInitData.h
        src/Player.cpp app/game/Player.h
        src/InputController.cpp app/input/InputController.h
        src/Model.cpp app/render/Model.h
        src/Camera.cpp app/render/Camera.h
        src/ModelLoader.cpp app/engine/ModelLoader.h
        src/ImageLoader.cpp app/engine/ImageLoader.h
        src/UI_Tools.cpp app/ui/windows/UI_Tools.h
        src/ModelLoadData.cpp app/engine/ModelLoadData.h
        src/DefaultUniformDescriptors.cpp app/render/DefaultUniformDescriptors.h
        app/render/FrameDrawData.h app/engine/ModelUniformBuffer.h
        src/DepthTexture.cpp app/render/DepthTexture.h
        src/StagingBuffer.cpp app/render/StagingBuffer.h
        src/Serialization.cpp app/engine/Serialization.h
        src/FrameBuffer.cpp app/render/FrameBuffer.h
        src/ModelImporter.cpp app/editor/ModelImporter.h
        src/Game_RenderData.cpp app/game/data/Game_RenderData.h
        src/ModelInstance.cpp app/render/ModelInstance.h
        src/TerrainChunk.cpp app/game/world_objects/TerrainChunk.h
        src/Game_WorldData.cpp app/game/data/Game_WorldData.h
        src/PerfData.cpp app/engine/PerfData.h
        src/UniformBufferBinding.cpp app/render/UniformBufferBinding.h
        src/EngineGlobals.cpp app/engine/EngineGlobals.h
        src/TextureSampler.cpp app/render/TextureSampler.h
        src/UI_PrefabEditor.cpp app/ui/windows/UI_PrefabEditor.h
        src/Building.cpp app/game/world_objects/Building.h
        app/game/data/grid_data/IGridEntity.h
        src/Game_GridUnits.cpp app/game/data/Game_GridUnits.h
        src/Game_UnitRegistry.cpp app/game/data/Game_UnitRegistry.h
        src/Tickable.cpp app/game/Tickable.h
        src/UtilMath.cpp app/util/UtilMath.h
        src/GridEntityBase.cpp app/game/data/grid_data/GridEntityBase.h
        src/GridEntityStorage.cpp app/game/data/grid_data/GridEntityStorage.h
        src/WorldPickCallback.cpp app/game/raycast_callbacks/WorldPickCallback.h
)

set(LIBS
        ${PROJECT_SOURCE_DIR}/lib/libglfw3.a
        ${PROJECT_SOURCE_DIR}/lib/libZep.a
        ${Vulkan_LIBRARY}
        ${LUA_LIBRARIES}
        ${ASSIMP_LIBRARIES}
        ${PROJECT_SOURCE_DIR}/lib/libreactphysics3d.a
        ${PROJECT_SOURCE_DIR}/lib/libOptickCore.dll.a
        )

set(INCLUDE_DIRS
        ${PROJECT_SOURCE_DIR}/include
        ${Vulkan_INCLUDE_DIRS}
        ${LUA_INCLUDE_DIR}
        ${ASSIMP_INCLUDE_DIR}
        )

add_executable(vk_proj ${CODE} ${IMGUI_CODE})

target_precompile_headers(vk_proj
        PRIVATE
            ${PROJECT_SOURCE_DIR}/include/glm/glm.hpp
            <vulkan/vulkan.h>
            <vk_mem_alloc.h>
            ${PROJECT_SOURCE_DIR}/app/Log.h
            <unordered_map>
            <vector>
            <string>
            <map>
            <set>
            <memory>
            <unordered_set>
       )

set_property(TARGET vk_proj PROPERTY INTERPROCEDURAL_OPTIMIZATION False)

target_include_directories(vk_proj PUBLIC ${INCLUDE_DIRS} )
target_link_libraries(vk_proj ${LIBS})